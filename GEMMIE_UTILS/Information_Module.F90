MODULE INFORMATION_MODULE
    USE PRECISION_CONTROL_MODULE
    USE GEMMIE_CONSTANTS
    USE LOGGER_MODULE
    USE CTL_TYPES_MODULE
    USE NIE_UTILITIES
    PUBLIC
    CONTAINS

        SUBROUTINE SHOW_GENERAL_INFO(run_data)
            TYPE(RUN_INPUT), INTENT(IN) :: run_data
            CALL SHOW_SOLVER_INFO(run_data%solver)
            IF (run_data%solver%SPHERICAL) THEN
                CALL SHOW_COMPUTATIONAL_INFO(run_data%cpt)
            ENDIF
        ENDSUBROUTINE

        SUBROUTINE SHOW_SOLVER_INFO(solver)
            TYPE (SOLVER_CTL_TYPE), INTENT(IN) ::solver
            
            CALL LOGGER("Problem type: "//solver%PROBLEM)
            IF (solver%SOLVE_EQUATION) CALL LOGGER("IE equation will be solved")
            IF (solver%COMPUTE_EM_FIELD) CALL LOGGER("Electrical and magnetic fields will be computed")
            IF (solver%out_prms%SAVE_SOLUTION) CALL LOGGER("The solution of IE will be stored to disk")

            IF (solver%out_prms%SAVE_TOTAL_FIELDS) THEN
                CALL LOGGER("Only total  fields will be stored to disk")
            ELSE 
                CALL LOGGER("Normal and anomalous  fields will be stored separately")
            ENDIF
            IF (solver%out_prms%SAVE_E_FIELDS) CALL LOGGER("Electrical  fields will be stored to disk")
            IF (solver%out_prms%SAVE_H_FIELDS) CALL LOGGER("Magnetic  fields will be stored to disk")
            IF (solver%INPUT_DIR/="") THEN
                CALL LOGGER("Input directory: "//solver%INPUT_DIR)
            ELSE
                CALL LOGGER("Input directory: "//".")
            ENDIF

            IF (solver%OUTPUT_DIR/="") THEN
                CALL LOGGER("Output directory: "//solver%OUTPUT_DIR)
            ELSE
                CALL LOGGER("Output directory: "//".")
            ENDIF


            SELECTCASE (solver%IO_format)
                CASE (GEMMIE_BIN_FORMAT)
                    CALL LOGGER("Output format: GEMMIE binary format")
                CASE (HDF5_FORMAT)
                    CALL LOGGER("Output format: HDF5 based binary format")
                CASE DEFAULT
            ENDSELECT

            IF (solver%ALLOW_REUSE_TENSORS) THEN
                CALL LOGGER("To speedup solving different problems at the same frequency")
                CALL LOGGER("The order of calculation may be changed for the sake of reusing tensors")
            ENDIF

            IF (solver%ONLY_CALC_TENSORS) THEN
                CALL LOGGER_ERROR("DEBUGGING OPTION IS ENABLED!")
                CALL LOGGER("PROBLEM WILL NOT BE SOLVED")
                CALL LOGGER("ONLY TENSORS WILL BE CALCULATED")
            ENDIF

        ENDSUBROUTINE


        SUBROUTINE SHOW_COMPUTATIONAL_INFO(cpt)
            TYPE (COMPUTATIONAL_PARAMETERS_TYPE), INTENT(IN) :: cpt

            CALL PRINTF_LOG("NUMBER OF SERIES TERMS USED FOR IE KERNEL: ",cpt%Nterms_IE )
            CALL PRINTF_LOG("NUMBER OF SERIES TERMS USED FOR RC KERNEL: ",cpt%Nterms_RC )

            CALL PRINTF_LOG("NUMBER OF INTEGRATION NODES FOR SHORT DISTANCES ALONG PHI FOR IE KERNEL: ",cpt%Ng_IE_phi )
            CALL PRINTF_LOG("NUMBER OF INTEGRATION NODES FOR SHORT DISTANCES ALONG THETA FOR IE KERNEL: ",cpt%Ng_IE_theta )

            CALL PRINTF_LOG("NUMBER OF INTEGRATION NODES FOR SHORT DISTANCES ALONG PHI FOR RC KERNEL: ",cpt%Ng_RC_phi )
            CALL PRINTF_LOG("NUMBER OF INTEGRATION NODES FOR SHORT DISTANCES ALONG THETA FOR RC KERNEL: ",cpt%Ng_RC_theta )

            CALL PRINTF_LOG("Step pow: ", cpt%STEP_POW)
            CALL PRINTF_LOG("Q threshold: ", cpt%Q_THRESHOLD)

            IF (cpt%use_opt_cond) THEN 
                CALL LOGGER("Optimal conductivity will be used for background")
            ELSE
                CALL LOGGER("The explicit conductivity values will be used for background")
            ENDIF
            IF (cpt%use_only_lat) THEN
                CALL LOGGER("Only lateral components of Green's tensor will be used!")
            ENDIF
        ENDSUBROUTINE


        SUBROUTINE SHOW_SOURCE_INFO(Sources, I)
            INTEGER, INTENT(IN) :: I
            TYPE (SOURCE_DATA_TYPE), INTENT(IN) :: Sources(:)
            CHARACTER (len=1024) :: msg
            WRITE(msg, '(A, I4, A, I4 )') "Source ", I, " from ", SIZE(Sources)
            CALL LOGGER(msg)
            CALL SHOW_FREQ_PERIOD(Sources(I)%freq)
            IF (ALLOCATED(Sources(I)%src_type_name)) &
                CALL LOGGER("Source type:      "//Sources(I)%src_type_name)
            SELECT CASE(Sources(I)%src_type)
            CASE (SHS_SOURCE)
                CALL LOGGER("Source data file: "//trim(Sources(I)%Source_Data))
                CALL LOGGER("ATTENTION !!!")
                CALL LOGGER("Harmonics' coefficients must represent scalar POTENTIAL of external magentic field !!")
                CALL LOGGER("Harmonics' coefficients are assumed be in nT !!")
                CALL LOGGER("Spherical  harmonics' are assumed be Schmidt-normalized !!")
            CASE (OUTSIDE_ANOMALY_SOURCE)
                CALL LOGGER("Source domain is decribed in "//trim(Sources(I)%Currents_Data%fname))
                CALL LOGGER("Currents are will be loaded from "//trim(Sources(I)%Source_Data))
            CASE (INSIDE_ANOMALY_SOURCE)
                CALL LOGGER("Currents are will be loaded from "//trim(Sources(I)%Source_Data))
            CASE (TIPPERS_SOURCE)
                CALL PRINTF_LOG("Pole coordinates are ", [Sources(I)%Currents_Data%pole_theta, &
                    Sources(I)%Currents_Data%pole_phi])
            CASE (MT_PX_SOURCE, MT_PY_SOURCE)
            CASE  DEFAULT
                CALL LOGGER_ERROR("Unsupported source")
            ENDSELECT

            IF (ALLOCATED(Sources(I)%guess_path)) THEN
                CALL LOGGER("Previous IE solution will be loaded from "//trim(Sources(I)%guess_path))
            ENDIF

            IF (ALLOCATED(Sources(I)%output_suffix)) &
                CALL LOGGER("Results will be stored with suffixes: "//trim(Sources(I)%output_suffix))
            
            IF (Sources(I)%dump_tensors) CALL LOGGER("Tensors will be stored to disk")
            IF (Sources(I)%dump_scalars) CALL LOGGER("Scalars will be stored to disk")
            IF (Sources(I)%dump_series) CALL LOGGER("Series will be stored to disk")
        ENDSUBROUTINE

        SUBROUTINE SHOW_FGMRES_DATA(fgmres)
            TYPE (FGMRES_CTL_TYPE), INTENT(IN) :: fgmres
            CALL LOGGER("**************** FGMRES PARAMETERS ******************")
            CALL PRINTF_LOG("Target relative misfit: ",fgmres%misfit)
            CALL PRINTF_LOG("Outer iterations buffer size: ", fgmres%fgmres_buf)
            CALL PRINTF_LOG("Inner iterations buffer size: ", fgmres%gmres_buf)
        ENDSUBROUTINE

        SUBROUTINE PRINT_NESTED_DATA(NIE_DATA)
            TYPE(S2S_NIE_DATA_TYPE), INTENT(IN) :: NIE_DATA

            CALL PRINTF_LOG("R grid+ range: ",NIE_DATA%Nr_loc_plus_dom)
            CALL PRINTF_LOG("R bnd+: ",NIE_DATA%R_plus_bnd)
            CALL PRINTF_LOG("R grid range: ",NIE_DATA%Nr_loc_dom)

            CALL PRINTF_LOG("Theta grid+ range: ",NIE_DATA%Ntheta_loc_plus_dom)
            CALL PRINTF_LOG("Theta bnd+: ",NIE_DATA%Theta_plus_bnd)
            CALL PRINTF_LOG("Theta grid range: ",NIE_DATA%Ntheta_loc_dom)

            CALL PRINTF_LOG("Phi grid+ range: ",NIE_DATA%Nphi_loc_plus_dom)
            CALL PRINTF_LOG("Phi bnd+: ",NIE_DATA%Phi_plus_bnd)
            CALL PRINTF_LOG("Phi grid range: ",NIE_DATA%Nphi_loc_dom)

        ENDSUBROUTINE


        SUBROUTINE SHOW_FREQ_PERIOD(freq)
            REAL(DP), INTENT(IN) :: freq
            CHARACTER (len=1024) :: msg
            REAL(DP), PARAMETER  :: ONE_HOUR=3600.0_DP
            REAL(DP), PARAMETER  :: ONE_DAY=ONE_HOUR*24.0_DP
            REAL(DP), PARAMETER  :: ONE_YEAR=ONE_DAY*365.0_DP
            REAL(DP), PARAMETER  :: FREQ_ONE_HOUR=R_ONE/ONE_HOUR
            REAL(DP), PARAMETER  :: FREQ_ONE_DAY=R_ONE/ONE_DAY
            REAL(DP), PARAMETER  :: FREQ_ONE_YEAR=R_ONE/ONE_YEAR
            REAL(DP) :: T
            IF (freq< R_ZERO) THEN
                PRINT*, "Something is wrong. Negative frequency: ", freq
                STOP
            ELSEIF (freq <TINY(freq)) THEN
                CALL LOGGER ("DIRECT CURRENT EMULATION. Frequency is set to ZERO!")
                RETURN
            ELSEIF (freq <=FREQ_ONE_YEAR) THEN
                T=R_ONE/freq/ONE_YEAR
                WRITE(msg, '(A, G15.8, A)') "Period: ", T, " years"
                CALL LOGGER(msg)
            ELSEIF (freq <=FREQ_ONE_DAY) THEN
                T=R_ONE/freq/ONE_DAY
                WRITE(msg, '(A, G15.8, A)') "Period: ", T, " days"
                CALL LOGGER(msg)
            ELSEIF (freq <=FREQ_ONE_HOUR) THEN
                T=R_ONE/freq/ONE_HOUR
                WRITE(msg, '(A, G15.8, A)') "Period: ", T, " hours"
                CALL LOGGER(msg)
            ELSEIF (freq <= R_ONE) THEN
                T=R_ONE/freq
                WRITE(msg, '(A, G15.8, A)') "Period: ", T, " seconds"
                CALL LOGGER(msg)
            ELSE
                WRITE(msg, '(A, 1ES10.2E2, A)') "Frequency: ", freq, " Hz"
                CALL LOGGER(msg)
            ENDIF
        ENDSUBROUTINE
ENDMODULE 
