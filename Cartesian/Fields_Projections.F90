MODULE FIELDS_PROJECTIONS_MODULE
    USE, INTRINSIC :: ISO_FORTRAN_ENV
    USE PRECISION_CONTROL_MODULE
    USE GEMMIE_CONSTANTS
    USE LOGGER_MODULE
    USE PROJECTIONS_MODULE
    USE CARTESIAN_DATA_TYPES
    USE RADIAL_DATA_TYPES_MODULE
    
    USE GAUSS_INTEGRATION_MODULE
    IMPLICIT NONE
    PRIVATE

    TYPE :: PROJ_DATA_TYPE
        CHARACTER(len=:), ALLOCATABLE :: name
        REAL(DP), ALLOCATABLE :: params(:)
    ENDTYPE



    INTERFACE BILIN_INTERP
        MODULE PROCEDURE BILIN_INTERP_REAL_SCALAR
        MODULE PROCEDURE BILIN_INTERP_COMPLEX_ARRAY
    ENDINTERFACE

    INTERFACE PROJECT_CONDUCTIVITY_CART_TO_CART
        MODULE PROCEDURE PROJECT_CONDUCTIVITY_CART_TO_CART_GEN
        MODULE PROCEDURE PROJECT_CONDUCTIVITY_CART_TO_CART_UN_UN
        MODULE PROCEDURE PROJECT_CONDUCTIVITY_CART_TO_CART_NUN_2_UN
        MODULE PROCEDURE PROJECT_CONDUCTIVITY_CART_TO_CART_UN_2_NUN
    ENDINTERFACE


    PUBLIC :: PROJ_DATA_TYPE, READ_PROJECTION_DATA
    PUBLIC :: PROJECT_VECTOR_FIELD_FROM_CART_TO_SPH_SITE_WISE

    PUBLIC ::  PROJECT_CONDUCTIVITY_SPH_TO_CART
    PUBLIC ::  PROJECT_CONDUCTIVITY_CART_TO_SPH
    PUBLIC ::  PROJECT_CONDUCTIVITY_CART_TO_CART
    PUBLIC ::  PROJECT_CONDUCTIVITY_GEODETIC_TO_GEOCENTRIC
    PUBLIC ::  PROJECT_TOPOGRAPHY_CART_TO_SPH
    PUBLIC ::  PROJECT_TOPOGRAPHY_SPH_TO_CART
    PUBLIC ::  PROJECT_EM_FIELD_CART_TO_SPH
 !   PUBLIC ::  GET_I_THETA_LIN_SEARCH
    PUBLIC ::  BIN_SEARCH_INTERVAL
CONTAINS


    SUBROUTINE PROJECT_CONDUCTIVITY_SPH_TO_CART(SPH_anomaly, Cart_anomaly,  sph_siga, PROJ, cart_siga)
        TYPE (SPH_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Sph_anomaly
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Cart_anomaly
        REAL(DP), INTENT(IN) :: sph_siga(:,:,:)
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        REAL(DP), INTENT(INOUT) :: cart_siga(:,:,:)
        INTEGER, PARAMETER :: Ngauss=8
        INTEGER :: Ix, Iy, Ixg, Iyg
        REAL(DP) :: xg(Ngauss), Wx(Ngauss)
        REAL(DP) ::  yg(Ngauss), Wy(Ngauss)
        REAL(DP) :: x, y, sg(SIZE(cart_siga,1)), sg_sum(SIZE(cart_siga,1))
        CALL GET_GAUSS_WEIGHTS_NODES(R_ZERO, Cart_anomaly%dx, xg, Wx)
        Wx=Wx/Cart_anomaly%dx
        CALL GET_GAUSS_WEIGHTS_NODES(R_ZERO, Cart_anomaly%dy, yg, Wy)
        Wy=Wy/Cart_anomaly%dy
        DO Iy=1, Cart_anomaly%Ny
            DO Ix=1, Cart_anomaly%Nx
                sg_sum=R_ZERO
                CELL_LOOP: DO Iyg=1, Ngauss
                    y=Cart_anomaly%dy*(Iy-1)-Cart_anomaly%Y_origin+yg(Iyg)
                    DO Ixg=1, Ngauss
                        x=Cart_anomaly%dx*(Ix-1)-Cart_anomaly%X_origin+xg(Ixg)
                        CALL PROJECT_PWC_SCALAR_FIELD_FROM_SPH_TO_CART(&
                            x,y, PROJ, SPH_anomaly, sph_siga, sg)
                        IF (ANY (sg < R_ZERO)) THEN
                            sg=cart_siga(:, Ix, Iy)
                        ENDIF
                        sg_sum=sg_sum+sg*Wx(Ixg)*Wy(Iyg)
                    ENDDO
                ENDDO CELL_LOOP
                cart_siga(:,Ix, Iy)=sg_sum
            ENDDO
        ENDDO
    ENDSUBROUTINE

    SUBROUTINE PROJECT_CONDUCTIVITY_GEODETIC_TO_GEOCENTRIC(domain_in, domain_out,  siga_in, PROJ, siga_out)
        TYPE (SPH_DOMAIN_SHAPE_TYPE), INTENT(IN) :: domain_in, domain_out
        REAL(DP), INTENT(IN) :: siga_in(:,:,:)
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        REAL(DP), INTENT(INOUT) :: siga_out(:,:,:)
        INTEGER, PARAMETER :: Ngauss=8
        INTEGER :: Iphi, Itheta, Ipg, Itg
        REAL(DP) :: phi_g(Ngauss), Wphi(Ngauss)
        REAL(DP) ::  theta_g(Ngauss), Wtheta(Ngauss)
        REAL(DP) :: phi, theta, sg(SIZE(siga_out,1)), sg_sum(SIZE(siga_out,1))
        REAL(DP) :: dtheta2, stheta2
        CALL GET_GAUSS_WEIGHTS_NODES(R_ZERO,domain_out%dphi, phi_g, Wphi)
        Wphi=Wphi/domain_out%dphi
        !$OMP PARALLEL DEFAULT(SHARED), PRIVATE(&
        !$OMP & Itheta, theta_g, Wtheta, stheta2, dtheta2, &
        !$OMP & Iphi, sg_sum, &
        !$OMP & Ipg, phi, Itg, theta, sg)
        !$OMP DO SCHEDULE(DYNAMIC)
        DO Itheta=1, domain_out%Ntheta
            CALL GET_GAUSS_WEIGHTS_NODES(domain_out%thetas(Itheta-1:Itheta), theta_g, Wtheta)
            dtheta2=(domain_out%thetas(Itheta)-domain_out%thetas(Itheta-1))/R_TWO
            stheta2=(domain_out%thetas(Itheta)+domain_out%thetas(Itheta-1))/R_TWO
            Wtheta=Wtheta*SIN(theta_g)/SIN(dtheta2)/SIN(stheta2)/R_TWO
            DO Iphi=1, domain_out%Nphi
                sg_sum=R_ZERO
                CELL_LOOP: DO Ipg=1, Ngauss
                    phi=domain_out%dphi*(Iphi-1)+domain_out%phi0+phi_g(Ipg)
                    DO Itg=1, Ngauss
                        theta=theta_g(Itg)
                        CALL PROJECT_PWC_SCALAR_FIELD_FROM_SPH_TO_CART(&
                            theta, phi, PROJ, domain_in, siga_in, sg)
                        IF (ANY (sg < R_ZERO)) THEN
                            sg=siga_out(:, Itheta, Iphi)
                        ENDIF
                        sg_sum=sg_sum+sg*Wphi(Ipg)*Wtheta(Itg)
                    ENDDO
                ENDDO CELL_LOOP
                siga_out(:,Itheta, Iphi)=sg_sum
            ENDDO
        ENDDO
        !$OMP ENDDO
        !$OMP ENDPARALLEL
    ENDSUBROUTINE

    SUBROUTINE PROJECT_CONDUCTIVITY_CART_TO_SPH(Cart_anomaly, Sph_anomaly,  cart_siga, PROJ, sph_siga)
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Cart_anomaly
        TYPE (SPH_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Sph_anomaly
        REAL(DP), INTENT(IN) :: cart_siga(:,:,:)
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        REAL(DP), INTENT(INOUT) :: sph_siga(:,:,:)
        INTEGER, PARAMETER :: Ngauss=8
        INTEGER :: Iphi, Itheta, Ipg, Itg
        REAL(DP) :: phi_g(Ngauss), Wphi(Ngauss)
        REAL(DP) ::  theta_g(Ngauss), Wtheta(Ngauss)
        REAL(DP) :: phi, theta, sg(SIZE(sph_siga,1)), sg_sum(SIZE(sph_siga,1))
        REAL(DP) :: dtheta2, stheta2
        CALL GET_GAUSS_WEIGHTS_NODES(R_ZERO,Sph_anomaly%dphi, phi_g, Wphi)
        Wphi=Wphi/Sph_anomaly%dphi
        DO Itheta=1, Sph_anomaly%Ntheta
            CALL GET_GAUSS_WEIGHTS_NODES(Sph_anomaly%thetas(Itheta-1:Itheta), theta_g, Wtheta)
            dtheta2=(Sph_anomaly%thetas(Itheta)-Sph_anomaly%thetas(Itheta-1))/R_TWO
            stheta2=(Sph_anomaly%thetas(Itheta)+Sph_anomaly%thetas(Itheta-1))/R_TWO
            Wtheta=Wtheta*SIN(theta_g)/SIN(dtheta2)/SIN(stheta2)/R_TWO
            DO Iphi=1, Sph_anomaly%Nphi
                sg_sum=R_ZERO
                CELL_LOOP: DO Ipg=1, Ngauss
                    phi=Sph_anomaly%dphi*(Iphi-1)+Sph_anomaly%phi0+phi_g(Ipg)
                    DO Itg=1, Ngauss
                        theta=theta_g(Itg)
                        CALL PROJECT_PWC_SCALAR_FIELD_FROM_CART_TO_SPH(&
                            theta, phi, PROJ, CART_anomaly, cart_siga, sg)
                        IF (ANY (sg < R_ZERO)) THEN
                            sg=sph_siga(:, Itheta, Iphi)
                        ENDIF
                        sg_sum=sg_sum+sg*Wphi(Ipg)*Wtheta(Itg)
                    ENDDO
                ENDDO CELL_LOOP
                sph_siga(:,Itheta, Iphi)=sg_sum
            ENDDO
        ENDDO
    ENDSUBROUTINE

    SUBROUTINE PROJECT_CONDUCTIVITY_CART_TO_CART_UN_2_NUN(domain_in, sig_in, Xc_out, Yc_out, Zc_out, sig_out)
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: domain_in
        REAL(DP), INTENT(IN) :: sig_in(:,:,:)
        REAL(DP), INTENT(IN) :: Xc_out(0:), Yc_out(0:), Zc_out(0:)
        REAL(DP), INTENT(INOUT) :: sig_out(:,:,:)

        REAL(DP), ALLOCATABLE :: Xc_in(:), Yc_in(:)
        INTEGER :: I

        ALLOCATE(Xc_in(0:domain_in%Nx), Yc_in(0:domain_in%Ny))

        DO I=0, domain_in%Nx
            Xc_in(I)=-domain_in%X_origin+ I*domain_in%dx
        ENDDO
        DO I=0, domain_in%Ny
            Yc_in(I)=-domain_in%Y_origin+ I*domain_in%dy
        ENDDO

        CALL PROJECT_CONDUCTIVITY_CART_TO_CART(Xc_in, Yc_in, domain_in%z, sig_in, &
            Xc_out, Yc_out, Zc_out, sig_out)

    ENDSUBROUTINE

    SUBROUTINE PROJECT_CONDUCTIVITY_CART_TO_CART_NUN_2_UN(Xc_in, Yc_in, Zc_in, sig_in, domain_out, sig_out)
        REAL(DP), INTENT(IN) :: Xc_in(0:), Yc_in(0:), Zc_in(0:)
        REAL(DP), INTENT(IN) :: sig_in(:,:,:)
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: domain_out
        REAL(DP), INTENT(INOUT) :: sig_out(:,:,:)

        REAL(DP), ALLOCATABLE :: Xc_out(:), Yc_out(:)
        INTEGER :: I

        ALLOCATE(Xc_out(0:domain_out%Nx), Yc_out(0:domain_out%Ny))

        DO I=0, domain_out%Nx
            Xc_out(I)=-domain_out%X_origin+ I*domain_out%dx
        ENDDO

        DO I=0, domain_out%Ny
            Yc_out(I)=-domain_out%Y_origin+ I*domain_out%dy
        ENDDO

        CALL PROJECT_CONDUCTIVITY_CART_TO_CART(Xc_in, Yc_in, Zc_in, sig_in, &
            Xc_out, Yc_out, domain_out%z, sig_out)

    ENDSUBROUTINE

    SUBROUTINE PROJECT_CONDUCTIVITY_CART_TO_CART_UN_UN(domain_in, sig_in, domain_out, sig_out)
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: domain_in
        REAL(DP), INTENT(IN) :: sig_in(:,:,:)
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: domain_out
        REAL(DP), INTENT(INOUT) :: sig_out(:,:,:)

        REAL(DP), ALLOCATABLE :: Xc_in(:), Yc_in(:)
        REAL(DP), ALLOCATABLE :: Xc_out(:), Yc_out(:)
        INTEGER :: I

        ALLOCATE(Xc_in(0:domain_in%Nx), Yc_in(0:domain_in%Ny))
        ALLOCATE(Xc_out(0:domain_out%Nx), Yc_out(0:domain_out%Ny))

        DO I=0, domain_in%Nx
            Xc_in(I)=-domain_in%X_origin+ I*domain_in%dx
        ENDDO
        DO I=0, domain_out%Nx
            Xc_out(I)=-domain_out%X_origin+ I*domain_out%dx
        ENDDO
        DO I=0, domain_in%Ny
            Yc_in(I)=-domain_in%Y_origin+ I*domain_in%dy
        ENDDO
        DO I=0, domain_out%Ny
            Yc_out(I)=-domain_out%Y_origin+ I*domain_out%dy
        ENDDO
        CALL PROJECT_CONDUCTIVITY_CART_TO_CART(Xc_in, Yc_in, domain_in%z, sig_in, &
            Xc_out, Yc_out, domain_out%z, sig_out)

    ENDSUBROUTINE

    SUBROUTINE PROJECT_CONDUCTIVITY_CART_TO_CART_GEN(Xc_in, Yc_in, Zc_in, sig_in, Xc_out, Yc_out, Zc_out, sig_out)
        REAL(DP), INTENT(IN) :: Xc_in(0:), Yc_in(0:), Zc_in(0:)
        REAL(DP), INTENT(IN) :: sig_in(:,:,:)
        REAL(DP), INTENT(IN) :: Xc_out(0:), Yc_out(0:), Zc_out(0:)
        REAL(DP), INTENT(INOUT) :: sig_out(:,:,:)
        INTEGER, PARAMETER :: Ngauss=16
        INTEGER :: Ix, Iy, Iz
        INTEGER, ALLOCATABLE :: Ix1(:,:), Iy1(:,:), Iz1(:,:)
        INTEGER :: Ixg, Iyg, Izg
        REAL(DP), ALLOCATABLE ::  Wx(:,:)
        REAL(DP), ALLOCATABLE ::  Wy(:,:)
        REAL(DP), ALLOCATABLE ::  Wz(:,:)
        REAL(DP) :: tmp_g(Ngauss)
        REAL(DP) :: sig0, sig_av

        ALLOCATE( Wz(Ngauss, SIZE(sig_out,1)), Iz1(Ngauss, SIZE(sig_out, 1)))
        ALLOCATE( Wx(Ngauss, SIZE(sig_out,2)), Ix1(Ngauss, SIZE(sig_out, 2)))
        ALLOCATE( Wy(Ngauss, SIZE(sig_out,3)), Iy1(Ngauss, SIZE(sig_out, 3)))

        DO Iz=1, SIZE(sig_out, 1)
            CALL GET_GAUSS_WEIGHTS_NODES(Zc_out(Iz-1:Iz), tmp_g, Wz(:, Iz))
            Wz(:, Iz)=Wz(:, Iz)/(Zc_out(Iz)-Zc_out(Iz-1))
            DO Izg=1, Ngauss
                Iz1(Izg, Iz)=BIN_SEARCH_INTERVAL(Zc_in, tmp_g(Izg))
            ENDDO
        ENDDO

        DO Ix=1, SIZE(sig_out, 2)
            CALL GET_GAUSS_WEIGHTS_NODES(Xc_out(Ix-1:Ix), tmp_g, Wx(:, Ix))
            Wx(:, Ix)=Wx(:, Ix)/(Xc_out(Ix)-Xc_out(Ix-1))
            DO Ixg=1, Ngauss
                Ix1(Ixg, Ix)=BIN_SEARCH_INTERVAL(Xc_in, tmp_g(Ixg))
            ENDDO
        ENDDO

        DO Iy=1, SIZE(sig_out, 3)
            CALL GET_GAUSS_WEIGHTS_NODES(Yc_out(Iy-1:Iy), tmp_g, Wy(:, Iy))
            Wy(:, Iy)=Wy(:, Iy)/(Yc_out(Iy)-Yc_out(Iy-1))
            DO Iyg=1, Ngauss
                Iy1(Iyg, Iy)=BIN_SEARCH_INTERVAL(Yc_in, tmp_g(Iyg))
            ENDDO
        ENDDO

        !$OMP PARALLEL DEFAULT(SHARED), PRIVATE(&
        !$OMP & Ix, Iy, Iz, &
        !$OMP & Ixg, Iyg, Izg,  &
        !$OMP & sig0, sig_av) 
        !$OMP DO SCHEDULE(DYNAMIC) COLLAPSE(3)
        DO Iy=1, SIZE(sig_out, 3)
            DO Ix=1, SIZE(sig_out, 2)
                DO Iz=1, SIZE(sig_out, 1)
                    sig_av=R_ZERO
                    DO Iyg=1, Ngauss
                        DO Ixg=1, Ngauss
                            DO Izg=1, Ngauss
                                IF (Ix1(Ixg, Ix)>0  .AND. Iy1(Iyg, Iy)>0 .AND.  Iz1(Izg, Iz)>0 ) THEN
                                    sig0=sig_in(Iz1(Izg, Iz), Ix1(Ixg, Ix), Iy1(Iyg, Iy))
                                ELSE
                                    sig0=sig_out(Iz, Ix, Iy)
                                ENDIF
                                sig_av=sig_av+sig0*Wx(Ixg, Ix)*Wy(Iyg, Iy)*Wz(Izg, Iz)
                            ENDDO
                        ENDDO
                    ENDDO
                    sig_out(Iz, Ix, Iy)=sig_av
                ENDDO
            ENDDO
        ENDDO
       !$OMP ENDDO
        !$OMP ENDPARALLEL
    ENDSUBROUTINE

    SUBROUTINE PROJECT_TOPOGRAPHY_CART_TO_SPH(Cart_domain, Sph_domain,  cart_topo, PROJ, sph_topo)
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Cart_domain
        TYPE (SPH_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Sph_domain
        REAL(DP), INTENT(IN) :: cart_topo(:,:)
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        REAL(DP), INTENT(INOUT) :: sph_topo(:,:)
        INTEGER :: Iphi, Itheta
        REAL(DP) :: phi, theta, d 
        DO Iphi=1, Sph_domain%Nphi
            phi=(Sph_domain%phis(Iphi-1)+Sph_domain%phis(Iphi))/R_TWO
            DO Itheta=1, Sph_domain%Ntheta
                theta=(Sph_domain%thetas(Itheta-1)+Sph_domain%thetas(Itheta))/R_TWO
                CALL PROJECT_BILIN_SCALAR_FIELD_FROM_CART_TO_SPH(&
                    theta, phi,  PROJ, CART_domain, cart_topo, d)
                sph_topo(Itheta, Iphi)=d
            ENDDO
        ENDDO
    ENDSUBROUTINE

    SUBROUTINE PROJECT_TOPOGRAPHY_SPH_TO_CART(Sph_domain, Cart_domain,  sph_topo, PROJ, cart_topo)
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Cart_domain
        TYPE (SPH_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Sph_domain
        REAL(DP), INTENT(IN) :: sph_topo(:,:)
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        REAL(DP), INTENT(INOUT) :: cart_topo(:,:)
        INTEGER :: Ix, Iy
        REAL(DP) ::x, y, d 
        DO Iy=1, Cart_domain%Ny
            y=Cart_domain%Y_origin+(Iy-1)*Cart_domain%dy+Cart_domain%dy*R_HALF
            DO Ix=1, Cart_domain%Nx
                x=Cart_domain%X_origin+(Ix-1)*Cart_domain%dx+Cart_domain%dx*R_HALF
                CALL PROJECT_BILIN_SCALAR_FIELD_FROM_SPH_TO_CART(&
                    x, y,  PROJ, SPH_domain, sph_topo, d)
                cart_topo(Ix, Iy)=d
            ENDDO
        ENDDO
    ENDSUBROUTINE


    SUBROUTINE PROJECT_PWC_SCALAR_FIELD_FROM_SPH_TO_CART(x,y,  PROJ,SPH_Anomaly, sc_field , val)
        REAL(DP), INTENT(IN) :: x, y
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        TYPE(SPH_DOMAIN_SHAPE_TYPE), INTENT(IN) :: SPH_Anomaly
        REAL(DP), INTENT(IN) ::  sc_field(:, 0:, 0:)
        REAL(DP), INTENT(OUT) :: val(:)
        REAL(DP) :: theta, phi
        INTEGER :: Itheta, Iphi
        CALL PROJ%CART_TO_SPH(x, y, theta, phi, PROJ%add_data)
        val=-HUGE(R_ONE)
        phi=phi-SPH_Anomaly%phi0
        IF (phi<0) RETURN
        Itheta=BIN_SEARCH_INTERVAL(SPH_Anomaly%thetas, theta)
        IF (Itheta <0) RETURN
        Iphi=FLOOR(phi/SPH_Anomaly%dphi)
        IF (SPH_Anomaly%GLOBE) THEN
            Iphi=MODULO(Iphi, SIZE(sc_field,3))
        ELSEIF (Iphi>SPH_Anomaly%Nphi-1) THEN
            RETURN
        ENDIF
        val=sc_field(:, Itheta, Iphi)
    ENDSUBROUTINE

    SUBROUTINE PROJECT_PWC_SCALAR_FIELD_FROM_CART_TO_SPH(theta, phi,  PROJ, CART_Anomaly, sc_field , val)
        REAL(DP), INTENT(IN) :: theta, phi
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        TYPE(CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: CART_Anomaly
        REAL(DP), INTENT(IN) ::  sc_field(:, 0:, 0:)
        REAL(DP), INTENT(OUT) :: val(:)
        REAL(DP) :: x, y
        INTEGER :: Ix, Iy
        CALL PROJ%SPH_TO_CART(theta, phi, x, y, PROJ%add_data)
        val=-HUGE(R_ONE)
        x=x+CART_Anomaly%X_origin
        y=y+CART_Anomaly%Y_origin
        Ix=FLOOR(x/CART_Anomaly%dx)
        Iy=FLOOR(y/CART_Anomaly%dy)
        IF (Ix<0 .OR. Ix>=SIZE(sc_field,2)) RETURN
        IF (Iy<0 .OR. Iy>=SIZE(sc_field,3)) RETURN
        val=sc_field(:, Ix, Iy)
    ENDSUBROUTINE

    SUBROUTINE PROJECT_BILIN_SCALAR_FIELD_FROM_CART_TO_SPH(theta, phi,  PROJ, CART_domain, sc_field , val)
        REAL(DP), INTENT(IN) :: theta, phi
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        TYPE(CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: CART_domain
        REAL(DP), INTENT(IN) ::  sc_field(0:, 0:)
        REAL(DP), INTENT(OUT) :: val
        REAL(DP)  :: v(0:1, 0:1)
        REAL(DP) :: x, y
        INTEGER :: Ix, Iy, Jx, Jy
        CALL PROJ%SPH_TO_CART(theta, phi, x, y, PROJ%add_data)
        x=x-CART_domain%X_origin
        y=y-CART_domain%Y_origin
        IF (x<R_ZERO .OR.  y <R_ZERO) THEN
            val=HUGE(R_ONE)
            RETURN
        ENDIF
        Ix=FLOOR(x/Cart_domain%dx)
        Iy=FLOOR(y/Cart_domain%dy)
        IF (Ix>=SIZE(sc_field,1)-1 .OR.  Iy >=SIZE(sc_field,2)-1) THEN
            val=HUGE(R_ONE)
            RETURN
        ENDIF
        DO Jy=0,1
            DO Jx=0,1
                v(Jx, Jy)=sc_field(Ix+Jx, Iy+Jy)
            ENDDO
        ENDDO
        val=BILIN_INTERP(x,y,CART_domain%dx, CART_domain%dy,v) 
    ENDSUBROUTINE

    SUBROUTINE PROJECT_BILIN_SCALAR_FIELD_FROM_SPH_TO_CART(x,y,  PROJ, SPH_domain, sc_field , val)
        REAL(DP), INTENT(IN) :: x, y
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        TYPE(SPH_DOMAIN_SHAPE_TYPE), INTENT(IN) :: SPH_domain
        REAL(DP), INTENT(IN) ::  sc_field(0:, 0:)
        REAL(DP), INTENT(OUT) :: val
        REAL(DP)  :: v(0:1, 0:1)
        REAL(DP) :: theta, phi, dphi, dtheta
        INTEGER :: Itheta, Iphi, Jx, Jy
        val=HUGE(R_ONE)
        CALL PROJ%CART_TO_SPH(x, y, theta, phi, PROJ%add_data)
        !Itheta=GET_I_THETA_LIN_SEARCH(SPH_domain%thetas, theta)
        Itheta=BIN_SEARCH_INTERVAL(SPH_domain%thetas, theta)
        IF (Itheta <0) RETURN

        phi=phi-SPH_domain%phi0
        dphi=SPH_domain%dphi
        Iphi=FLOOR(phi/dphi)
        IF (Itheta>=SIZE(sc_field,1)-1 .OR.  Iphi>=SIZE(sc_field,2)-1) THEN
            val=HUGE(R_ONE)
            RETURN
        ENDIF
        DO Jy=0,1
            DO Jx=0,1
                v(Jx, Jy)=sc_field(Itheta+Jx, Iphi+Jy)
            ENDDO
        ENDDO
        val=BILIN_INTERP(theta, phi, dtheta, dphi,v) 
    ENDSUBROUTINE

#ifdef old
    FUNCTION GET_I_THETA_LIN_SEARCH(thetas, theta) RESULT(I_theta)
        REAL(DP), INTENT(IN) :: thetas(0:)
        REAL(DP), INTENT(IN) :: theta
        INTEGER :: I_theta
        I_theta=-1
        IF (theta>=thetas(0)) THEN
            IF (theta<=thetas(SIZE(thetas)-1)) THEN
                I_theta=0
                DO WHILE (thetas(I_theta+1)<=theta )
                    I_theta=I_theta+1
                ENDDO
            ELSE
                I_theta=-SIZE(thetas)
                RETURN
            ENDIF
        ENDIF
    ENDFUNCTION
#endif

    SUBROUTINE PROJECT_EM_FIELD_CART_TO_SPH(theta, phi, Cart_domain, PROJ, EH_cart, EH_sph)
        REAL(DP), INTENT(IN) :: theta(:), phi(:)
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Cart_domain
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        COMPLEX(DP), INTENT(IN) :: EH_cart(:,:,:,:)
        COMPLEX(DP), INTENT(OUT) :: EH_sph(:,:,:,:)
        COMPLEX(DP) :: TMP(3, SIZE(EH_sph,1))
        INTEGER :: Itheta, Iphi 
        DO Iphi=1, SIZE(phi)
            DO Itheta=1, SIZE(theta)
                CALL PROJECT_VECTOR_FIELD_FROM_CART_TO_SPH(&
                    theta(Itheta), phi(Iphi), PROJ, Cart_domain, EH_cart , TMP)
                EH_sph(:, Itheta, Iphi, 1)=-TMP(1,:)
                EH_sph(:, Itheta, Iphi, 2)= TMP(2,:)
                EH_sph(:, Itheta, Iphi, 3)=-TMP(3,:)
            ENDDO
        ENDDO
    ENDSUBROUTINE


    SUBROUTINE PROJECT_VECTOR_FIELD_FROM_CART_TO_SPH(theta, phi, PROJ, Cart_domain, v_field , val)
        REAL(DP), INTENT(IN) :: phi, theta
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        TYPE (CART_DOMAIN_SHAPE_TYPE), INTENT(IN) :: Cart_domain
        COMPLEX(DP), INTENT(IN) :: v_field(:, :, 0:, 0:)
        COMPLEX(DP), INTENT(OUT) :: val(:,:)
        COMPLEX(DP) :: v(3, 0:1,0:1), v0(3)
        REAL(DP) :: x, y, angle, ca, sa
        INTEGER :: Ix, Iy, Jx, Jy, J
        CALL PROJ%SPH_TO_CART(theta, phi, x, y, PROJ%add_data)
        x=x+Cart_domain%X_origin-Cart_domain%dx/2.0_DP
        y=y+Cart_domain%Y_origin-Cart_domain%dy/2.0_DP
        IF (x<R_ZERO .OR.  y <R_ZERO) THEN
            val=HUGE(R_ONE)
            RETURN
        ENDIF
        Ix=FLOOR(x/Cart_domain%dx)
        Iy=FLOOR(y/Cart_domain%dy)
        IF (Ix>=SIZE(v_field,3)-1 .OR.  Iy >=SIZE(v_field,4)-1) THEN
            val=HUGE(R_ONE)
            RETURN
        ENDIF
        angle= PROJ%GET_ANGLE_SPH(theta, phi, PROJ%add_data)
        ca=COS(angle)
        sa=SIN(angle)
        DO J=1, SIZE(v_field,2)
            DO Jy=0,1
                DO Jx=0,1
                    v(:, Jx, Jy)=v_field(:, J, Ix+Jx, Iy+Jy)
                ENDDO
            ENDDO
            v0=BILIN_INTERP(x, y, Cart_domain%dx, Cart_domain%dy, v)
            val(1, J)=v0(1)*ca+v0(2)*sa
            val(2, J)=-v0(1)*sa+v0(2)*ca
            val(3, J)=v0(3)
        ENDDO
    ENDSUBROUTINE

    SUBROUTINE PROJECT_VECTOR_FIELD_FROM_CART_TO_SPH_SITE_WISE(x, y, PROJ, v_in , theta, phi, v_out)
        REAL(DP), INTENT(IN) :: x, y
        TYPE(PROJ_SUB), INTENT(IN) :: PROJ
        COMPLEX(DP), INTENT(IN) :: v_in(:)
        COMPLEX(DP), INTENT(OUT) :: v_out(:)
        REAL(DP), INTENT(OUT) :: phi, theta
        REAL(DP) ::  angle, ca, sa
        INTEGER ::  J

        CALL PROJ%CART_TO_SPH(x, y, theta,  phi, PROJ%add_data)
        angle= PROJ%GET_ANGLE_SPH(theta, phi, PROJ%add_data)

        ca=COS(angle)
        sa=SIN(angle)
        DO J=0, SIZE(v_in)-1, 3
            v_out(J+1)=v_in(J+1)*ca+v_in(J+2)*sa
            v_out(J+2)=-v_in(J+1)*sa+v_in(J+2)*ca
            v_out(J+3)=v_in(J+3)
        ENDDO
    ENDSUBROUTINE

   PURE FUNCTION BILIN_INTERP_COMPLEX_ARRAY(x, y, dx, dy, v) RESULT(RES)
        REAL(DP), INTENT(IN) :: x, y, dx, dy
        COMPLEX(DP), INTENT(IN) :: v(:, 0:, 0:)
        COMPLEX(DP) :: RES(SIZE(v,1))
        REAL(DP) :: x_loc, y_loc
        x_loc=x/dx
        y_loc=y/dy
        x_loc=x_loc-FLOOR(x_loc)
        y_loc=y_loc-FLOOR(y_loc)
        RES=((v(:,1,1)-v(:,1,0)-v(:,0,1)+v(:, 0, 0))*x_loc+v(:,0,1)&
            -v(:,0,0))*y_loc+(v(:,1,0)-v(:, 0, 0))*x_loc+v(:,0,0)
    ENDFUNCTION

   PURE FUNCTION BILIN_INTERP_REAL_SCALAR(x, y, dx, dy, v) RESULT(RES)
        REAL(DP), INTENT(IN) :: x, y, dx, dy
        REAL(DP), INTENT(IN) :: v(0:1,0:1)
        REAL(DP) :: RES
        REAL(DP) :: x_loc, y_loc
        x_loc=x/dx
        y_loc=y/dy
        x_loc=x_loc-FLOOR(x_loc)
        y_loc=y_loc-FLOOR(y_loc)
        RES=((v(1,1)-v(1,0)-v(0,1)+v( 0, 0))*x_loc+v(0,1)&
            -v(0,0))*y_loc+(v(1,0)-v(0, 0))*x_loc+v(0,0)
    ENDFUNCTION

    SUBROUTINE READ_PROJECTION_DATA(pr_data, PROJ)
        TYPE(PROJ_DATA_TYPE), INTENT(IN) :: pr_data
        TYPE(PROJ_SUB), INTENT(OUT) :: PROJ
        IF (pr_data%name=="lcc" ) THEN 
            CALL PREPARE_SUB_PROJ_LCC(pr_data%params(1), & 
                pr_data%params(2), &
                pr_data%params(3), &
                pr_data%params(4), &
                pr_data%params(5), PROJ) 
        ELSEIF (TRIM(pr_data%name)=="tm") THEN
            CALL PREPARE_SUB_PROJ_TM(pr_data%params(1), & 
                pr_data%params(2), &
                pr_data%params(3), &
                pr_data%params(4), PROJ)
        ELSE
#ifdef USE_PROJ8
            CALL LOGGER("Projections are performed by PROJ with "//TRIM(pr_data%name))
            CALL PREPARE_SUB_PROJ_BY_PROJ8(TRIM(pr_data%name), PROJ )
#endif
        ENDIF
    ENDSUBROUTINE

    FUNCTION BIN_SEARCH_INTERVAL(X, V) RESULT(I_RES)
        REAL(DP),              INTENT(IN) :: X(:)
        REAL(DP),              INTENT(IN) :: V
        INTEGER                       :: I_RES
        INTEGER :: I1, I2

        I1 = 1
        I2 = SIZE(X)

        IF (V <= X(1)) THEN
            I_RES = -1
            RETURN
        ENDIF

        IF (V >= X(SIZE(X))) THEN
            I_RES = -SIZE(X)
            RETURN
        ENDIF
        ! binary search
        DO WHILE (I2 > (I1 + 1))
            I_RES = (I2 + I1) / 2
            IF  (X(I_RES) < V) THEN
                I1 = I_RES
            ELSE IF (X(I_RES) > V) THEN
                I2 = I_RES
            ELSE
                RETURN
            ENDIF
        ENDDO
    ENDFUNCTION
ENDMODULE
