MODULE PROJECTIONS_MODULE
    USE ISO_C_BINDING
    IMPLICIT NONE
    PRIVATE
!-------------------------------------------------------------------------------------!    
#ifdef SINGLE
    INTEGER, PARAMETER :: DP  = SELECTED_REAL_KIND(5) 
#else
#ifdef QUAD
    INTEGER, PARAMETER :: DP  = SELECTED_REAL_KIND(30) 
#else
    INTEGER, PARAMETER :: DP  = SELECTED_REAL_KIND(15) 
#endif
#endif
    REAL(DP),PARAMETER:: R_ZERO =0.0_DP 
    REAL(DP),PARAMETER:: R_ONE =1.0_DP 
    REAL(DP),PARAMETER:: R_TWO =2.0_DP 
    REAL(DP),PARAMETER:: R_HALF =0.5_DP 
    REAL(DP), PARAMETER:: PI =3.1415926535897932384626433832795029_DP 
    REAL(DP), PARAMETER:: HALF_PI =PI/2.0_DP 
    REAL(DP), PARAMETER:: PI2 = PI*R_TWO

#ifdef USE_PROJ8
    TYPE, BIND(C) :: PJ_COORD_F
        REAL(C_DOUBLE) :: coords(4) 
    ENDTYPE

    TYPE, BIND(C) :: PJ_FACTORS_F
        REAL(C_DOUBLE) :: meridional_scale
        REAL(C_DOUBLE) :: parallel_scale
        REAL(C_DOUBLE) :: areal_scale

        REAL(C_DOUBLE) :: angular_distortion
        REAL(C_DOUBLE) :: meridian_parallel_angle
        REAL(C_DOUBLE) :: meridian_convergence

        REAL(C_DOUBLE) :: tissot_semimajor
        REAL(C_DOUBLE) :: tissot_semiminor

        REAL(C_DOUBLE) :: dx_dlam
        REAL(C_DOUBLE) :: dx_dphi
        REAL(C_DOUBLE) :: dy_dlam
        REAL(C_DOUBLE) :: dy_dphi
    ENDTYPE

    ENUM, BIND( C )
        ENUMERATOR :: &
            PJ_FWD   =  1, & 
            PJ_IDENT =  0, & 
            PJ_INV   = -1    
    ENDENUM

    INTERFACE

        FUNCTION proj_context_create_f() bind(c, name='proj_context_create') RESULT (RES)
            USE ISO_C_BINDING
            TYPE(C_PTR) :: RES
        ENDFUNCTION


        FUNCTION proj_create_f(context, proj_cmd) bind(c, name='proj_create') RESULT (RES)
            USE ISO_C_BINDING
            TYPE(C_PTR), VALUE, INTENT(IN) :: context
            CHARACTER(KIND=C_CHAR), INTENT(IN) :: proj_cmd(*)
            TYPE(C_PTR) :: RES
        ENDFUNCTION

        FUNCTION proj_destroy_f(ptr) bind(c, name='proj_destroy') RESULT (RES)
            USE ISO_C_BINDING
            TYPE(C_PTR), VALUE, INTENT(IN) :: ptr
            TYPE(C_PTR) :: RES
        ENDFUNCTION

        FUNCTION proj_coord_f(x,y,z,t) bind(c, name='proj_coord') RESULT (RES)
            USE ISO_C_BINDING
            IMPORT PJ_COORD_F
            REAL(C_DOUBLE), VALUE, INTENT(IN) :: x,y,z,t
            TYPE(PJ_COORD_F) :: RES
        ENDFUNCTION

        FUNCTION proj_factors_f(proj_ptr, coords) bind(c, name='proj_factors') RESULT (RES)
            USE ISO_C_BINDING
            IMPORT PJ_COORD_F, PJ_FACTORS_F
            TYPE(C_PTR), VALUE, INTENT(IN) :: proj_ptr
            TYPE(PJ_COORD_F), VALUE, INTENT(IN) :: coords
            TYPE(PJ_FACTORS_F) :: RES
        ENDFUNCTION

        FUNCTION proj_trans_f(proj_ptr, proj_dir, coords) bind(c, name='proj_trans') RESULT (RES)
            USE ISO_C_BINDING
            IMPORT PJ_COORD_F, PJ_FWD
            TYPE(C_PTR), VALUE, INTENT(IN) :: proj_ptr
            INTEGER(KIND(PJ_FWD)), VALUE, INTENT(IN) :: proj_dir
            TYPE(PJ_COORD_F), VALUE, INTENT(IN) :: coords
            TYPE(PJ_COORD_F) :: RES
        ENDFUNCTION
    ENDINTERFACE
#endif

!--------------------------------------------------------------------------------------!

    INTEGER, PARAMETER :: LCC_R=1 
    INTEGER, PARAMETER :: LCC_phi0=2  
    INTEGER, PARAMETER :: LCC_n=3     
    INTEGER, PARAMETER :: LCC_rho0=4  
    INTEGER, PARAMETER :: LCC_F=5     

    INTEGER, PARAMETER :: TM_R=1 
    INTEGER, PARAMETER :: TM_phi0=2  
    INTEGER, PARAMETER :: TM_theta0=3     
    INTEGER, PARAMETER :: TM_k0=4  
    INTERFACE

        SUBROUTINE PROJECT_FROM_SPH_TO_CART(theta, phi, x, y, add_data)
            IMPORT DP 
            REAL(DP), INTENT(IN) :: theta, phi
            REAL(DP), INTENT(OUT) :: x, y
            REAL(DP), INTENT(IN) :: add_data(:)
        ENDSUBROUTINE

        SUBROUTINE PROJECT_FROM_CART_TO_SPH(x, y, theta, phi, add_data)
            IMPORT DP 
            REAL(DP), INTENT(IN) :: x, y
            REAL(DP), INTENT(OUT) :: theta, phi
            REAL(DP), INTENT(IN) :: add_data(:)
        ENDSUBROUTINE

        FUNCTION GET_CONVERGENCE_ANGLE(x, y, add_data) RESULT(RES)
            IMPORT DP 
            REAL(DP), INTENT(IN) :: x, y
            REAL(DP), INTENT(IN) :: add_data(:)
            REAL(DP) :: RES
        ENDFUNCTION
    ENDINTERFACE

    TYPE PROJ_SUB
        PROCEDURE(PROJECT_FROM_SPH_TO_CART), POINTER, NOPASS :: SPH_TO_CART=>NULL()
        PROCEDURE(PROJECT_FROM_CART_TO_SPH), POINTER, NOPASS :: CART_TO_SPH=>NULL()
        PROCEDURE(GET_CONVERGENCE_ANGLE), POINTER, NOPASS :: GET_ANGLE_CART=>NULL()
        PROCEDURE(GET_CONVERGENCE_ANGLE), POINTER, NOPASS :: GET_ANGLE_SPH=>NULL()
        REAL(DP) :: add_data(5)
        CHARACTER(len=:), ALLOCATABLE :: proj_cmd
    ENDTYPE

    PUBLIC :: PROJECT_FROM_SPH_TO_CART, PROJECT_FROM_CART_TO_SPH
    PUBLIC :: PROJ_SUB

    PUBLIC :: PREPARE_ADD_DATA_FOR_LCC
    PUBLIC :: PREPARE_ADD_DATA_FOR_TM

    PUBLIC :: TRANSVERSE_MERCATOR_PROJ_SPH_TO_CART
    PUBLIC :: TRANSVERSE_MERCATOR_PROJ_CART_TO_SPH

    PUBLIC :: CONFORMAL_CON_LAMBERT_PROJ_SPH_TO_CART
    PUBLIC :: CONFORMAL_CON_LAMBERT_PROJ_CART_TO_SPH

    PUBLIC :: PREPARE_SUB_PROJ_LCC
    PUBLIC :: PREPARE_SUB_PROJ_TM

#ifdef USE_PROJ8
    PUBLIC :: PREPARE_SUB_PROJ_BY_PROJ8
#endif
CONTAINS

    SUBROUTINE TRANSVERSE_MERCATOR_PROJ_SPH_TO_CART(theta, phi, x,y, add_data)
        REAL(DP), INTENT(IN) :: theta, phi
        REAL(DP), INTENT(OUT) :: x, y
        REAL(DP), INTENT(IN) :: add_data(:)
        REAL(DP) :: R, theta0, dphi, k0
        R=add_data(TM_R)
        dphi=phi-add_data(TM_phi0)
        theta0=add_data(TM_theta0)
        k0=add_data(TM_k0)
        x=R*ATAN(TAN(HALF_PI-theta)/COS(dphi))-R*(HALF_PI-theta0)
        y=R_HALF*R*LOG( (R_ONE+sin(dphi)*SIN(theta))/(R_ONE-SIN(dphi)*SIN(theta)))
        x=x*k0
        y=y*k0
    ENDSUBROUTINE

    SUBROUTINE TRANSVERSE_MERCATOR_PROJ_CART_TO_SPH(x,y, theta, phi, add_data)
        REAL(DP), INTENT(IN) :: x, y
        REAL(DP), INTENT(OUT) :: theta, phi
        REAL(DP), INTENT(IN) :: add_data(:)
        REAL(DP) :: R, theta0,  x1, k0,y1
        R=add_data(TM_R)
        theta0=add_data(TM_theta0)
        k0=add_data(TM_k0)
        x1=x/R/k0+(HALF_PI-theta0)
        y1=y/R/k0

        theta=HALF_PI-ASIN(SIN(x1)/COSH(y1))
        phi=ATAN2(SINH(y1), COS(x1))+add_data(TM_phi0)
        IF (phi<R_ZERO) phi=phi+PI2
        IF (phi > PI2) phi=phi-PI2
    ENDSUBROUTINE

    SUBROUTINE PREPARE_ADD_DATA_FOR_TM(R, theta0, phi0, k0, add_data)
        REAL(DP), INTENT(IN) :: R, theta0, phi0, k0
        REAL(DP), INTENT(OUT) :: add_data(:)

        add_data(TM_R)=R
        add_data(TM_phi0)=phi0
        add_data(TM_theta0)=theta0
        add_data(TM_k0)=k0
    ENDSUBROUTINE

    SUBROUTINE CONFORMAL_CON_LAMBERT_PROJ_SPH_TO_CART(theta, phi, x,y, add_data)
        REAL(DP), INTENT(IN) :: theta, phi
        REAL(DP), INTENT(OUT) :: x, y
        REAL(DP), INTENT(IN) :: add_data(:)
        REAL(DP) :: R,  phi0, n, rho0, F
        REAL(DP) ::  rho
        R=add_data(LCC_R)
        phi0=add_data(LCC_phi0)
        n=add_data(LCC_n)
        rho0=add_data(LCC_rho0)
        F=add_data(LCC_F)

        rho=R*F/( (TAN((PI-theta)/R_TWO))**n)
        x=rho0-rho*COS(n*(phi-phi0))
        y=rho*SIN(n*(phi-phi0))
    ENDSUBROUTINE

    SUBROUTINE CONFORMAL_CON_LAMBERT_PROJ_CART_TO_SPH(x, y, theta, phi, add_data)
        REAL(DP), INTENT(IN) :: x, y
        REAL(DP), INTENT(OUT) :: theta, phi
        REAL(DP), INTENT(IN) :: add_data(:)
        REAL(DP) :: R,  phi0, n, rho0, F
        REAL(DP) ::  rho, x1, s

        R=add_data(LCC_R)
        phi0=add_data(LCC_phi0)
        n=add_data(LCC_n)
        rho0=add_data(LCC_rho0)
        F=add_data(LCC_F)

        x1=rho0-x
        rho=SIGN(SQRT(x1**2+y**2),n)
        rho=(rho/R/F)**(-R_ONE/n)
        theta=PI-R_TWO*ATAN(rho)

        s=SIGN(R_ONE,n)
        phi=ATAN2(s*y,s*x1)

        phi=phi/n+phi0
    ENDSUBROUTINE

    FUNCTION GET_CONVERGENCE_ANGLE_LCC_SPH(theta, phi, add_data) RESULT(RES)
        REAL(DP), INTENT(IN) :: theta, phi
        REAL(DP), INTENT(IN) :: add_data(:)
        REAL(DP) :: RES
        REAL(DP) :: n, phi1, cos_p, sin_p
        n=add_data(LCC_n)
        phi1=phi-add_data(LCC_phi0)
        cos_p=COS(n*phi1)
        sin_p=-SIN(n*phi1)
        RES=ATAN2(sin_p, cos_p)
    ENDFUNCTION

    FUNCTION GET_CONVERGENCE_ANGLE_TM_SPH(theta, phi, add_data) RESULT(RES)
        REAL(DP), INTENT(IN) :: theta, phi
        REAL(DP), INTENT(IN) :: add_data(:)
        REAL(DP) :: RES
        REAL(DP) :: phi1, cos_p, sin_p, cos_t
        phi1=phi-add_data(TM_phi0)
        cos_p=COS(phi1)
        sin_p=SIN(phi1)
        cos_t=COS(theta)
        RES=ATAN2(sin_p*cos_t, cos_p)
    ENDFUNCTION

    SUBROUTINE PREPARE_SUB_PROJ_LCC(R, theta1, theta2, theta0, phi0,PROJ )
        REAL(DP), INTENT(IN) :: R, theta1, theta2, theta0, phi0
        TYPE(PROJ_SUB), INTENT(INOUT) :: PROJ
        CALL PREPARE_ADD_DATA_FOR_LCC(R, theta1, theta2, theta0, phi0, PROJ%add_data)
        PROJ%CART_TO_SPH=>CONFORMAL_CON_LAMBERT_PROJ_CART_TO_SPH
        PROJ%SPH_TO_CART=>CONFORMAL_CON_LAMBERT_PROJ_SPH_TO_CART
        PROJ%GET_ANGLE_SPH=>GET_CONVERGENCE_ANGLE_LCC_SPH
        PROJ%GET_ANGLE_CART=>NULL()
    ENDSUBROUTINE

    SUBROUTINE PREPARE_SUB_PROJ_TM(R, theta0, phi0,k0,  PROJ )
        REAL(DP), INTENT(IN) :: R,theta0, phi0, k0
        TYPE(PROJ_SUB), INTENT(INOUT) :: PROJ
        CALL PREPARE_ADD_DATA_FOR_TM(R, theta0, phi0, k0, PROJ%add_data)
        PROJ%CART_TO_SPH=>TRANSVERSE_MERCATOR_PROJ_CART_TO_SPH
        PROJ%SPH_TO_CART=>TRANSVERSE_MERCATOR_PROJ_SPH_TO_CART
        PROJ%GET_ANGLE_SPH=>GET_CONVERGENCE_ANGLE_TM_SPH
        PROJ%GET_ANGLE_CART=>NULL()
    ENDSUBROUTINE

    SUBROUTINE PREPARE_ADD_DATA_FOR_LCC(R, theta1, theta2, theta0, phi0, add_data)
        REAL(DP), INTENT(IN) :: R, theta1, theta2, theta0, phi0
        REAL(DP), INTENT(OUT) :: add_data(:)
        REAL(DP) :: n,  rho0, F
        n=LOG(SIN(theta1)/SIN(theta2))
        n=n/LOG( TAN ((PI-theta2)/R_TWO)/TAN((PI-theta1)/R_TWO))
        F=SIN(theta1)*(TAN((PI-theta1)/R_TWO))**n/n
        rho0=R*F/( (TAN((PI-theta0)/R_TWO))**n)

        add_data(LCC_R)=R
        add_data(LCC_phi0)=phi0
        add_data(LCC_n)=n
        add_data(LCC_rho0)=rho0
        add_data(LCC_F)=F
    ENDSUBROUTINE

#ifdef USE_PROJ8
        SUBROUTINE PREPARE_SUB_PROJ_BY_PROJ8(proj_cmd, PROJ )
            CHARACTER(len=*), INTENT(IN) :: proj_cmd
            TYPE(PROJ_SUB), INTENT(OUT) :: PROJ
            TYPE(C_PTR) :: proj8_ptr
            INTEGER :: K
            CHARACTER(len=*), PARAMETER ::GC='geoc'

            proj8_ptr=PROJ8_CREATE_PROJECTION(proj_cmd)
            PROJ%add_data=TRANSFER(proj8_ptr, PROJ%add_data)
            K=INDEX(proj_cmd, GC)
            IF (K==0) THEN
                PROJ%CART_TO_SPH=>PROJ8_PROJECT_CART_TO_SPH
                PROJ%SPH_TO_CART=>PROJ8_PROJECT_SPH_TO_CART
            ELSE
                PROJ%CART_TO_SPH=>PROJ8_PROJECT_GEODETIC_TO_GEOCENTRIC_BWD
                PROJ%SPH_TO_CART=>PROJ8_PROJECT_GEODETIC_TO_GEOCENTRIC_FWD
            ENDIF
            PROJ%GET_ANGLE_SPH=>PROJ8_GET_CONVERGENCE_ANGLE
            PROJ%GET_ANGLE_CART=>NULL()
            ALLOCATE(CHARACTER(len=LEN_TRIM(proj_cmd))::PROJ%proj_cmd)
            PROJ%proj_cmd=proj_cmd
        ENDSUBROUTINE

        FUNCTION PROJ8_CREATE_PROJECTION(proj_params) RESULT(RES)
            CHARACTER(len=*), INTENT(IN) :: proj_params
            TYPE(C_PTR) :: RES
            TYPE(C_PTR) :: context
            context=proj_context_create_f()
            RES=proj_create_f(context, proj_params//C_NULL_CHAR)
        ENDFUNCTION

        SUBROUTINE PROJ8_PROJECT_SPH_TO_CART(theta, phi, x, y, add_data)
            REAL(C_DOUBLE), INTENT(IN) :: theta, phi
            REAL(C_DOUBLE), INTENT(OUT) :: x, y
            REAL(DP), INTENT(IN) :: add_data(:)
            TYPE(C_PTR) :: proj_ptr
            TYPE(PJ_COORD_F):: coords_in
            TYPE(PJ_COORD_F):: coords_out
            proj_ptr=TRANSFER(add_data, proj_ptr)
            coords_in=proj_coord_f(phi, HALF_PI-theta, 0d0, 0d0)
            coords_out = proj_trans_f(proj_ptr, PJ_FWD, coords_in)
            y=coords_out%coords(1)
            x=coords_out%coords(2)
        ENDSUBROUTINE

        SUBROUTINE PROJ8_PROJECT_CART_TO_SPH(x, y,  theta, phi, add_data)
            REAL(C_DOUBLE), INTENT(IN) :: x, y
            REAL(C_DOUBLE), INTENT(OUT) :: theta, phi
            REAL(DP), INTENT(IN) :: add_data(:)
            TYPE(C_PTR) :: proj_ptr
            TYPE(PJ_COORD_F):: coords_in
            TYPE(PJ_COORD_F):: coords_out
            proj_ptr=TRANSFER(add_data, proj_ptr)
            coords_in=proj_coord_f(y, x, 0d0, 0d0)
            coords_out = proj_trans_f(proj_ptr, PJ_INV, coords_in)
            phi=coords_out%coords(1)
            IF (phi<0) phi=phi+ PI2
            theta=HALF_PI-coords_out%coords(2)
        ENDSUBROUTINE

        FUNCTION PROJ8_GET_CONVERGENCE_ANGLE(theta, phi, add_data) RESULT(RES)
            REAL(C_DOUBLE), INTENT(IN) :: theta, phi
            REAL(DP), INTENT(IN) :: add_data(:)
            TYPE(C_PTR) :: proj_ptr
            REAL(C_DOUBLE) :: RES
            TYPE(PJ_COORD_F):: coords_in
            TYPE(PJ_FACTORS_F):: factors_out
            proj_ptr=TRANSFER(add_data, proj_ptr)
            coords_in=proj_coord_f(phi, HALF_PI-theta, 0d0, 0d0)
            factors_out = proj_factors_f(proj_ptr, coords_in)
            RES= factors_out%meridian_convergence
        ENDFUNCTION


        SUBROUTINE PROJ8_PROJECT_GEODETIC_TO_GEOCENTRIC_FWD(theta, phi, theta_out, phi_out, add_data)
            REAL(C_DOUBLE), INTENT(IN) :: theta, phi
            REAL(C_DOUBLE), INTENT(OUT) :: theta_out, phi_out
            REAL(DP), INTENT(IN) :: add_data(:)
            CALL PROJ8_PROJECT_GEODETIC_TO_GEOCENTRIC(theta, phi, theta_out, phi_out, add_data, PJ_FWD)
        ENDSUBROUTINE

        SUBROUTINE PROJ8_PROJECT_GEODETIC_TO_GEOCENTRIC_BWD(theta, phi, theta_out, phi_out, add_data)
            REAL(C_DOUBLE), INTENT(IN) :: theta, phi
            REAL(C_DOUBLE), INTENT(OUT) :: theta_out, phi_out
            REAL(DP), INTENT(IN) :: add_data(:)
            CALL PROJ8_PROJECT_GEODETIC_TO_GEOCENTRIC(theta, phi, theta_out, phi_out, add_data, PJ_INV)
        ENDSUBROUTINE

        SUBROUTINE PROJ8_PROJECT_GEODETIC_TO_GEOCENTRIC(theta, phi, theta_out, phi_out, add_data, dir)
            REAL(C_DOUBLE), INTENT(IN) :: theta, phi
            REAL(C_DOUBLE), INTENT(OUT) :: theta_out, phi_out
            REAL(DP), INTENT(IN) :: add_data(:)
            INTEGER(KIND(PJ_FWD)), INTENT(IN) :: dir
            TYPE(C_PTR) :: proj_ptr
            TYPE(PJ_COORD_F):: coords_in
            TYPE(PJ_COORD_F):: coords_out
            proj_ptr=TRANSFER(add_data, proj_ptr)
            coords_in=proj_coord_f(phi, HALF_PI-theta, 0d0, 0d0)
            coords_out = proj_trans_f(proj_ptr, dir, coords_in)
            phi_out=coords_out%coords(1)
            IF (phi_out<0) phi_out=phi_out+ PI2
            theta_out=HALF_PI-coords_out%coords(2)
        ENDSUBROUTINE



        SUBROUTINE DELETE_PROJECTION_PROJ8(proj_ptr)
            TYPE(C_PTR), INTENT(INOUT) :: proj_ptr
            proj_ptr=proj_destroy_f(proj_ptr)
        ENDSUBROUTINE
#endif
ENDMODULE
