function write_GEMMIE_bin_array(A, fname, force_cmplx)
    fd=fopen(fname,"w+");
	M=size(A);
	N=length(M);

	if M(end)==1
		N=N-1;
		M=M(1:end-1);
	end
	realA=isreal(A) & ~exist("force_cmplx", "var");

	if realA
		N=-N;
	end
	
	if  ~exist("force_cmplx", "var") | force_cmplx>0;
		fwrite(fd,N,'int32');
	end
    fwrite(fd,M,'int32');
	A=A(:);
	if realA
		fwrite(fd, A, 'double');
	else	
		M=zeros([2*length(A), 1]);
		M(1:2:end)=real(A);
		M(2:2:end)=imag(A);
		fwrite(fd,M,'double');
	end
    fclose(fd);    
end 
