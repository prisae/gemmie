function [F_theta, F_phi, F_r, F]=read_GEMMIE_field(fname)
	F=read_GEMMIE_bin_array(fname);
	s=size(F);
	if length(s) ~=3 || isreal(F)
		print("Incorrect file. The file with EM field must contains three-dimensional complex-valued array");
		return;
   	end

	F_theta=reshape(F(1,:,:),s(2:3));
	F_phi=  reshape(F(2,:,:),s(2:3));
	F_r=    reshape(F(3,:,:),s(2:3));
end
