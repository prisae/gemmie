function G=read_GEMMIE_bin_array(fname, NN)
	fname
    fd=fopen(fname,"r");
	if exist("NN", "var") 
		N1=NN;
	else
		N1=fread(fd,1,'int32');
	end
	N=abs(N1)
	if (N1<0) 
		M=fread(fd,N,'int32')
		A=fread(fd,prod(M),'double');
		fclose(fd);    
		G=reshape(A,M);
	else
		M=fread(fd,N,'int32');
		
		if exist("NN", "var")
			M=shift(M,1);
		end	
		A=fread(fd,2*prod(M),'double');
		fclose(fd);    
		A=complex(A(1:2:end),A(2:2:end));
		G=reshape(A,M);
	end
end 
