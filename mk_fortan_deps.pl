#!/usr/bin/perl  

use strict;
use warnings;

my $use_rec=1;      #find all fortran sources recoursively (default)
my $use_path=0;     #use user-provided path to find sources
my $use_flist=0;    # list of source files presented by user explisitly
my $in_flist=0;
my $in_ex_flist=0;     
my $in_inc_path_list=0;	


my @flist=();   #list of sources
my %ex_hash=(); #if one provide black list of sources
my @inc_path_list=('-I.');
my $fpath="";

my $deps_file_name="";

my $prog_file="";
my $build_obj_list=0;

my @tailslist=("for","f90","FOR","F90");
my $tails_reg = join "|", @tailslist;

my @ARGV_TMP=@ARGV;
while (@ARGV){

    my $arg=shift;

    if ($arg =~ /^-r$/){
		input_flags_reset();
        $use_rec=1;
    }elsif ($arg =~ /^-p$/){
		input_flags_reset();
        $use_path=1;
        $fpath=shift;
    }elsif ($arg =~ /^-f$/){
		input_flags_reset();
        $use_flist=1;
        $in_flist=1;
        $in_ex_flist=0;
    }elsif ($arg =~ /^-o$/){
        $deps_file_name=shift;
        $in_flist=0;
		$in_inc_path_list=0;	
        $in_ex_flist=0;
    }elsif ($arg =~ /^-t$/){
        $prog_file=shift;
        $build_obj_list=1;
        $in_flist=0;
        $in_ex_flist=0;
		$in_inc_path_list=0;	
    }elsif ($arg =~ /^-e$/){
        $in_ex_flist=1;
        $in_flist=0;
		$in_inc_path_list=0;	
	}elsif ($arg =~ /^--include-paths$/){
        $in_ex_flist=0;
        $in_flist=0;
		$in_inc_path_list=1;	
    }elsif ($in_flist and $arg =~ /\.($tails_reg)$/) { 
        push @flist, fix_paths($arg);
    }elsif ($in_ex_flist and $arg =~ /\.($tails_reg)$/) { 
        my $epath=fix_paths($arg);
        $ex_hash{$epath}=1;
	}elsif ($in_inc_path_list){
        push @inc_path_list, $arg;
    }else{
        print STDERR "Wrong flag for mk_fortran_deps.pl : $arg \n";
		print STDERR "@ARGV_TMP \n";
        exit -1;
    }

}

if ($use_flist and scalar(@flist==0)) { #possible in case of Makefile
    $use_flist=0;
}

if ($use_path and not defined $fpath) { #possible in case of Makefile
    $use_path=0;
}

if (not ($use_rec or $use_path or $use_flist)) { #switch to default behaviour
    $use_rec=1;
}

if ($use_rec) {
    @flist=fortran_src_rec(".");
}elsif ($use_path) {
    my @files_path=split(/:/,$fpath);
    @flist=map( {fortran_src_rec($_,0)} @files_path);
}elsif (not $use_flist){
    print STDERR "Something strange -no way to find files \n";
    exit -2;
}


@flist=uniq (@flist);

@flist  = grep {not $ex_hash{$_}} @flist;

if (not @flist ){
    print STDERR "There are no source files for your set of the  options\n";
    exit -1;
}


$prog_file=fix_paths($prog_file);
if (@inc_path_list) {@inc_path_list=uniq(@inc_path_list)};




my %module_files=();
my %used_modules=();
my %used_includes=();

my %prog_files=();
my $inc_full_path;

foreach my $sourcefile (@flist){
    if (not  open( SOURCEFILE, "$sourcefile")) { 
        print STDERR "Can't open source file: $sourcefile \n";
        exit -1;
    }
    my $module_name="";
    my @modulelist=();
    my @includelist=();
    while (my $line=<SOURCEFILE>){
        if ($sourcefile ne $prog_file ){
            if ($line =~ /^[ \t]*program[ \t]*/io ){
                $prog_files{$sourcefile}=1;
                last;
            }
        }
        if ($line =~ /^[ \t]*module[ \t]+(\w+)/io ){
            $module_name= uc $1;
            if ($module_name ne "PROCEDURE"){
                if (exists $module_files{$module_name} and
                    $module_files{$module_name} ne  $sourcefile){ #The possible situation in case of ifdef
                    print STDERR "Collision! \n";
                    print STDERR "Two different files: $module_files{$module_name},  $sourcefile contain  module $module_name\n";
                    print STDERR "Can't choose the correct one \n";
                    exit -3
                }
                $module_files{$module_name}=$sourcefile;
            }
        }elsif($line =~ /^[ \t]*use[ \t]+(\w+)/io ) { 
            $module_name=uc $1;
            push @modulelist,  $module_name
        }elsif ($line =~ /^[ \t]*#?include[ \t]+[\"\']([^\"\']+)[\"\']/io){
			#if (not exists $external_incs{$1}) {
			#        push @includelist, $1 if ( -e $1);
			#}
			$inc_full_path=is_inc_exists($1, \@inc_path_list);
			
            push @includelist, $inc_full_path if $inc_full_path;
        }
    }
    close(SOURCEFILE);
    @modulelist = uniq (@modulelist);
    @includelist = uniq (@includelist);

    $used_modules{$sourcefile}=[ @modulelist ];
    $used_includes{$sourcefile}=[ @includelist ];
}


open (my $fh, '>&', STDOUT);

if ($deps_file_name  ne "" ){
    if (not open($fh,">>",$deps_file_name) ){
            print STDERR "Could not create file $deps_file_name ";
            exit -1;
    }
}


my @objlist=();

my %objs=();

if ($build_obj_list) {
    %objs = get_target_deps($prog_file, \%used_modules, \%module_files, \%objs);
    @objlist =keys %objs;
    print STDOUT "@objlist \n";
}

foreach my $sourcefile (@flist){
    if (exists $prog_files{$sourcefile} ){
        next;
    }
    my $objfile=obj_name($sourcefile);
	if ( not exists $objs{$objfile}){
		next;
	}
    my @deplist=();
    foreach  my $mf (@{$used_modules{$sourcefile}}) {
        my $obj=$module_files{$mf};
        if (defined $module_files{$mf} and $obj ne $sourcefile ) {
            $obj=obj_name($obj);
            push @deplist, $obj
        }
    }
    if (@deplist){
        push @deplist, @{$used_includes{$sourcefile}};
        @deplist =uniq(@deplist);
        print {$fh}  "$objfile: @deplist \n";
    }else{
        if (@{$used_includes{$sourcefile}}) {
            print {$fh}  "$sourcefile: @{$used_includes{$sourcefile}} \n";
        }
    }
}



exit 0;

#================== SUBROUTINES ========================

sub input_flags_reset{ #operate with global variables
	$use_rec=0;      
	$use_path=0;     
	$use_flist=0;    
	
	$in_flist=0;
	$in_ex_flist=0;    
   	$in_inc_path_list=0;	
}

sub fix_paths{
    my @paths=();
    foreach my $p (@_) {
        $p =~ s/\/+/\//g;
        $p =~ s/\.\///g;
        push @paths, $p
    }

    if (scalar (@_)==1 ){
        return $paths[0];
    }
    return @paths;
}


sub uniq{
    return sort keys %{{map { $_ => 1 } @_}};
}

sub obj_name{
    (my $fname =$_[0] ) =~s/\.($tails_reg)$/\.o/o ;
    return $fname ;
}

sub fortran_src_rec{
    my ($start_path, $not_rec) = @_;
    my @src=();
    foreach my $file (glob "$start_path/*"){
        if (-f $file) {
            if ( $file =~ /\.($tails_reg)$/ ){
                $file =fix_paths ($file);
                push  @src, $file;
            }
        }elsif( (-d $file) and not $not_rec ){
            push @src, fortran_src_rec($file);
        }

    }
    return @src ;
}

sub get_target_deps{
    my $target=shift;
    my $used_modules=shift;
    my $module_files=shift;
    my %res=%{shift()};

    my $obj= obj_name($target);
  
    if (exists $res{$obj} ){ return %res};

    foreach  my $mod (@{$$used_modules{$target}}) {
        if ( $$module_files{$mod}  ){
			
			%res=get_target_deps($$module_files{$mod}, $used_modules, $module_files, \%res) if 
				($$module_files{$mod} ne $target)   
		}
    }
    $res{$obj}=1;
    return %res;
}

sub is_inc_exists{
    my $fname=shift;
    my @paths_list=@{shift()};
	my $full_path;
	foreach my $p (@paths_list){
		$full_path=$p;
		$full_path =~s /-I//og ;
		$full_path =~s /^\./\.\//og ;
		$full_path = $full_path . '/' .$fname;
		$full_path =fix_paths($full_path);
		if ( -e $full_path) {return $full_path};
	};
	return ''
}
