! Copyright (c) 2012 Joseph A. Levin
!
! Permission is hereby granted, free of charge, to any person obtaining a copy of this
! software and associated documentation files (the "Software"), to deal in the Software
! without restriction, including without limitation the rights to use, copy, modify, merge,
! publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
! persons to whom the Software is furnished to do so, subject to the following conditions:
!
! The above copyright notice and this permission notice shall be included in all copies or 
! substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
! INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
! PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
! LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
! OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
! DEALINGS IN THE SOFTWARE.

!     
! File:   fson_path_m.f95
! Author: Joseph A. Levin
!
! Created on March 10, 2012, 11:01 PM
!
! Current modification was done by Mikhail Kruglyakov (m.kruglyakov@gmail.com) for the sake of
! 1)Strict compatibilty with  Fortan 2003 standard
! 2)Independency of compiler
! 3)Building without any warnings in case of -Wall -Wextra -Wpedantic
! Modified on August 15, 2019



module fson_path_m

    use iso_c_binding
    use fson_value_m 
    use fson_string_m
    implicit none
    private

#ifdef __PGI
#define NO_QP
#endif

    public :: fson_path_get

    interface fson_path_get
        module procedure get_by_path
        module procedure get_integer32
        module procedure get_integer64
        module procedure get_real_sp
        module procedure get_real_dp
#ifndef NO_QP
        module procedure get_real_qp
#endif
        module procedure get_logical
        module procedure get_chars
        module procedure get_array_1d_integer32
        module procedure get_array_1d_integer64
        module procedure get_array_1d_real_sp
        module procedure get_array_1d_real_dp
#ifndef NO_QP
        module procedure get_array_1d_real_qp
#endif
        module procedure get_array_1d_logical
        module procedure get_array_1d_char
    end interface fson_path_get


contains
    !
    ! GET BY PATH
    !
    ! $     = root 
    ! @     = this
    ! .     = child object member
    ! []    = child array element
    !
    recursive subroutine get_by_path(this, path, p)
        type(fson_value), pointer,intent(in) :: this        
        character(len=*),intent(in) :: path
        type(fson_value), pointer,intent(inout) ::  p        
        integer :: i, length, child_i
        character :: c
        logical :: array       

        ! default to assuming relative to this
        p => this

        child_i = 1          
        array = .false.

        length = len_trim(path)

        do i=1, length
            c = path(i:i)   
            select case (c)
            case ("$")
                ! root
                do while (associated (p % parent))
                    p => p % parent
                end do
                child_i = i + 1
            case ("@")
                ! this                    
                p => this
                child_i = i + 1
            case (".", "[")                    
                ! get child member from p                          
                if (child_i < i) then                          
                    p => fson_value_get(p, path(child_i:i-1))
                else
                    child_i = i + 1
                    cycle
                end if

                if(.not.associated(p)) then
                    return                                        
                end if

                child_i = i+1

                ! check if this is an array
                ! if so set the array flag
                if (c == "[") then
                    ! start looking for the array element index
                    array = .true.
                end if
            case ("]")
                if (.not.array) then
                    call error_stop( "Unexpected ], not missing preceding [")
                end if
                array = .false.
                child_i = parse_integer(path(child_i:i-1))                                                
                p => fson_value_get(p, child_i)                                                                                                                    

                child_i= i + 1
            case default
                cycle                    
            end select
        end do

        ! grab the last child if present in the path
        if (child_i <= length) then            
            p => fson_value_get(p, path(child_i:length))
            if(.not.associated(p)) then
                return
            else                
            end if
        end if

    end subroutine get_by_path

    !
    ! PARSE INTEGER
    !
    integer function parse_integer(chars) result(integral)
        character(len=*),intent(in) :: chars
        character :: c
        integer :: tmp, i

        integral = 0        
        do i=1, len_trim(chars)
            c = chars(i:i)            
            select case(c)
            case ("0":"9")
                ! digit        
                read (c, '(i1)') tmp                                               

                ! shift
                if(i > 1) then
                    if (integral<real((huge(integral)-tmp),kind=dp)/10.0_dp) then
                        integral = integral * 10
                    else
                        call error_stop("index is too large for integer")
                    endif
                end if
                ! add
                integral = integral + tmp

            case default                          
                return
            end select            
        end do

    end function parse_integer    

    !
    ! GET INTEGER
    !
    subroutine get_integer32(this, path, value)
        type(fson_value), pointer, intent(in) :: this
        character(len=*), optional,intent(in) :: path
        integer(c_int32_t),intent(out) :: value        
        type(fson_value), pointer ::  p


        nullify(p)                
        if(present(path)) then
            call get_by_path(this=this, path=path, p=p)
        else
            p => this
        end if

        if(.not.associated(p)) then
            call error_stop("Unable to resolve path: "//trim(path))
        end if


        if(p % value_type == TYPE_INTEGER) then
            if ( p % value_integer <HUGE(value) .AND. p % value_integer > -HUGE(value)) then
                value = int(p % value_integer,kind=c_int32_t)
            else
                call error_stop("The integer at: "//trim(path)//"is too large for 4 bytes! ")
            endif
        else
            call error_stop("Unable to resolve value to integer: "//trim(path))
        end if

    end subroutine get_integer32

    subroutine get_integer64(this, path, value)
        type(fson_value), pointer, intent(in) :: this
        character(len=*), optional,intent(in) :: path
        integer(c_intmax_t),intent(out) :: value        
        type(fson_value), pointer ::  p


        nullify(p)                
        if(present(path)) then
            call get_by_path(this=this, path=path, p=p)
        else
            p => this
        end if

        if(.not.associated(p)) then
            call error_stop("Unable to resolve path: "//trim(path))
        end if


        if(p % value_type == TYPE_INTEGER) then
            value = p % value_integer
        else
            call error_stop("Unable to resolve value to integer: "//trim(path))
        end if

    end subroutine get_integer64


    !
    ! GET REAL
    !
    subroutine get_real_sp(this, path, value)
        type(fson_value), pointer,      intent(in) ::  this
        character(len=*), optional,     intent(in) :: path

        real(sp),intent(out) :: value        

        type(fson_value), pointer ::  p


        nullify(p)                

        if(present(path)) then
            call get_by_path(this=this, path=path, p=p)
        else
            p => this
        end if

        if(.not.associated(p)) then
            call error_stop("Unable to resolve path: "//trim(path))
        end if

        if(p % value_type == TYPE_INTEGER) then
            value = real(p % value_integer,kind=sp)
        else if (p % value_type == TYPE_REAL) then
            if (p % value_real <huge(value) .and. p % value_real>-huge(value)) then 
                value = real(p % value_real,kind=sp)
            else
                call error_stop("The real value at: "//trim(path)//"is too large for single precision! ")
            endif
        else
            call error_stop("Unable to resolve value to real: "//trim(path))
        end if

    end subroutine get_real_sp

    subroutine get_real_dp(this, path, value)
        type(fson_value), pointer,      intent(in) ::  this
        character(len=*), optional,     intent(in) :: path

        real(dp),intent(out) :: value        

        type(fson_value), pointer ::  p


        nullify(p)                

        if(present(path)) then
            call get_by_path(this=this, path=path, p=p)
        else
            p => this
        end if

        if(.not.associated(p)) then
            call error_stop("Unable to resolve path: "//trim(path))
        end if

        if(p % value_type == TYPE_INTEGER) then
            value = real(p % value_integer,kind=dp)
        else if (p % value_type == TYPE_REAL) then
            if (p % value_real <huge(value) .and. p % value_real>-huge(value)) then 
                value = real(p % value_real,kind=dp)
            else
                call error_stop("The real value at: "//trim(path)//"is too large for double precision! ")
            endif
        else
            call error_stop("Unable to resolve value to real: "//trim(path))
        end if

    end subroutine get_real_dp

#ifndef NO_QP
    subroutine get_real_qp(this, path, value)
        type(fson_value), pointer,      intent(in) ::  this
        character(len=*), optional,     intent(in) :: path

        real(qp),intent(out) :: value        

        type(fson_value), pointer ::  p


        nullify(p)                

        if(present(path)) then
            call get_by_path(this=this, path=path, p=p)
        else
            p => this
        end if

        if(.not.associated(p)) then
            call error_stop("Unable to resolve path: "//trim(path))
        end if

        if(p % value_type == TYPE_INTEGER) then
            value = real(p % value_integer,kind=qp)
        else if (p % value_type == TYPE_REAL) then
            value = p % value_real
        else
            call error_stop("Unable to resolve value to real: "//trim(path))
        end if

    end subroutine get_real_qp
#endif

    !
    ! GET LOGICAL
    !
    subroutine get_logical(this, path, value)
        type(fson_value), pointer,      intent(in) ::  this
        character(len=*), optional,     intent(in) :: path

        logical,intent(out) :: value        

        type(fson_value), pointer ::  p


        nullify(p)                

        if(present(path)) then
            call get_by_path(this=this, path=path, p=p)
        else
            p => this
        end if

        if(.not.associated(p)) then
            call error_stop("Unable to resolve path: "//trim(path))
        end if


        if (p % value_type == TYPE_LOGICAL) then
            value = p % value_logical
        else
            call error_stop("Unable to resolve value to logical: "//trim(path))
        end if

    end subroutine get_logical

    !
    ! GET CHARS
    !
    subroutine get_chars(this, path, value)
        type(fson_value), pointer,      intent(in) ::  this
        character(len=*), optional,     intent(in) :: path

        character(len=:), allocatable, intent(inout) :: value        
        type(fson_value), pointer ::  p

        nullify(p)                

        if(present(path)) then
            call get_by_path(this=this, path=path, p=p)
        else
            p => this
        end if

        if(.not.associated(p)) then
            call error_stop("Unable to resolve path: "//trim(path))
        end if

        if  (p % value_type == TYPE_STRING) then            
            call fson_string_copy(p % value_string, value)
            else
                call error_stop("Unable to resolve value to string: "//trim(path))
            end if

        end subroutine get_chars

        !
        ! GET ARRAY 1D
        !


        subroutine get_array_1d(this, path, type_marker,array)
            type(fson_value), pointer,intent(in) :: this
            character(len = *), optional,intent(in) :: path
            class(*),intent(in)::type_marker
            class(*),allocatable,intent(out)::array(:)

            type(fson_value), pointer :: p, element, p2
            integer :: i, n, max_len

            nullify(p)                

            ! resolve the path to the value
            if (present(path)) then
                call get_by_path(this=this, path=path, p=p)
            else
                p => this
            end if

            if(.not.associated(p)) then
                call error_stop("Unable to resolve path: "//trim(path))
            end if

            if(p % value_type == TYPE_ARRAY) then            
                n = fson_value_count(p)
                element => p % children
                select type(type_marker)
                type is (integer(c_int32_t));   allocate(integer(c_int32_t)::array(n))
                type is (integer(c_intmax_t));  allocate(integer(c_intmax_t)::array(n))
                type is (real(sp));             allocate(real(sp)::array(n))
                type is (real(dp));             allocate(real(dp)::array(n))
#ifndef NO_QP
                type is (real(qp));             allocate(real(qp)::array(n))
#endif
                type is (logical);              allocate(logical::array(n))
                type is (character(len=*))
                    max_len=-1
                    p2=>element
                    do i=1, n
                        if (p2%value_type==TYPE_STRING) then
                            max_len=MAX(max_len, p2%value_string%str_len)
                        else
                            call error_stop("Unable to resolve value to array of strings: "//trim(path))
                        endif
                        p2 => p2 % next
                    enddo
                    allocate(character(len=len(type_marker))::array(n))
                endselect

                do i = 1, n
                    select type(array)
                    type is (integer(c_int32_t));  call fson_path_get(element, "", array(i))
                    type is (integer(c_intmax_t)); call fson_path_get(element, "", array(i))
                    type is (real(sp));            call fson_path_get(element, "", array(i))
                    type is (real(dp));            call fson_path_get(element, "", array(i))
#ifndef NO_QP
                    type is (real(qp));            call fson_path_get(element, "", array(i))
#endif
                    type is (logical);             call fson_path_get(element, "", array(i))
                    type is (character(len=*));    call get_chars_prealloc(element, "", array(i))
                    endselect
                   element => element % next
               end do
           else
               call error_stop("Resolved value is not an array. "//trim(path))
           end if

           if (associated(p)) nullify(p)

       end subroutine get_array_1d

       !
       ! GET ARRAY INTEGER 1D
       !

       subroutine get_array_1d_integer32(this, path, arr)
           type(fson_value), pointer, intent(in) :: this
           character(len=*),          intent(in) :: path   

           integer(c_int32_t), allocatable, intent(out) :: arr(:)
           integer(c_int32_t)::type_marker
           class(*),allocatable::tmp(:)

           if (allocated(arr)) deallocate(arr)
           call get_array_1d(this, path, type_marker,tmp)
           select type(tmp)
           type is (integer(c_int32_t))
               ALLOCATE(arr(SIZE(tmp))); arr=tmp
           endselect
           if (allocated(tmp)) deallocate(tmp)

       end subroutine get_array_1d_integer32

       subroutine get_array_1d_integer64(this, path, arr)
           type(fson_value), pointer, intent(in) :: this
           character(len=*),          intent(in) :: path   

           integer(c_int64_t), allocatable, intent(out) :: arr(:)

           integer(c_int64_t)::type_marker
           
           class(*),allocatable::tmp(:)

           if (allocated(arr)) deallocate(arr)
           call get_array_1d(this, path, type_marker,tmp)
           select type(tmp)
           type is (integer(c_int64_t))
               ALLOCATE(arr(SIZE(tmp))); arr=tmp
           endselect
           if (allocated(tmp)) deallocate(tmp)

       end subroutine get_array_1d_integer64

       !
       ! GET ARRAY REAL 1D

       subroutine get_array_1d_real_sp(this, path, arr)
           type(fson_value), pointer, intent(in) :: this
           character(len=*),          intent(in) :: path   

           real(sp), allocatable, intent(out) :: arr(:)

           real(sp)::type_marker
           class(*),allocatable::tmp(:)

           if (allocated(arr)) deallocate(arr)
           call get_array_1d(this, path, type_marker,tmp)
           select type(tmp)
           type is (real(sp))
               ALLOCATE(arr(SIZE(tmp))); arr=tmp
           endselect
           if (allocated(tmp)) deallocate(tmp)

       end subroutine get_array_1d_real_sp

       subroutine get_array_1d_real_dp(this, path, arr)
           type(fson_value), pointer, intent(in) :: this
           character(len=*),          intent(in) :: path   

           real(dp), allocatable, intent(out) :: arr(:)

           real(dp)::type_marker
           class(*),allocatable::tmp(:)

           if (allocated(arr)) deallocate(arr)
           call get_array_1d(this, path, type_marker,tmp)
           select type(tmp)
           type is (real(dp))
               ALLOCATE(arr(SIZE(tmp))); arr=tmp
           endselect
           if (allocated(tmp)) deallocate(tmp)

       end subroutine get_array_1d_real_dp

#ifndef NO_QP
       subroutine get_array_1d_real_qp(this, path, arr)
           type(fson_value), pointer, intent(in) :: this
           character(len=*),          intent(in) :: path   

           real(qp), allocatable, intent(out) :: arr(:)

           real(qp)::type_marker
           class(*),allocatable::tmp(:)

           if (allocated(arr)) deallocate(arr)
           call get_array_1d(this, path, type_marker,tmp)
           select type(tmp)
           type is (real(qp));
               ALLOCATE(arr(SIZE(tmp))); arr=tmp
           endselect
           if (allocated(tmp)) deallocate(tmp)

       end subroutine get_array_1d_real_qp
#endif


       !
       ! GET ARRAY LOGICAL 1D
       !
       subroutine get_array_1d_logical(this, path, arr)
           type(fson_value), pointer, intent(in) :: this
           character(len=*),          intent(in) :: path   

           logical, allocatable, intent(out) :: arr(:)

           logical::type_marker
           class(*),allocatable::tmp(:)

           if (allocated(arr)) deallocate(arr)
           call get_array_1d(this, path, type_marker,tmp)
           select type(tmp)
           type is (logical);
               ALLOCATE(arr(SIZE(tmp))); arr=tmp
           endselect
           if (allocated(tmp)) deallocate(tmp)

       end subroutine get_array_1d_logical

       !
       ! GET ARRAY CHAR 1D
       !
       subroutine get_array_1d_char(this, path, arr)
           type(fson_value), pointer,        intent(in) :: this
           character(len=*),                 intent(in) :: path

           character(len = :), allocatable, intent(out) :: arr(:)

           character(len= 1)::type_marker
           class(*),allocatable::tmp(:)

           if (allocated(arr)) deallocate(arr)
           call get_array_1d(this, path, type_marker,tmp)
           select type(tmp)
           type is (character(len=*));
               ALLOCATE(character(len=LEN(tmp(1))):: arr(SIZE(tmp))); arr=tmp
           endselect
           if (allocated(tmp)) deallocate(tmp)

       end subroutine get_array_1d_char

    subroutine get_chars_prealloc(this, path, value)
        type(fson_value), pointer,      intent(in) ::  this
        character(len=*), optional,     intent(in) :: path

        character(len=*), intent(out) :: value        
        type(fson_value), pointer ::  p
        character(len=:), allocatable :: tmp

        nullify(p)                

        if(present(path)) then
            call get_by_path(this=this, path=path, p=p)
        else
            p => this
        end if

        if(.not.associated(p)) then
            call error_stop("Unable to resolve path: "//trim(path))
        end if

        if  (p % value_type == TYPE_STRING) then            
            call fson_string_copy(p % value_string, tmp)
            value=tmp
        else
            call error_stop("Unable to resolve value to string: "//trim(path))
        end if

        end subroutine 

   end module fson_path_m
