!Copyright (c) 2016 Mikhail Kruglyakov 
!This file is part of GIEM2G.
!
!GIEM2G is free software: you can redistribute it and/or modify
!it under the terms of the GNU General Public License as published by
!the Free Software Foundation, either version 2 of the License.
!
!GIEM2G is distributed in the hope that it will be useful,
!but WITHOUT ANY WARRANTY; without even the implied warranty of
!MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!GNU General Public License for more details.
!
!You should have received a copy of the GNU General Public License
!along with GIEM2G.  If not, see <http://www.gnu.org/licenses/>.
!
!
!

MODULE IE_SOLVER_MODULE
    USE CONST_MODULE
    USE FFTW3
    USE MPI_MODULE
    USE DATA_TYPES_MODULE
    USE OPERATORS_MODULE 
    USE OPERATOR_A2A_MODULE 
    USE Timer_Module 
    USE PGIEM2G_CHECK_MEMORY
    USE PGIEM2G_LOGGER_MODULE
    USE PGIEM2G_APPLY_OPERATOR_A2A_MODULE 

    USE POLYNOMIAL_UTILITIES_MODULE
    USE FGMRES_INTERFACES
    USE SEQ_FGMRES

    IMPLICIT NONE
    PRIVATE

!For integral equation kernel!

    PUBLIC:: SolveEquation
    
    CONTAINS

    SUBROUTINE SolveEquation(ie_op,fgmres_ctl,E_bkg,Esol,E0)
        TYPE(Operator_A2A),INTENT(INOUT)::ie_op
        TYPE (FGMRES_CTL_TYPE),INTENT(IN)::fgmres_ctl
        COMPLEX(REALPARM),POINTER,INTENT(IN)::E_bkg(:,:,:,:,:,:,:),Esol(:,:,:,:,:,:,:)
        COMPLEX(REALPARM),POINTER,INTENT(IN),OPTIONAL::E0(:,:,:,:,:,:,:) !
        COMPLEX(REALPARM),POINTER::guess(:,:,:,:,:,:,:)
        INTEGER::anom_shape(3)
        INTEGER::tmp_shape(3)
        INTEGER::Iz,Nz,Nx,Ny_loc
        INTEGER::Ix,Iy,Ic
        INTEGER::Ipx,Ipy,Ipz
        INTEGER::Px,Py,Pz
        INTEGER(MPI_CTL_KIND)::IERROR
        COMPLEX(REALPARM)::asiga
        REAL(DOUBLEPARM)::time1,time2
        REAL(REALPARM)::S
        CALL PRINT_BORDER
        CALL LOGGER('IE solving starts')
        time1=GetTime()
        Nx=ie_op%Nx
        Nz=ie_op%col%Nz
        Ny_loc=ie_op%Ny_loc
        Px=ie_op%col%Px
        Py=ie_op%col%Py
        Pz=ie_op%col%Pz
        IF (ie_op%real_space) THEN
            ALLOCATE(guess(3,0:ie_op%col%Pz,0:Px,0:Py,ie_op%Nx,ie_op%Ny_loc,Nz))
            DO Iz=1,Nz
                DO Ipy=0,Py
                    DO Ipx=0,Px
                        DO Ipz=0,Pz
                            S=ie_op%col%sqdz(Iz)/ie_op%col%pnorm(Ipz,Ipx,Ipy)
                            Esol(:,Ipz,Ipx,Ipy,:,:,Iz)=ie_op%sqsigb(Iz)*&
                                &E_bkg(:,Ipz,Ipx,Ipy,:,:,Iz)*S
                        ENDDO
                    ENDDO
                ENDDO
            ENDDO
            IF (PRESENT(E0)) THEN
                DO Iz=1,Nz
                    DO Iy=1,Ny_loc
                        DO Ix=1,ie_op%Nx
                            IF ( .NOT. ie_op%UPSCALING ) THEN
                                asiga=C_TWO*ie_op%sqsigb(Iz)/&
                                    &(ie_op%csiga(Ix,Iy,Iz)+CONJG(ie_op%csigb(Iz)))
                            ENDIF
                            DO Ipy=0,Py
                                DO Ipx=0,Px
                                    DO Ipz=0,Pz
                                        S=ie_op%col%sqdz(Iz)/&
                                            &ie_op%col%pnorm(Ipz,Ipx,Ipy)
                                        IF ( .NOT. ie_op%UPSCALING) THEN
                                            guess(:,Ipz,Ipx,Ipy,Ix,Iy,Iz)=&
                                                    &E0(:,Ipz,Ipx,Ipy,Ix,Iy,Iz)/asiga*S
                                        ELSE
                                            guess(:,Ipz,Ipx,Ipy,Ix,Iy,Iz)=&
                                                    &E0(:,Ipz,Ipx,Ipy,Ix,Iy,Iz)*S
                                        ENDIF
                                    ENDDO
                                ENDDO
                            ENDDO
                        ENDDO
                    ENDDO
                ENDDO
            ELSE
                guess=Esol
            ENDIF
        ENDIF
        CALL GIEM2G_FGMRES(ie_op,fgmres_ctl,Esol,guess)


        IF (ie_op%real_space) THEN
            DO Iz=1,Nz
                DO Iy=1,Ny_loc
                    DO Ix=1,ie_op%Nx
                        IF ( .NOT. ie_op%UPSCALING ) THEN
                            asiga=C_TWO*ie_op%sqsigb(Iz)/(ie_op%csiga(Ix,Iy,Iz)+CONJG(ie_op%csigb(Iz)))
                        ENDIF
                        DO Ipy=0,Py
                            DO Ipx=0,Px
                                DO Ipz=0,Pz
                                    S=ie_op%col%sqdz(Iz)/&
                                        &ie_op%col%pnorm(Ipz,Ipx,Ipy)
                                    IF ( .NOT. ie_op%UPSCALING ) THEN
                                        Esol(:,Ipz,Ipx,Ipy,Ix,Iy,Iz)=&
                                                &Esol(:,Ipz,Ipx,Ipy,Ix,Iy,Iz)*asiga/S
                                    ELSE
                                        Esol(:,Ipz,Ipx,Ipy,Ix,Iy,Iz)=&
                                                &Esol(:,Ipz,Ipx,Ipy,Ix,Iy,Iz)/S
                                    ENDIF
                                ENDDO
                             ENDDO
                        ENDDO
                    ENDDO
                ENDDO
            ENDDO
                DEALLOCATE(guess)
        ENDIF
        time2=GetTime()
        CALL LOGGER('Solving has been finished')
        CALL PRINT_CALC_TIME('Total time:                               ',time2-time1)
        CALL PRINT_CALC_TIME('Time for multiplications:                 ', ie_op%counter%apply)
        CALL PRINT_CALC_TIME('Time for dot products:                    ', ie_op%counter%dotprod)
        CALL PRINT_CALC_TIME('Average dot product:                      ', ie_op%counter%dotprod/ie_op%counter%dotprod_num)
        CALL PRINT_CALC_TIME('Average fftw forward:                     ', ie_op%counter%mult_fftw/ie_op%counter%mult_num)
        CALL PRINT_CALC_TIME('Average fftw backward:                    ', ie_op%counter%mult_fftw_b/ie_op%counter%mult_num)
        CALL PRINT_CALC_TIME('Average zgemv:                            ', ie_op%counter%mult_zgemv/ie_op%counter%mult_num)
        CALL PRINT_CALC_TIME('Average mult:                             ', ie_op%counter%apply/ie_op%counter%mult_num)
        CALL PRINT_CALC_TIME('Total mult/dp:                            ', ie_op%counter%apply/ie_op%counter%dotprod)
        CALL PRINT_CALC_TIME('Average mult/dp:                          ',&
                & ie_op%counter%apply/ie_op%counter%dotprod*ie_op%counter%dotprod_num/ie_op%counter%mult_num)
        CALL PRINT_CALC_NUMBER('Number of matrix-vector multiplications:', ie_op%counter%mult_num)
        CALL PRINT_CALC_NUMBER('Number of dotproducts:                  ', ie_op%counter%dotprod_num)
        CALL PRINT_BORDER
    END SUBROUTINE

!-------------------------------------PRIVATE---------------------------------------------------------------------!
    SUBROUTINE GIEM2G_FGMRES(ie_op,fgmres_ctl,Esol, guess)
        TYPE(Operator_A2A), TARGET, INTENT(INOUT)::ie_op
        TYPE (FGMRES_CTL_TYPE),INTENT(IN)::fgmres_ctl
        COMPLEX(REALPARM),POINTER,INTENT(IN)::Esol(:,:,:,:,:,:,:)
        COMPLEX(REALPARM),POINTER,INTENT(IN)::guess(:,:,:,:,:,:,:)

        TYPE(SEQ_FGMRES_DATA)::seq_solver
        PROCEDURE (MatrixVectorMult),POINTER::Apply
        PROCEDURE (MANYDOTPRODUCTS),POINTER::MANYDP
        PROCEDURE (InformAboutIteration),POINTER::InformMe
        COMPLEX(REALPARM),ALLOCATABLE::solution(:),initial_guess(:),rhs(:)
        COMPLEX(REALPARM),POINTER::pguess(:),pEsol(:)
        TYPE(C_PTR)::ptmp
        TYPE(MPI_POSITION_TYPE)::mpi
        INTEGER(C_INTPTR_T)::Length
        INTEGER(MPI_CTL_KIND),TARGET::comm
        TYPE(C_PTR)::pcomm
        INTEGER(MPI_CTL_KIND):: IERROR
        REAL(8)::time1,time2
        REAL(8)::full_time
        CHARACTER(LEN=*), PARAMETER  :: info_fmt = "(A, I6, A, ES10.2E2)"
        CHARACTER(LEN=2048,KIND=C_CHAR)::message 
        INTEGER::Nloc
        TYPE(GMRES_PARAMETERS)::params
        INTEGER::MM(2)
        TYPE(RESULT_INFO)::info
#ifndef IBM_Bluegene
        CLASS(*), POINTER :: pA
#else
        TYPE(VOID_WRAPPER) :: pA
#endif

        full_time=GetTime()
        Apply=>FULL_APPLY
        MANYDP=>DISTRIBUTED_DOT_PRODUCT
        InformMe=>Information
        mpi=ie_op%mpi
        CALL DROP_IE_COUNTER(ie_op)
        params%MaxIt=fgmres_ctl%fgmres_maxit
        params%Tol=fgmres_ctl%misfit
        params%RESTART_RESIDUAL=.FALSE.

        MM(1)=fgmres_ctl%fgmres_buf
        MM(2)=fgmres_ctl%gmres_buf
        Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2
        CALL INIT_SEQ_FGMRES(seq_solver,Nloc,MM,2,params,Length)
#ifndef IBM_Bluegene
        pA=>ie_op
#else
        pA%C=>ie_op
#endif

#ifndef IBM_Bluegene
        CALL SET_OPERATORS(seq_solver, Apply, MANYDP, informMe, pA)
#else
        CALL SET_OPERATORS(seq_solver, Apply, MANYDP, informMe, pA%C)
#endif
        comm=mpi%comm
        pcomm=C_LOC(comm)

#ifndef IBM_Bluegene
        CALL SET_DP_INSTANCE(seq_solver,pA)
#else
        CALL SET_DP_INSTANCE(seq_solver,pA%C)
#endif

        WRITE (message,*) 'Solver needs',Length*16.0/1024/1024/1024, 'Gb for workspace'
    
        CALL LOGGER(message)

        ALLOCATE(solution(1:Nloc),initial_guess(1:Nloc),rhs(1:Nloc))
        solution=C_ONE
        initial_guess=C_ONE
        rhs=C_ONE
        CALL MPI_BARRIER(ie_op%mpi%comm,IERROR)
        IF (ie_op%real_space) THEN
            Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2
            
            ptmp=C_LOC(Esol(1,0,0,0,1,1,1))
            CALL C_F_POINTER(ptmp,pEsol,(/2*Nloc/))

            ptmp=C_LOC(guess(1,0,0,0,1,1,1))
            CALL C_F_POINTER(ptmp,pguess,(/2*Nloc/))
            CALL MPI_SEND(pguess(Nloc+1:),Nloc, MPI_DOUBLE_COMPLEX,&
                & mpi%partner, mpi%me,mpi%comm, IERROR)


            Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2

            CALL MPI_SEND(pEsol(Nloc+1:),Nloc, MPI_DOUBLE_COMPLEX,&
                & mpi%partner, mpi%me+mpi%comm_size,mpi%comm, IERROR)

            CALL ZCOPY(Nloc,Esol,ONE,rhs,ONE)
            CALL ZCOPY(Nloc,pguess,ONE,initial_guess,ONE)

        ELSE
            Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2
            CALL MPI_RECV(initial_guess,Nloc, MPI_DOUBLE_COMPLEX, mpi%partner,&
                & mpi%partner,mpi%comm,MPI_STATUS_IGNORE, IERROR)

            Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2
            CALL MPI_RECV(rhs,Nloc, MPI_DOUBLE_COMPLEX, mpi%partner,&
                & mpi%partner+mpi%comm_size,mpi%comm,MPI_STATUS_IGNORE, IERROR)
        ENDIF

        CALL LOGGER("After allocating buffers for FGMRES")
        CALL CHECK_MEM(mpi%me,0,mpi%comm)
        CALL MPI_BARRIER(mpi%comm,IERROR)

        CALL SEQ_FGMRES_SOLVE(seq_solver,solution,initial_guess,rhs,info)

        IF (ie_op%real_space) THEN
            Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2
            CALL MPI_RECV(pEsol(Nloc+1:),Nloc, MPI_DOUBLE_COMPLEX,&
                & mpi%partner, mpi%partner,mpi%comm,MPI_STATUS_IGNORE, IERROR)
            Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2
            CALL ZCOPY (Nloc, solution, ONE, pEsol, ONE)
        ELSE
            Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2
            CALL MPI_SEND(solution,Nloc, MPI_DOUBLE_COMPLEX,&
                & mpi%partner, mpi%me,mpi%comm, IERROR)
        ENDIF

        DEALLOCATE(solution,initial_guess,rhs)
        CALL DELETE_SEQ_FGMRES(seq_solver)


        full_time=GetTime()-full_time
        ie_op%counter%solving=full_time

        IF (info%stat==CONVERGED ) THEN
            WRITE(message,info_fmt) 'GFGMRES converged in', info%Iterations, &
                &' iterations with misfit:',info%be 
        ELSEIF (info%stat==NON_TRUE_CONVERGED) THEN
            WRITE(message, '(A, I6, A, ES10.2E2, A,  ES10.2E2)' )  'GFGMRES  more or less converged in', info%Iterations, &
                &' iterations with Arnoldy misfit',info%bea, &
                &' and true misfit', info%be    
        ELSEIF (info%stat==NON_CONVERGED) THEN
            WRITE(message,info_fmt) 'GFGMRES has NOT converged in', info%Iterations, &
                &' iterations with Arnoldy  misfit:',info%bea   
        ELSE
            WRITE(message,'(A)') 'Unknown behaviour of GFGMRES'
        ENDIF
        CALL LOGGER(message)
    END SUBROUTINE

#ifndef IBM_Bluegene
    SUBROUTINE FULL_APPLY (ie_op,v_in,v_out)
        CLASS(*), POINTER, INTENT(IN) :: ie_op
#else
    SUBROUTINE FULL_APPLY (A,v_in,v_out)
    TYPE(VOID_WRAPPER), INTENT(IN) :: A
#endif
        COMPLEX(REALPARM),INTENT(IN)::v_in(:)
        COMPLEX(REALPARM),INTENT(INOUT)::v_out(:)
        COMPLEX(REALPARM),TARGET::tmp_in(2*SIZE(v_in))
        COMPLEX(REALPARM),TARGET::tmp_out(2*SIZE(v_out))
        COMPLEX(REALPARM),POINTER::ptmp(:)
        INTEGER::Nloc
        INTEGER(MPI_CTL_KIND):: IERROR=0
#ifdef IBM_Bluegene
    CLASS(*), POINTER ::ie_op
    ie_op=>A%C
#endif
        SELECT TYPE (ie_op)
        TYPE IS (Operator_A2A)
            Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2
            ptmp=>tmp_in(Nloc+1:)
            IF (ie_op%real_space) THEN
                CALL ZCOPY (Nloc, v_in, ONE, tmp_in, ONE)
                Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2
                CALL MPI_RECV(ptmp,Nloc, MPI_DOUBLE_COMPLEX,&
                    & ie_op%mpi%partner, ie_op%mpi%partner+1,&
                    &ie_op%mpi%comm,MPI_STATUS_IGNORE, IERROR)

                CALL APPLY_PRECOND(ie_op,tmp_in,tmp_out)
                Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2

                CALL MPI_SEND(tmp_out(Nloc+1:),Nloc, MPI_DOUBLE_COMPLEX,&
                    & ie_op%mpi%partner, ie_op%mpi%me+ie_op%mpi%comm_size+1,&
                    &ie_op%mpi%comm, IERROR)
                CALL ZCOPY (Nloc, tmp_out, ONE, v_out, ONE)

            ELSE
                CALL MPI_SEND(v_in,Nloc, MPI_DOUBLE_COMPLEX, ie_op%mpi%partner,&
                    & ie_op%mpi%me+1,ie_op%mpi%comm, IERROR)
                CALL APPLY_IE_ZEROS(ie_op)
                Nloc=3*ie_op%col%Nu*ie_op%Nx*ie_op%Ny_loc/2

                CALL MPI_RECV(v_out,Nloc, MPI_DOUBLE_COMPLEX, ie_op%mpi%partner,&
                    & ie_op%mpi%partner+ie_op%mpi%comm_size+1,&
                    &ie_op%mpi%comm,MPI_STATUS_IGNORE,IERROR)
            ENDIF

            ie_op%counter%mult_num=ie_op%counter%mult_num+1

        CLASS DEFAULT 
            CALL LOGGER("WRONG TYPE FOR OPERATOR A2A")
            STOP 3
        ENDSELECT
    ENDSUBROUTINE




#ifndef IBM_Bluegene
    SUBROUTINE DISTRIBUTED_DOT_PRODUCT(M, v, K, res, ie_op)
#else
    SUBROUTINE DISTRIBUTED_DOT_PRODUCT(M, v, K, res, A)
#endif
        COMPLEX(REALPARM),  INTENT(IN):: M(:,:)
        COMPLEX(REALPARM),  INTENT(IN):: v(:)
        INTEGER    ,  INTENT(IN):: K
        COMPLEX(REALPARM),  INTENT(INOUT):: res(:)
#ifndef IBM_Bluegene
        CLASS(*), POINTER,  INTENT(IN)::ie_op
#else
    TYPE(VOID_WRAPPER), INTENT(IN) :: A
#endif
    COMPLEX(REALPARM),TARGET::tmp(K)
    INTEGER(MPI_CTL_KIND):: IERROR,comm,KK
    REAL(DOUBLEPARM)::time1,time2
    INTEGER::N
#ifdef IBM_Bluegene
    CLASS(*), POINTER ::ie_op
    ie_op=>A%C
#endif
        time1=GetTime()
        N=SIZE(v)

        CALL ZGEMV('C', N, K, C_ONE, M, N, v, ONE, C_ZERO, res, ONE)

        SELECT TYPE (ie_op)
        TYPE IS (Operator_A2A)
            comm=ie_op%mpi%comm
            KK=K
            CALL MPI_ALLREDUCE(MPI_IN_PLACE,res(1:KK),KK,&
                &MPI_DOUBLE_COMPLEX,MPI_SUM,comm,IERROR)
            IF (IERROR/=0) THEN
                PRINT*,'DP ##',ie_op%mpi%me, ie_op%counter%dotprod_num,res(1),IERROR
            ENDIF
            time2=GetTime()
            ie_op%counter%dotprod_num=ie_op%counter%dotprod_num+K
            ie_op%counter%dotprod=ie_op%counter%dotprod+time2-time1
        CLASS DEFAULT 
            CALL LOGGER("WRONG TYPE FOR OPERATOR A2A")
            STOP 3
        ENDSELECT

    ENDSUBROUTINE

    FUNCTION Information(info) RESULT(interp)
    TYPE(RESULT_INFO),INTENT(IN)::info
    INTEGER::interp
    CHARACTER(LEN=2048,KIND=C_CHAR)::message 
    WRITE (message,'(A, I7, A, ES13.3E3)') "FGMRES outer  iteration:", info%Iterations,&
        & " Arnoldy misfit: ", info%bea
    CALL Logger(message)
    interp=NOINTERUPT
    ENDFUNCTION

END MODULE  
