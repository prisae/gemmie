!Copyright (c) 2016 Mikhail Kruglyakov 
!This file is part of GIEM2G.
!
!GIEM2G is free software: you can redistribute it and/or modify
!it under the terms of the GNU General Public License as published by
!the Free Software Foundation, either version 2 of the License.
!
!GIEM2G is distributed in the hope that it will be useful,
!but WITHOUT ANY WARRANTY; without even the implied warranty of
!MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!GNU General Public License for more details.
!
!You should have received a copy of the GNU General Public License
!along with GIEM2G.  If not, see <http://www.gnu.org/licenses/>.
!
!
!

MODULE SOURCES_MODULE
    USE CONST_MODULE
    USE APQ_MODULE
    USE PGIEM2G_LOGGER_MODULE
    USE DATA_TYPES_MODULE
    USE LEGENDRE_VERTICAL_MODULE
    IMPLICIT NONE
    INTERFACE PLANE_WAVE
        MODULE PROCEDURE PLANE_WAVE_AT_RECVS
        MODULE PROCEDURE PLANE_WAVE_AT_DEPTHS
    ENDINTERFACE
CONTAINS
    SUBROUTINE PLANE_WAVE_AT_RECVS(pol,bkg,recvs,F0)
        INTEGER,INTENT(IN)::pol
        TYPE (BKG_DATA_TYPE),TARGET,INTENT(IN)::bkg
        TYPE (RECEIVERS_TYPE)::recvs
        COMPLEX(REALPARM),INTENT(OUT)::F0(:,:,:,:)
        REAL(REALPARM)::zrecv(recvs%Nr)
        INTEGER::Ir,Id,Iz,Nm
        Ir=0
        DO Id=1,recvs%Nd
            Nm=recvs%doms(Id)%Nm
            DO Iz=1,recvs%doms(Id)%Nd
                zrecv(Ir+1:Ir+Nm)=recvs%doms(Id)%depth(Iz)
                Ir=Ir+Nm
            ENDDO
        ENDDO
        CALL PLANE_WAVE_AT_DEPTHS(pol,bkg,zrecv,F0)
    ENDSUBROUTINE

    SUBROUTINE PLANE_WAVE_AT_DEPTHS(pol,bkg,zrecv,F0)  
        INTEGER,INTENT(IN)::pol
        TYPE (BKG_DATA_TYPE),TARGET,INTENT(IN)::bkg
        REAL(REALPARM),INTENT(IN)::zrecv(:)
        COMPLEX(REALPARM),INTENT(OUT)::F0(:,:,:,:)
        COMPLEX(REALPARM)::G(2)
        COMPLEX(REALPARM)::A(bkg%Nl,bkg%Nl,2),p(bkg%Nl,2),q(bkg%Nl,2),eta(bkg%Nl),Arr(bkg%Nl,2)
        COMPLEX(REALPARM)::t1,t2,e1,e2
        INTEGER::Nr,Ir,l
        F0=C_ZERO
        CALL Calc_APQ(bkg,R_ZERO,Arr,p,q,eta,A)
        IF (bkg%Nl>1) THEN
            t1=C_ONE+q(1,1)*EXP(-bkg%depth(1)*eta(1)*C_TWO)
        ELSE
            t1=C_ONE
        ENDIF
        t1=A(1,1,1)*t1
        Nr=SIZE(zrecv)
        DO Ir=1,Nr
            l=GetLayer(zrecv(Ir),bkg)
            l=MAX(l,1)
            t2=A(l,1,1)/t1
            e1=EXP(-eta(l)*zrecv(Ir))
            IF (l<bkg%Nl) THEN
                e2=q(l,1)*(EXP(-eta(l)*(C_TWO*bkg%depth(l)-zrecv(Ir))))
            ELSE
                e2=C_ZERO
            ENDIF
            G(1)=t2*(e1+e2)
            G(2)=-t2*(e1-e2)*eta(l)/bkg%iwm
            IF (pol==EX) THEN
                F0(Ir,EX,:,:)=G(1)
                F0(Ir,HY,:,:)=G(2)
            ELSEIF (pol==EY) THEN
                F0(Ir,EY,:,:)=G(1)
                F0(Ir,HX,:,:)=-G(2)
            ELSE
                CALL LOGGER('INCORRECT POLARIZATION')
            ENDIF
        ENDDO
    END SUBROUTINE
    SUBROUTINE PlaneWaveIntegral(pol,bkg,anomaly,E0)
        INTEGER,INTENT(IN)::pol
        TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        COMPLEX(REALPARM),INTENT(OUT)::E0(:,:,:,:)
        COMPLEX(REALPARM)::A(bkg%Nl,bkg%Nl,2),p(bkg%Nl,2),q(bkg%Nl,2),eta(bkg%Nl),Arr(bkg%Nl,2)
        COMPLEX(REALPARM)::t1,t2,e1,e2,e3,e4,G
        INTEGER::Nz,Iz,l
        REAL(REALPARM)::dz
        E0=0d0
        CALL Calc_APQ(bkg,R_ZERO,Arr,p,q,eta,A)
        IF (bkg%Nl>1) THEN
            t1=C_ONE+q(1,1)*EXP(-bkg%depth(1)*eta(1)*C_TWO)
        ELSE
            t1=C_ONE
        ENDIF
        t1=A(1,1,1)*t1
        Nz=anomaly%Nz
        DO Iz=1,Nz
            dz=anomaly%z(Iz)-anomaly%z(Iz-1)
            l=anomaly%Lnumber(Iz)
            e1=EXP(-eta(l)*anomaly%z(Iz-1))
            e2=EXP(-eta(l)*anomaly%z(Iz))
            IF (l<bkg%Nl) THEN
                e3=q(l,1)*(EXP(-eta(l)*(C_TWO*bkg%depth(l)-anomaly%z(Iz-1))))
                e4=q(l,1)*(EXP(-eta(l)*(C_TWO*bkg%depth(l)-anomaly%z(Iz))))
            ELSE
                e3=C_ZERO
                e4=C_ZERO
            ENDIF
            G=(e1-e2+e4-e3)*A(l,1,1)/t1
            G=G/dz/eta(l)
            E0(pol,:,:,Iz)=G
        ENDDO
    END SUBROUTINE
    SUBROUTINE PlaneWaveIntegralP(pol,bkg,anomaly,E0)
        INTEGER,INTENT(IN)::pol
        TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        COMPLEX(REALPARM),INTENT(OUT)::E0(1:,0:,0:,0:,1:,1:,1:)
        COMPLEX(REALPARM)::A(bkg%Nl,bkg%Nl,2),p(bkg%Nl,2),q(bkg%Nl,2),eta(bkg%Nl),Arr(bkg%Nl,2)
        COMPLEX(REALPARM)::NormE,Psi(0:SIZE(E0,2))
        COMPLEX(REALPARM)::t1,t2,e1,e2,e3,e4,G
        INTEGER::Nz,Iz,l
        REAL(REALPARM)::dz
        E0=C_ZERO
        CALL Calc_APQ(bkg,R_ZERO,Arr,p,q,eta,A)
        IF (bkg%Nl>1) THEN
            NormE=C_ONE+q(1,1)*EXP(-bkg%depth(1)*eta(1)*C_TWO)
        ELSE
            NormE=C_ONE
        ENDIF
        NormE=A(1,1,1)*NormE
        Nz=anomaly%Nz
        DO Iz=1,Nz
            dz=anomaly%z(Iz)-anomaly%z(Iz-1)
            l=anomaly%Lnumber(Iz)
            e1=EXP(-eta(l)*anomaly%z(Iz-1))
            e2=EXP(-eta(l)*anomaly%z(Iz))
            IF (l<bkg%Nl) THEN
                e3=q(l,1)*(EXP(-eta(l)*(C_TWO*bkg%depth(l)-anomaly%z(Iz-1))))
                e4=q(l,1)*(EXP(-eta(l)*(C_TWO*bkg%depth(l)-anomaly%z(Iz))))
            ELSE
                e3=C_ZERO
                e4=C_ZERO
            ENDIF
            G=(e1-e2+e4-e3)*A(l,1,1)/NormE
            G=G/dz/eta(l)
            E0(pol,0,0,0,:,:,Iz)=G
        ENDDO
    END SUBROUTINE
    SUBROUTINE PlaneWaveIntegralP2(pol,bkg,anomaly,E0)
        INTEGER,INTENT(IN)::pol
        TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        COMPLEX(REALPARM),INTENT(OUT)::E0(1:,0:,0:,0:,1:,1:,1:)
        COMPLEX(REALPARM)::A(bkg%Nl,bkg%Nl,2),p(bkg%Nl,2),q(bkg%Nl,2),eta(bkg%Nl),Arr(bkg%Nl,2)
        COMPLEX(REALPARM)::NormE,Psi(2:SIZE(E0,2)-1)
        COMPLEX(REALPARM)::expz(anomaly%Nz)
        COMPLEX(REALPARM)::fcont_b(2,anomaly%Nz),qez(2,anomaly%Nz)
        COMPLEX(REALPARM)::b_frame(3,0:1,anomaly%Nz),base
        COMPLEX(REALPARM)::ee(0:1),G,kappa,k
        INTEGER::Nz,Iz,l,Np,Ip
        REAL(REALPARM)::dz,S
        E0=C_ZERO
        CALL Calc_APQ(bkg,R_ZERO,Arr,p,q,eta,A)
        IF (bkg%Nl>1) THEN
            NormE=C_ONE+q(1,1)*EXP(-bkg%depth(1)*eta(1)*C_TWO)
            l=anomaly%Lnumber(1)
            base=exp(-eta(l)*anomaly%z(0))+q(l,1)*&
                    &exp(-eta(l)*(C_TWO*bkg%depth(l)-anomaly%z(0)))
            base=base/NormE
            base=base*A(1,l,1)/A(1,1,1)
        ELSE
            base=exp(-eta(1)*anomaly%z(0))
        ENDIF

        Np=SIZE(E0,2)-1
        Nz=anomaly%Nz
        DO Iz=1,Nz
            l=anomaly%Lnumber(Iz)
            expz(Iz)=EXP(-eta(l)*anomaly%dz(Iz))
        ENDDO
        CALL Calc_qexpz(bkg,anomaly,expz,eta,q,qez)
        CALL OBTAIN_ALL_BOTTOM_FRAMES(bkg,anomaly,ONE,expz,eta,qez,fcont_b,b_frame)
        DO Iz=1,Nz
            ee=base*b_frame(1,0:1,Iz)
            S=R_ONE/anomaly%dz(Iz)
            E0(pol,0,0,0,:,:,Iz)=ee(0)*S
            IF (Np>0) THEN
                S=R_ONE/anomaly%dz(Iz)*3
                E0(pol,1,0,0,:,:,Iz)=ee(1)*S
                l=anomaly%Lnumber(Iz)
                kappa=eta(l)*anomaly%dz(Iz)/C_TWO
                CALL CALC_PSIMULT(kappa,expz(Iz),Np, Psi)
                DO Ip=2,Np
                    S=R_ONE*(2*Ip+1)/anomaly%dz(Iz)
                    E0(pol,Ip,0,0,:,:,Iz)=ee(MOD(Ip,2))*Psi(Ip)*S
                ENDDO
            ENDIF
            base=base*fcont_b(1,Iz)
        ENDDO
    END SUBROUTINE
END MODULE
