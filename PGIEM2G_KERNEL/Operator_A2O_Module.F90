!Copyright (c) 2016 Mikhail Kruglyakov 
!This file is part of GIEM2G.
!
!GIEM2G is free software: you can redistribute it and/or modify
!it under the terms of the GNU General Public License as published by
!the Free Software Foundation, either version 2 of the License.
!
!GIEM2G is distributed in the hope that it will be useful,
!but WITHOUT ANY WARRANTY; without even the implied warranty of
!MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!GNU General Public License for more details.
!
!You should have received a copy of the GNU General Public License
!along with GIEM2G.  If not, see <http://www.gnu.org/licenses/>.
!
!
!

MODULE OPERATOR_A2O_MODULE

    USE CONST_MODULE
    USE FFTW3
    USE MEMORY_MANAGER_MODULE
    USE MPI_MODULE
    USE OPERATORS_MODULE 
    USE Timer_Module 
    USE DATA_TYPES_MODULE
    USE POLYNOMIAL_UTILITIES_MODULE
    USE DISTRIBUTED_FFT_MODULE
    USE PGIEM2G_LOGGER_MODULE
    IMPLICIT NONE

    PRIVATE

    PUBLIC:: OPERATOR_A2O
    PUBLIC:: PREPARE_OPERATOR_A2O
    PUBLIC:: DELETE_OPERATOR_A2O
    PUBLIC:: A2O_LOCAL_KERNEL_SIZE
    PUBLIC:: A2O_FFTW_FWD,A2O_FFTW_BWD
    PUBLIC:: CALC_FFT_OF_A2O_KERNEL
    !        PUBLIC:: PRINT_STATS,DROP_IE_COUNTER
CONTAINS

    SUBROUTINE PREPARE_OPERATOR_A2O(rc_op, anomaly, bkg, recvs, P, comm, fftw_threads_ok, cptrs)
        TYPE(OPERATOR_A2O),INTENT(INOUT)::rc_op
        TYPE (ANOMALY_TYPE),INTENT(INOUT)::anomaly
        TYPE(BKG_DATA_TYPE),INTENT(IN)::bkg
        TYPE (RECEIVERS_TYPE),INTENT(IN)::recvs
        TYPE(POL_ORDER_TYPE)::P
        INTEGER(MPI_CTL_KIND),INTENT(IN)::comm 
        LOGICAL,INTENT(IN)::fftw_threads_ok
        TYPE(OPERATOR_C_PTR),OPTIONAL,INTENT(IN)::cptrs
        INTEGER::Nx,Ny,Nz,Nd
        INTEGER::Nx2,Ny2,Nc,Nr3,Nopt
        INTEGER(MPI_CTL_KIND)::Np,IERROR
        TYPE(C_PTR)::p1,p2
        INTEGER::NT
        
        IF (PRESENT(cptrs)) THEN
            rc_op%cptrs=cptrs
            rc_op%SELF_ALLOCATED=.FALSE.
        ELSE
            CALL MPI_COMM_SIZE(comm,Np,IERROR) 
            rc_op%cptrs=ALLOCATE_A2O_PTRS(anomaly,P,recvs%Nr,Np)
            rc_op%SELF_ALLOCATED=.TRUE.
        ENDIF
        CALL PREPARE_A2O_KERNEL(rc_op,anomaly,P,recvs,comm)
        rc_op%fftw_threads_ok=fftw_threads_ok
        CALL PREPARE_A2O_FFT(rc_op) 

        rc_op%csiga=>NULL()
        rc_op%csigb=>NULL()
        anomaly%Ny_loc=rc_op%Ny_loc
        CALL ANOMALY_TO_VERTICAL_DOMAIN(anomaly,rc_op%col)
        CALL BKG_TO_VERTICAL_DOMAIN(bkg,rc_op%col)
    ENDSUBROUTINE

    SUBROUTINE  PREPARE_A2O_KERNEL(rc_op,anomaly,P,recvs,comm)
        TYPE(OPERATOR_A2O),INTENT(INOUT)::rc_op
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        TYPE(POL_ORDER_TYPE),INTENT(IN)::P
        TYPE (RECEIVERS_TYPE),INTENT(IN)::recvs
        INTEGER(MPI_CTL_KIND),INTENT(IN)::comm 
        TYPE(MPI_POSITION_TYPE)::mpi
        INTEGER::Nx,Ny,Nz
        INTEGER::Nx2,Ny2,Nc
        INTEGER(MPI_CTL_KIND)::IERROR
        INTEGER::Nrd,I,Nm,Nr,Na

        Nx=anomaly%Nx
        Ny=anomaly%Ny
        Nz=anomaly%Nz

        rc_op%Nx=Nx
        rc_op%Ny=Ny

        rc_op%col=GET_VERTICAL_DOMAIN(Nz,P)

        CALL COPY_RECVS(recvs,rc_op%recvs)
        rc_op%Nr=recvs%Nr


        mpi=GET_MPI_DATA(comm,Ny)
        rc_op%mpi=mpi

        rc_op%master=(mpi%me==mpi%master_proc)

        IF (MOD(2*Ny,mpi%comm_size)/=0) THEN
            CALL LOGGER('Wrong number of processes. GIEM2G forced to halt!')
            CALL MPI_FINALIZE(IERROR)
            STOP
        ENDIF
        rc_op%Ny_loc=2*Ny/mpi%comm_size
        rc_op%Ny_offset=mpi%me*rc_op%Ny_loc
        rc_op%Nx2Ny_loc=2*Nx*rc_op%Ny_loc
        rc_op%Nx2Ny2=4*Nx*Ny
        rc_op%real_space=mpi%real_space
        CALL SET_KERNEL_POINTERS(rc_op)

    ENDSUBROUTINE
    !--------------------------------------------------------------------------------------------------------------------!


    SUBROUTINE  SET_KERNEL_POINTERS(rc_op)
        TYPE(OPERATOR_A2O),INTENT(INOUT)::rc_op
        COMPLEX(REALPARM),POINTER:: ptr(:)
        INTEGER::Nx,Ny,Nd,NxHalf,Lx,Ly
        INTEGER::NxNy_loc,N1,N2
        INTEGER::Nr,Nx2
        CALL C_F_POINTER(rc_op%cptrs%kernel,ptr,(/rc_op%cptrs%kernel_length/))
        ptr=C_ZERO

        Nd=rc_op%col%Nu
        Nr=rc_op%Nr
        Nx2=2*rc_op%Nx-1
        N1=rc_op%Ny_offset
        N2=rc_op%Ny_offset+rc_op%Ny_loc-1

        rc_op%G_E(1:Nd,1:Nr,REXX:REZZ,0:Nx2,N1:N2)=>ptr
        rc_op%G_E_fftw(1:Nd,1:8,1:Nr,0:Nx2,N1:N2)=>ptr
        rc_op%G_E4(1:Nd,1:Nr,REXX:REZZ,1:rc_op%Nx2Ny_loc)=>ptr


        ptr=>ptr(SIZE(rc_op%G_E)+1:)
        rc_op%G_H(1:Nd,1:Nr,RHXX:RHZY,0:Nx2,N1:N2)=>ptr
        rc_op%G_H_fftw(1:Nd,1:7,1:Nr,0:Nx2,N1:N2)=>ptr
        rc_op%G_H4(1:Nd,1:Nr,RHXX:RHZY,1:rc_op%Nx2Ny_loc)=>ptr
    ENDSUBROUTINE
    !--------------------------------------------------------------------------------------------------------------------!
    SUBROUTINE  PREPARE_A2O_FFT(rc_op)
        TYPE(OPERATOR_A2O),INTENT(INOUT)::rc_op
        TYPE(C_PTR)::fft_buff_in
        TYPE(C_PTR)::fft_buff_out
        INTEGER(C_INTPTR_T)::buff_len
        INTEGER::Nx2,Ny2,Ny_loc,Nx2Ny_loc
        INTEGER::NT!,OMP_GET_MAX_THREADS
        INTEGER::Nc,Nr,Nr3,Nd
        IF (rc_op%fftw_threads_ok) THEN
            NT=OMP_GET_MAX_THREADS()
            CALL FFTW_PLAN_WITH_NTHREADS(NT)
        ENDIF
        Nx2=2*rc_op%Nx
        Ny2=2*rc_op%Ny
        Nc=3*rc_op%col%Nu
        Nr3=rc_op%Nr*3
        Ny_loc=rc_op%Ny_loc
        Nx2Ny_loc=rc_op%Nx2Ny_loc
        Nd=rc_op%col%Nu
        Nr=rc_op%Nr

        buff_len=rc_op%cptrs%input_fft_length
        fft_buff_in=rc_op%cptrs%input_fft_in
        fft_buff_out=rc_op%cptrs%input_fft_out

        CALL PrepareDistributedFourierData(rc_op%DFD_Current,Nx2,Ny2,Nc,rc_op%mpi%comm,&
            &fft_buff_in,fft_buff_out,buff_len)

        buff_len=rc_op%cptrs%output_fft_length
        fft_buff_in=rc_op%cptrs%output_fft_in
        fft_buff_out=rc_op%cptrs%output_fft_out

        CALL PrepareDistributedFourierData(rc_op%DFD_Result,Nx2,Ny2,Nr3,rc_op%mpi%comm,&
            &fft_buff_in,fft_buff_out,buff_len)

        rc_op%field_in4(1:Nd,1:3,1:Nx2,1:Ny_loc)=>rc_op%DFD_Current%field_out
        rc_op%field_in3(1:Nd,1:3,1:Nx2Ny_loc)=>rc_op%DFD_Current%field_out

        rc_op%field_out4(1:Nr,1:3,1:Nx2,1:Ny_loc)=>rc_op%DFD_Result%field_out
        rc_op%field_out3(1:Nr,1:3,1:Nx2Ny_loc)=>rc_op%DFD_Result%field_out
    ENDSUBROUTINE


    SUBROUTINE A2O_FFTW_FWD(rc_op)
        TYPE(OPERATOR_A2O),INTENT(INOUT)::rc_op
        CALL    CalcDistributedFourier(rc_op%DFD_Current,FFT_FWD)
    ENDSUBROUTINE
    SUBROUTINE A2O_FFTW_BWD(rc_op)
        TYPE(OPERATOR_A2O),INTENT(INOUT)::rc_op
        CALL    CalcDistributedFourier(rc_op%DFD_Result,FFT_BWD)
        rc_op%field_out4=rc_op%field_out4/rc_op%Nx2Ny2
    ENDSUBROUTINE
    SUBROUTINE CALC_FFT_OF_A2O_KERNEL(rc_op)
        TYPE(OPERATOR_A2O),INTENT(INOUT)::rc_op
        INTEGER::Ir

        INTEGER(MPI_CTL_KIND)::IERROR
        REAL(8)::time1,time2
        !!!! ----------------------ATTENTION DIRTY TRICK with output of CalcDistributedFourier(rc_op%DFD_*,FFT_FWD) -----------!!!!
        time1=GetTime()
        DO Ir=1,rc_op%Nr
            rc_op%field_in4=rc_op%G_E_fftw(:,1:3,Ir,:,:)
            CALL A2O_FFTW_FWD(rc_op)
            rc_op%G_E_fftw(:,1:3,Ir,:,:)=rc_op%field_in4

            rc_op%field_in4=rc_op%G_E_fftw(:,4:6,Ir,:,:)
            CALL A2O_FFTW_FWD(rc_op)
            rc_op%G_E_fftw(:,4:6,Ir,:,:)=rc_op%field_in4

            rc_op%field_in4(:,1:2,:,:)=rc_op%G_E_fftw(:,7:8,Ir,:,:)
            rc_op%field_in4(:,3,:,:)=rc_op%G_H_fftw(:,1,Ir,:,:)
            CALL A2O_FFTW_FWD(rc_op)
            rc_op%G_E_fftw(:,7:8,Ir,:,:)=rc_op%field_in4(:,1:2,:,:)
            rc_op%G_H_fftw(:,1,Ir,:,:)=rc_op%field_in4(:,3,:,:)

            rc_op%field_in4=rc_op%G_H_fftw(:,2:4,Ir,:,:)
            CALL A2O_FFTW_FWD(rc_op)
            rc_op%G_H_fftw(:,2:4,Ir,:,:)=rc_op%field_in4

            rc_op%field_in4=rc_op%G_H_fftw(:,5:7,Ir,:,:)
            CALL A2O_FFTW_FWD(rc_op)
            rc_op%G_H_fftw(:,5:7,Ir,:,:)=rc_op%field_in4
        ENDDO
        time2=GetTime()
        CALL PRINT_CALC_TIME('FFT of RC tensor has been computed: ',time2-time1)
        rc_op%counter%tensor_fft=time2-time1
    ENDSUBROUTINE

    FUNCTION ALLOCATE_A2O_PTRS(anomaly,P,Nr,Np) RESULT(a2o_ptrs)
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        TYPE(POL_ORDER_TYPE),INTENT(IN)::P
        INTEGER(MPI_CTL_KIND),INTENT(IN)::Nr,Np 
        TYPE (OPERATOR_C_PTR)::a2o_ptrs
        INTEGER(C_INTPTR_T)::local_len
        INTEGER::Nd
        local_len=A2O_LOCAL_KERNEL_SIZE(anomaly,P,Nr,Np) 

        a2o_ptrs%kernel=ALLOCATE_COMPLEX(local_len)
        a2o_ptrs%kernel_length=local_len
        CALL PRINT_STORAGE_SIZE('A2O matrix needs', local_len*R_TWO*REALSIZE/1024/1024/1024)

        Nd=anomaly%Nz*(P%Nx+1)*(P%Ny+1)*(P%Nz+1)
        local_len=CalcLocalFFTSize(2*anomaly%Nx,2*anomaly%Ny,3*Nd,Np)
        a2o_ptrs%input_fft_length=local_len
        a2o_ptrs%input_fft_in=ALLOCATE_COMPLEX(local_len)
        a2o_ptrs%input_fft_out=ALLOCATE_COMPLEX(local_len)

        local_len=CalcLocalFFTSize(2*anomaly%Nx,2*anomaly%Ny,3*Nr,Np)

        a2o_ptrs%output_fft_length=local_len
        a2o_ptrs%output_fft_in=ALLOCATE_COMPLEX(local_len)
        a2o_ptrs%output_fft_out=ALLOCATE_COMPLEX(local_len)
    ENDFUNCTION

    SUBROUTINE DEALLOCATE_A2O_PTRS(a2o_ptrs)
        TYPE (OPERATOR_C_PTR),INTENT(INOUT)::a2o_ptrs
        CALL DEALLOCATE_COMPLEX(a2o_ptrs%kernel)
        a2o_ptrs%kernel=C_NULL_PTR
        a2o_ptrs%kernel_length=-ONE
        CALL DEALLOCATE_COMPLEX(a2o_ptrs%input_fft_in)
        CALL DEALLOCATE_COMPLEX(a2o_ptrs%input_fft_out)
        a2o_ptrs%input_fft_length=-ONE
        CALL DEALLOCATE_COMPLEX(a2o_ptrs%output_fft_in)
        CALL DEALLOCATE_COMPLEX(a2o_ptrs%output_fft_out)
        a2o_ptrs%output_fft_length=-ONE
        a2o_ptrs%output_fft_in=C_NULL_PTR
        a2o_ptrs%input_fft_in=C_NULL_PTR
        a2o_ptrs%output_fft_out=C_NULL_PTR
        a2o_ptrs%input_fft_out=C_NULL_PTR
    ENDSUBROUTINE

    FUNCTION A2O_LOCAL_KERNEL_SIZE(anomaly,P,Nr,Np) RESULT(local_length)!Quantity of  complex numbers
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        TYPE(POL_ORDER_TYPE),INTENT(IN)::P
        INTEGER(MPI_CTL_KIND),INTENT(IN)::Nr,Np 
        INTEGER(LONG_INT)::local_length
        INTEGER::Nu,Nxyloc
        Nu=anomaly%Nz*(P%Nx+1)*(P%Ny+1)*(P%Nz+1)
        Nxyloc=anomaly%Nx*anomaly%Ny*4/Np
        local_length=15*Nu*Nxyloc*Nr
    ENDFUNCTION

    SUBROUTINE DELETE_OPERATOR_A2O(rc_op)
        TYPE(OPERATOR_A2O),INTENT(INOUT)::rc_op
        rc_op%field_in4=>NULL()
        rc_op%field_in3=>NULL()
        rc_op%field_out4=>NULL()
        rc_op%field_out3=>NULL()
        rc_op%G_E=>NULL()
        rc_op%G_E_fftw=>NULL()
        rc_op%G_E4=>NULL()
        rc_op%G_H=>NULL()
        rc_op%G_H_fftw=>NULL()
        rc_op%G_H4=>NULL()
        !CALL DeleteDistributedFourierData(rc_op%DFD_Current)
        !CALL DeleteDistributedFourierData(rc_op%DFD_Result)
        IF (rc_op%SELF_ALLOCATED) CALL DEALLOCATE_A2O_PTRS(rc_op%cptrs) 
    ENDSUBROUTINE
ENDMODULE
