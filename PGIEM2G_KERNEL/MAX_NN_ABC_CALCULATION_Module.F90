MODULE CALCULATE_EXP_ABS_DIRECT_MODULE 
    USE ABC_COEFFS_MODULE 
    USE CALCULATE_POLYNOMIAL_MODULE 
    USE SHIFTED_EXPONENT_MODULE 
IMPLICIT NONE 
PRIVATE 

    INTEGER, PARAMETER ::DP = SELECTED_REAL_KIND(15) 
    INTEGER, PARAMETER ::NP_HARD_LIMIT=128 
    INTEGER, PARAMETER ::NP_MAX= 33 
    PUBLIC:: CALCULATE_ABC_NMAX, GET_NPMAX
    PUBLIC:: NP_MAX, NP_HARD_LIMIT 
CONTAINS 
    SUBROUTINE CALCULATE_ABC_NMAX(Np,X,E,A,B,C,Nm) 
        INTEGER,     INTENT(IN)::Np 
        COMPLEX(DP), INTENT(IN)::X,E 
        COMPLEX(DP), INTENT(OUT)::A(0:1), B(0:1), C(0:1) 
        INTEGER,     INTENT(OUT)::Nm 
        SELECT CASE (Np) 
            CASE(0) 
                CALL CALCULATE_ABC_0(X,E,A(0),B(0),C(0))
                Nm=Np
            CASE(1) 
                CALL CALCULATE_ABC_0(X,E,A(0),B(0),C(0))
                CALL CALCULATE_ABC_1(X,E,A(1),B(1),C(1))
                Nm=Np
            CASE(2) 
                CALL CALCULATE_ABC_1(X,E,A(1),B(1),C(1))
                CALL CALCULATE_ABC_2(X,E,A(0),B(0),C(0))
                Nm=Np
            CASE(3) 
                CALL CALCULATE_ABC_2(X,E,A(0),B(0),C(0))
                CALL CALCULATE_ABC_3(X,E,A(1),B(1),C(1))
                Nm=Np
            CASE(4) 
                CALL CALCULATE_ABC_3(X,E,A(1),B(1),C(1))
                CALL CALCULATE_ABC_4(X,E,A(0),B(0),C(0))
                Nm=Np
            CASE(5) 
                CALL CALCULATE_ABC_4(X,E,A(0),B(0),C(0))
                CALL CALCULATE_ABC_5(X,E,A(1),B(1),C(1))
                Nm=Np
            CASE(6) 
                CALL CALCULATE_ABC_5(X,E,A(1),B(1),C(1))
                CALL CALCULATE_ABC_6(X,E,A(0),B(0),C(0))
                Nm=Np
            CASE(7) 
                CALL CALCULATE_ABC_6(X,E,A(0),B(0),C(0))
                CALL CALCULATE_ABC_7(X,E,A(1),B(1),C(1))
                Nm=Np
            CASE(8) 
                CALL CALCULATE_ABC_7(X,E,A(1),B(1),C(1))
                CALL CALCULATE_ABC_8(X,E,A(0),B(0),C(0))
                Nm=Np
            CASE(9) 
                CALL CALCULATE_ABC_8(X,E,A(0),B(0),C(0))
                CALL CALCULATE_ABC_9(X,E,A(1),B(1),C(1))
                Nm=Np
            CASE(10) 
                CALL CALCULATE_ABC_9(X,E,A(1),B(1),C(1))
                CALL CALCULATE_ABC_10(X,E,A(0),B(0),C(0))
                Nm=Np
            CASE( 11: NP_MAX ) 
                CALL CALCULATE_ABC_32(X,E,A(0),B(0),C(0))
                CALL CALCULATE_ABC_33(X,E,A(1),B(1),C(1))
                Nm=33
            CASE( NP_MAX+1 : NP_HARD_LIMIT ) 
                 A=0.0_DP; B=0.0_DP; C=0.0_DP
                Nm=128
            CASE DEFAULT 
                 STOP 10 
        END SELECT
    ENDSUBROUTINE

    FUNCTION GET_NPMAX(Np) RESULT(RES) 
        INTEGER,     INTENT(IN)::Np 
        INTEGER     RES 
        RES=MIN(Np,33,128) 
    ENDFUNCTION

!!!!!!!!      PRIVATE    

    SUBROUTINE CALCULATE_ABC_0(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 0.5) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,1*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_0_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_0_4, x) 
            A=x**SINGL_POW_A_0_3*p0+x**SINGL_POW_A_0_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_0_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_0_4, x) 
            B=x**SINGL_POW_B_0_3*p0+x**SINGL_POW_B_0_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_0_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_0_4, x) 
            C=x**SINGL_POW_C_0_3*p0+x**SINGL_POW_C_0_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_0_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_0_2, y) 
            A=y**SINGL_POW_A_0_1*p0+y**SINGL_POW_A_0_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_0_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_0_2, y) 
            B=y**SINGL_POW_B_0_1*p0+y**SINGL_POW_B_0_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_0_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_0_2, y) 
            C=y**SINGL_POW_C_0_1*p0+y**SINGL_POW_C_0_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_1(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 1) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,2*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_1_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_1_4, x) 
            A=x**SINGL_POW_A_1_3*p0+x**SINGL_POW_A_1_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_1_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_1_4, x) 
            B=x**SINGL_POW_B_1_3*p0+x**SINGL_POW_B_1_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_1_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_1_4, x) 
            C=x**SINGL_POW_C_1_3*p0+x**SINGL_POW_C_1_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_1_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_1_2, y) 
            A=y**SINGL_POW_A_1_1*p0+y**SINGL_POW_A_1_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_1_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_1_2, y) 
            B=y**SINGL_POW_B_1_1*p0+y**SINGL_POW_B_1_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_1_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_1_2, y) 
            C=y**SINGL_POW_C_1_1*p0+y**SINGL_POW_C_1_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_2(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 1.5) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,3*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_2_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_2_4, x) 
            A=x**SINGL_POW_A_2_3*p0+x**SINGL_POW_A_2_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_2_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_2_4, x) 
            B=x**SINGL_POW_B_2_3*p0+x**SINGL_POW_B_2_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_2_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_2_4, x) 
            C=x**SINGL_POW_C_2_3*p0+x**SINGL_POW_C_2_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_2_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_2_2, y) 
            A=y**SINGL_POW_A_2_1*p0+y**SINGL_POW_A_2_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_2_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_2_2, y) 
            B=y**SINGL_POW_B_2_1*p0+y**SINGL_POW_B_2_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_2_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_2_2, y) 
            C=y**SINGL_POW_C_2_1*p0+y**SINGL_POW_C_2_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_3(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 2) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,4*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_3_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_3_4, x) 
            A=x**SINGL_POW_A_3_3*p0+x**SINGL_POW_A_3_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_3_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_3_4, x) 
            B=x**SINGL_POW_B_3_3*p0+x**SINGL_POW_B_3_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_3_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_3_4, x) 
            C=x**SINGL_POW_C_3_3*p0+x**SINGL_POW_C_3_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_3_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_3_2, y) 
            A=y**SINGL_POW_A_3_1*p0+y**SINGL_POW_A_3_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_3_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_3_2, y) 
            B=y**SINGL_POW_B_3_1*p0+y**SINGL_POW_B_3_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_3_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_3_2, y) 
            C=y**SINGL_POW_C_3_1*p0+y**SINGL_POW_C_3_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_4(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 2.5) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,5*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_4_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_4_4, x) 
            A=x**SINGL_POW_A_4_3*p0+x**SINGL_POW_A_4_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_4_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_4_4, x) 
            B=x**SINGL_POW_B_4_3*p0+x**SINGL_POW_B_4_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_4_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_4_4, x) 
            C=x**SINGL_POW_C_4_3*p0+x**SINGL_POW_C_4_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_4_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_4_2, y) 
            A=y**SINGL_POW_A_4_1*p0+y**SINGL_POW_A_4_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_4_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_4_2, y) 
            B=y**SINGL_POW_B_4_1*p0+y**SINGL_POW_B_4_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_4_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_4_2, y) 
            C=y**SINGL_POW_C_4_1*p0+y**SINGL_POW_C_4_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_5(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 3) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,6*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_5_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_5_4, x) 
            A=x**SINGL_POW_A_5_3*p0+x**SINGL_POW_A_5_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_5_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_5_4, x) 
            B=x**SINGL_POW_B_5_3*p0+x**SINGL_POW_B_5_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_5_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_5_4, x) 
            C=x**SINGL_POW_C_5_3*p0+x**SINGL_POW_C_5_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_5_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_5_2, y) 
            A=y**SINGL_POW_A_5_1*p0+y**SINGL_POW_A_5_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_5_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_5_2, y) 
            B=y**SINGL_POW_B_5_1*p0+y**SINGL_POW_B_5_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_5_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_5_2, y) 
            C=y**SINGL_POW_C_5_1*p0+y**SINGL_POW_C_5_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_6(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 3.5) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,7*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_6_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_6_4, x) 
            A=x**SINGL_POW_A_6_3*p0+x**SINGL_POW_A_6_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_6_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_6_4, x) 
            B=x**SINGL_POW_B_6_3*p0+x**SINGL_POW_B_6_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_6_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_6_4, x) 
            C=x**SINGL_POW_C_6_3*p0+x**SINGL_POW_C_6_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_6_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_6_2, y) 
            A=y**SINGL_POW_A_6_1*p0+y**SINGL_POW_A_6_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_6_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_6_2, y) 
            B=y**SINGL_POW_B_6_1*p0+y**SINGL_POW_B_6_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_6_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_6_2, y) 
            C=y**SINGL_POW_C_6_1*p0+y**SINGL_POW_C_6_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_7(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 4) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,8*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_7_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_7_4, x) 
            A=x**SINGL_POW_A_7_3*p0+x**SINGL_POW_A_7_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_7_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_7_4, x) 
            B=x**SINGL_POW_B_7_3*p0+x**SINGL_POW_B_7_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_7_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_7_4, x) 
            C=x**SINGL_POW_C_7_3*p0+x**SINGL_POW_C_7_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_7_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_7_2, y) 
            A=y**SINGL_POW_A_7_1*p0+y**SINGL_POW_A_7_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_7_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_7_2, y) 
            B=y**SINGL_POW_B_7_1*p0+y**SINGL_POW_B_7_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_7_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_7_2, y) 
            C=y**SINGL_POW_C_7_1*p0+y**SINGL_POW_C_7_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_8(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 4.5) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,9*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_8_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_8_4, x) 
            A=x**SINGL_POW_A_8_3*p0+x**SINGL_POW_A_8_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_8_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_8_4, x) 
            B=x**SINGL_POW_B_8_3*p0+x**SINGL_POW_B_8_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_8_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_8_4, x) 
            C=x**SINGL_POW_C_8_3*p0+x**SINGL_POW_C_8_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_8_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_8_2, y) 
            A=y**SINGL_POW_A_8_1*p0+y**SINGL_POW_A_8_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_8_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_8_2, y) 
            B=y**SINGL_POW_B_8_1*p0+y**SINGL_POW_B_8_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_8_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_8_2, y) 
            C=y**SINGL_POW_C_8_1*p0+y**SINGL_POW_C_8_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_9(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 5) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,10*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_9_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_9_4, x) 
            A=x**SINGL_POW_A_9_3*p0+x**SINGL_POW_A_9_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_9_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_9_4, x) 
            B=x**SINGL_POW_B_9_3*p0+x**SINGL_POW_B_9_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_9_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_9_4, x) 
            C=x**SINGL_POW_C_9_3*p0+x**SINGL_POW_C_9_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_9_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_9_2, y) 
            A=y**SINGL_POW_A_9_1*p0+y**SINGL_POW_A_9_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_9_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_9_2, y) 
            B=y**SINGL_POW_B_9_1*p0+y**SINGL_POW_B_9_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_9_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_9_2, y) 
            C=y**SINGL_POW_C_9_1*p0+y**SINGL_POW_C_9_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_10(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 5.5) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,11*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_10_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_10_4, x) 
            A=x**SINGL_POW_A_10_3*p0+x**SINGL_POW_A_10_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_10_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_10_4, x) 
            B=x**SINGL_POW_B_10_3*p0+x**SINGL_POW_B_10_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_10_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_10_4, x) 
            C=x**SINGL_POW_C_10_3*p0+x**SINGL_POW_C_10_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_10_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_10_2, y) 
            A=y**SINGL_POW_A_10_1*p0+y**SINGL_POW_A_10_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_10_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_10_2, y) 
            B=y**SINGL_POW_B_10_1*p0+y**SINGL_POW_B_10_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_10_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_10_2, y) 
            C=y**SINGL_POW_C_10_1*p0+y**SINGL_POW_C_10_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_32(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 16.5) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,33*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_32_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_32_4, x) 
            A=x**SINGL_POW_A_32_3*p0+x**SINGL_POW_A_32_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_32_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_32_4, x) 
            B=x**SINGL_POW_B_32_3*p0+x**SINGL_POW_B_32_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_32_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_32_4, x) 
            C=x**SINGL_POW_C_32_3*p0+x**SINGL_POW_C_32_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_32_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_32_2, y) 
            A=y**SINGL_POW_A_32_1*p0+y**SINGL_POW_A_32_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_32_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_32_2, y) 
            B=y**SINGL_POW_B_32_1*p0+y**SINGL_POW_B_32_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_32_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_32_2, y) 
            C=y**SINGL_POW_C_32_1*p0+y**SINGL_POW_C_32_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
    SUBROUTINE CALCULATE_ABC_33(X,E,A,B,C)
        COMPLEX(DP),INTENT(IN)::X,E 
        COMPLEX(DP),INTENT(OUT)::A, B, C 
        COMPLEX(DP)::y,se 
        COMPLEX(DP)::p0,p1 
        IF (ABS(X) < 17) THEN 
            se=SHIFTED_EXP(-2.0_DP*X,e,34*N_NORM)
            p0=COMPUTE_POLYNOMIAL(SINGL_A_33_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_33_4, x) 
            A=x**SINGL_POW_A_33_3*p0+x**SINGL_POW_A_33_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_33_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_33_4, x) 
            B=x**SINGL_POW_B_33_3*p0+x**SINGL_POW_B_33_4*p1*se 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_33_3, x) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_33_4, x) 
            C=x**SINGL_POW_C_33_3*p0+x**SINGL_POW_C_33_4*p1*se 

        ELSE
            y=1.0_DP/x
            p0=COMPUTE_POLYNOMIAL(SINGL_A_33_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_A_33_2, y) 
            A=y**SINGL_POW_A_33_1*p0+y**SINGL_POW_A_33_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_B_33_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_B_33_2, y) 
            B=y**SINGL_POW_B_33_1*p0+y**SINGL_POW_B_33_2*p1*e 

            p0=COMPUTE_POLYNOMIAL(SINGL_C_33_1, y) 
            p1=COMPUTE_POLYNOMIAL(SINGL_C_33_2, y) 
            C=y**SINGL_POW_C_33_1*p0+y**SINGL_POW_C_33_2*p1*e 

        ENDIF
    ENDSUBROUTINE 
END MODULE
