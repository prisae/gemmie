
MODULE  RADIAL_DATA_TYPES_MODULE
    USE PRECISION_CONTROL_MODULE
    USE GEMMIE_CONSTANTS
    USE ARRAYS_UTILITIES
    IMPLICIT NONE

    TYPE :: MEDIA_LAYER_TYPE
        REAL(DP)::r1,r2
        REAL(DP)::s=-R_ONE
        REAL(DP)::sigma=-1
        REAL(DP)::eps=1
        REAL(DP)::omega=-1
        COMPLEX(DP)::c_sigma=-1
        COMPLEX(DP)::k=HUGE(R_ONE), k2=HUGE(R_ONE)
        COMPLEX(DP)::z1=HUGE(R_ONE), z2=HUGE(R_ONE)
        COMPLEX(DP)::iwm=HUGE(R_ONE)

    ENDTYPE

    TYPE :: SPHERICAL_LAYER_TYPE
        REAL(DP)::r1,r2
        REAL(DP)::s=-R_ONE
        REAL(DP)::sigma=-1
        REAL(DP)::eps=1
        REAL(DP)::omega=-1
        COMPLEX(DP)::c_sigma=-1
        
        COMPLEX(DP)::k=HUGE(R_ONE), k2=HUGE(R_ONE)
        COMPLEX(DP)::z1=HUGE(R_ONE), z2=HUGE(R_ONE)
        COMPLEX(DP)::iwm=HUGE(R_ONE)

        INTEGER::N=-1

        COMPLEX(DP),ALLOCATABLE::Rj1(:)
        COMPLEX(DP),ALLOCATABLE::Rj2(:)
        COMPLEX(DP),ALLOCATABLE::Rh1(:)
        COMPLEX(DP),ALLOCATABLE::Rh2(:)
        COMPLEX(DP),ALLOCATABLE::Kappa(:)
    ENDTYPE

    TYPE ::  OPERATOR_LAYER_TYPE
        TYPE (SPHERICAL_LAYER_TYPE) :: Spherical_Layer
        REAL(DP),ALLOCATABLE::sn(:)
        INTEGER :: I_bkg=-1 !position in background
        COMPLEX(DP),ALLOCATABLE :: Kappa_j(:)
        COMPLEX(DP),ALLOCATABLE :: Kappa_h(:)
        COMPLEX(DP),ALLOCATABLE :: a_bkg(:,:)
        COMPLEX(DP),ALLOCATABLE :: b_bkg(:,:)
    ENDTYPE


    TYPE :: BACKGROUND_TYPE

        INTEGER::Nl
        REAL(DP)  :: MainSphereRadius=-R_ONE
        LOGICAL   :: normalized=.FALSE.
        REAL(DP), ALLOCATABLE ::r(:)
        REAL(DP), ALLOCATABLE :: sigma(:)
        REAL(DP), ALLOCATABLE :: eps(:)
        REAL(DP) :: OUTSIDE_CONDUCTITY
        REAL(DP):: w=-R_ONE
        TYPE(MEDIA_LAYER_TYPE),ALLOCATABLE::Layers(:)
    ENDTYPE

    TYPE :: RADIAL_OPERATOR_TYPE
        INTEGER::Nr=-1
        INTEGER::N=-1
        TYPE(OPERATOR_LAYER_TYPE), ALLOCATABLE::Layers(:)
    ENDTYPE

    

    TYPE :: SPH_DOMAIN_SHAPE_TYPE
        LOGICAL :: GLOBE=.TRUE.
        INTEGER :: PHI_TYPE=PHI_GLOBE ! to exploit symmetries in FFT
        INTEGER :: Nphi=-1
        INTEGER :: Nphi_loc(2)=[1, -1]
        INTEGER :: Ntheta=-1
        INTEGER :: Nr=-1
        INTEGER :: Mr1=-1, Mr2=-1 !Layers Mr1:Mr2 in bkg
        REAL(DP) :: dphi=-R_ONE
        REAL(DP) :: phi0=R_ZERO
        REAL(DP) :: Domain_Radius=-R_ONE
        REAL(DP), ALLOCATABLE :: r(:)      !cells boundaries
        REAL(DP), ALLOCATABLE :: thetas(:) !cells boundaries
        REAL(DP), ALLOCATABLE :: phis(:)   !cells boundaries
    ENDTYPE

    


    INTERFACE ASSIGNMENT(=)
        MODULE PROCEDURE SET_MEDIA_TO_OPERATOR
    ENDINTERFACE

    INTERFACE GET_CELL_INDEX
        MODULE PROCEDURE GET_CELL_INDEX1
        MODULE PROCEDURE GET_CELL_INDEX2
    ENDINTERFACE

CONTAINS

    PURE SUBROUTINE SET_MEDIA_TO_OPERATOR(op_layer, med_layer)
        TYPE(MEDIA_LAYER_TYPE),INTENT(IN)::med_layer
        TYPE(SPHERICAL_LAYER_TYPE),INTENT(INOUT)::op_layer
        op_layer%r1      =med_layer%r1         
        op_layer%r2      =med_layer%r2
        op_layer%s       =med_layer%s
        op_layer%sigma   =med_layer%sigma
        op_layer%eps     =med_layer%eps
        op_layer%omega   =med_layer%omega
        op_layer%c_sigma =med_layer%c_sigma
        op_layer%k       =med_layer%k
        op_layer%k2      =med_layer%k2
        op_layer%z1      =med_layer%z1
        op_layer%z2      =med_layer%z2
        op_layer%iwm     =med_layer%iwm
    ENDSUBROUTINE

    SUBROUTINE SET_MODEL_RADIUS(BKG)
        TYPE (BACKGROUND_TYPE), INTENT(INOUT) :: BKG
        BKG%MainSphereRadius=BKG%r(UBOUND(BKG%r,1))
        BKG%normalized=.TRUE.
    ENDSUBROUTINE


    FUNCTION GET_CELL_INDEX1(x, D) RESULT(RES)
        REAL(DP), INTENT(IN) :: x
        REAL(DP), INTENT(IN) :: D(0:)
        INTEGER :: RES
        IF (x<D(0)) THEN
            RES=0
        ELSEIF(x>D(SIZE(D)-1)) THEN
            RES=-SIZE(D)
        ELSE
            RES=MINLOC(DIM(x, D),1)-1
            RES=MAX(RES,1)
            IF (x>=D(RES)) RES=RES+1
        ENDIF
    ENDFUNCTION


    FUNCTION GET_CELL_INDEX2(x, D) RESULT(RES)
        REAL(DP), INTENT(IN) :: x(2)
        REAL(DP), INTENT(IN) :: D(0:)
        INTEGER :: RES(2)
        RES(1)=GET_CELL_INDEX(x(1)*(R_ONE+R_ACCURACY),D)
        RES(2)=GET_CELL_INDEX(x(2)*(R_ONE-R_ACCURACY),D)
    ENDFUNCTION


    FUNCTION CALC_RADIAL_OPERATOR_MEM(rad_op) RESULT(RES)
        TYPE(RADIAL_OPERATOR_TYPE):: rad_op
        REAL(DP) :: RES
        INTEGER :: I
        RES=R_ZERO
        IF (ALLOCATED(rad_op%Layers)) THEN
            DO I=1, SIZE(rad_OP%Layers)
                IF (ALLOCATED(rad_op%Layers(I)%Kappa_j)) THEN
                    RES=RES+SIZE(rad_op%Layers(I)%Kappa_j)*R_SIZE_GB*R_TWO
                ENDIF
                IF (ALLOCATED(rad_op%Layers(I)%Kappa_h)) THEN
                    RES=RES+SIZE(rad_op%Layers(I)%Kappa_h)*R_SIZE_GB*R_TWO
                ENDIF
                IF (ALLOCATED(rad_op%Layers(I)%a_bkg)) THEN
                    RES=RES+SIZE(rad_op%Layers(I)%a_bkg)*R_SIZE_GB*R_TWO
                ENDIF
                IF (ALLOCATED(rad_op%Layers(I)%b_bkg)) THEN
                    RES=RES+SIZE(rad_op%Layers(I)%b_bkg)*R_SIZE_GB*R_TWO
                ENDIF
                
                IF (ALLOCATED(rad_op%Layers(I)%sn)) THEN
                    RES=RES+SIZE(rad_op%Layers(I)%sn)*R_SIZE_GB
                ENDIF

                IF (ALLOCATED(rad_op%Layers(I)%Spherical_Layer%Rj1)) THEN
                    RES=RES+SIZE(rad_op%Layers(I)%Spherical_Layer%Rj1)*R_SIZE_GB*R_TWO
                ENDIF

                IF (ALLOCATED(rad_op%Layers(I)%Spherical_Layer%Rj2)) THEN
                    RES=RES+SIZE(rad_op%Layers(I)%Spherical_Layer%Rj2)*R_SIZE_GB*R_TWO
                ENDIF

                IF (ALLOCATED(rad_op%Layers(I)%Spherical_Layer%Rh1)) THEN
                    RES=RES+SIZE(rad_op%Layers(I)%Spherical_Layer%Rh1)*R_SIZE_GB*R_TWO
                ENDIF

                IF (ALLOCATED(rad_op%Layers(I)%Spherical_Layer%Rh2)) THEN
                    RES=RES+SIZE(rad_op%Layers(I)%Spherical_Layer%Rh2)*R_SIZE_GB*R_TWO
                ENDIF

                IF (ALLOCATED(rad_op%Layers(I)%Spherical_Layer%kappa)) THEN
                    RES=RES+SIZE(rad_op%Layers(I)%Spherical_Layer%kappa)*R_SIZE_GB*R_TWO
                ENDIF
            ENDDO
        ENDIF
    ENDFUNCTION

    SUBROUTINE CLEAN_BACKGROUND(bkg)
        TYPE(BACKGROUND_TYPE), INTENT(INOUT) :: bkg
        IF (ALLOCATED(bkg%r)) DEALLOCATE(bkg%r)
        IF (ALLOCATED(bkg%sigma)) DEALLOCATE(bkg%sigma)
        IF (ALLOCATED(bkg%eps)) DEALLOCATE(bkg%eps)
        IF (ALLOCATED(bkg%Layers)) DEALLOCATE(bkg%Layers)
    ENDSUBROUTINE
    
END MODULE  RADIAL_DATA_TYPES_MODULE
