#!/bin/sh


for NM in IE_LT IE_LP IE_LR IE_RR
do
	echo $NM
	sed "s/#NAME#/$NM/g" ./template_double_large_n > ../Double/DOUBLE_SMALL_KR_G_$NM"_INC.F90"
	sed "s/#NAME#/$NM/g" ./template_double_small_sn2 > ../Double/DOUBLE_SMALL_SN_G_$NM"_HERMIT_INC.F90"
done

for K in 1 2 3 4 5
do
	echo $K
	sed "s/#N#/$K/g" ./template_single_small_sn > ../Single/SINGLE_SMALL_SN_$K"_INC.F90"
	sed "s/#N#/$K/g" ./template_single_large_n > ../Single/SINGLE_LARGE_N_$K"_INC.F90"
done


