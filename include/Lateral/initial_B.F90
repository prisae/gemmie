!\eqref{ initial_B_0 } 
V0(BS) = R_FOUR*cnu*skappa*smu*stau
V0(BC) = R_FOUR*ckappa*mu*stau
V0(B0) = R_FOUR*mu*tau
V0(BDSS) = R_ZERO
V0(BDSC) = R_ZERO
V0(BDS) = R_ZERO
V0(BDC) = R_ZERO
V0(BD0) = R_ZERO
V0(BDDSS) = R_ZERO
V0(BDDSC) = R_ZERO
V0(BDDS) = R_ZERO
V0(BDDC) = R_ZERO
V0(BDD0) = R_ZERO
!\eqref{ initial_B_1 } 
V1(BS) = (R_FOUR*cnu*skappa*smu*stau*gamma)/alpha-R_TWO*cmu*smu*&
    &snu2*stheta*tau+cmu*smu*stheta*tau+mu*stheta*tau-R_FOUR*cmu*ctau*&
    &skappa2*smu*snu2*stau*stheta+R_TWO*cmu*ctau*smu*snu2*stau*stheta+&
    &R_TWO*cmu*ctau*skappa2*smu*stau*stheta-cmu*ctau*smu*stau*stheta+&
    &R_TWO*ctau*mu*skappa2*stau*stheta-ctau*mu*stau*stheta+R_FOUR*&
    &ckappa*cnu*ctau*ctheta*skappa*smu*stau
V1(BC) = (R_FOUR*ckappa*mu*stau*gamma)/alpha+R_TWO*ctheta*mu*tau+&
    &R_FOUR*ckappa*cnu*ctau*skappa*smu*stau*stheta+R_FOUR*ckappa2*&
    &ctau*ctheta*mu*stau-R_TWO*ctau*ctheta*mu*stau
V1(B0) = (R_FOUR*mu*tau*gamma)/alpha+R_FOUR*cnu*skappa*smu*stau*&
    &stheta+R_FOUR*ckappa*ctheta*mu*stau
V1(BDSS) = R_TWO*mu*tau+R_FOUR*ctau*mu*skappa2*stau-R_TWO*ctau*&
    &mu*stau
V1(BDSC) = R_FOUR*ckappa*cnu*ctau*skappa*smu*stau
V1(BDS) = R_FOUR*cnu*skappa*smu*stau
V1(BDC) = R_FOUR*ckappa*mu*stau
V1(BD0) = R_FOUR*mu*tau
V1(BDDSS) = R_ZERO
V1(BDDSC) = R_ZERO
V1(BDDS) = R_ZERO
V1(BDDC) = R_ZERO
V1(BDD0) = R_ZERO
