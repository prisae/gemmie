!\eqref{ initial_A_0 } 
V0(AS) = R_FOUR*mu*skappa*stau
V0(AC) = R_FOUR*ckappa*cnu*smu*stau
V0(AD0) = R_ZERO
V0(ADS) = R_ZERO
V0(ADC) = R_ZERO
V0(ADD0) = R_ZERO
V0(ADDS) = R_ZERO
V0(ADDC) = R_ZERO
!\eqref{ initial_A_1 } 
V1(AS) = (R_FOUR*mu*skappa*stau*gamma)/alpha+R_TWO*cnu*smu*&
    &stheta*tau+R_FOUR*cnu*ctau*skappa2*smu*stau*stheta-R_TWO*cnu*ctau*smu*&
    &stau*stheta+R_FOUR*ckappa*ctau*ctheta*mu*skappa*stau
V1(AC) = (R_FOUR*ckappa*cnu*smu*stau*gamma)/alpha+R_TWO*cnu*&
    &ctheta*smu*tau-R_FOUR*ckappa*cmu*ctau*skappa*smu*snu2*stau*stheta+&
    &R_TWO*ckappa*cmu*ctau*skappa*smu*stau*stheta+R_TWO*ckappa*ctau*mu*&
    &skappa*stau*stheta-R_FOUR*cnu*ctau*ctheta*skappa2*smu*stau+R_TWO*&
    &cnu*ctau*ctheta*smu*stau
V1(AD0) = R_FOUR*cnu*smu*tau
V1(ADS) = R_FOUR*mu*skappa*stau
V1(ADC) = R_FOUR*ckappa*cnu*smu*stau
V1(ADD0) = R_ZERO
V1(ADDS) = R_ZERO
V1(ADDC) = R_ZERO
