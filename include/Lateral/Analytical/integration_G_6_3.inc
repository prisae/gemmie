!INTEGER, PARAMETER:: G_6_COEFFS_LEN = 22 

PURE FUNCTION  CONVERT_COEFFS_FOR_G_6 (A, y1, y2) RESULT(U) 
COMPLEX(DP),INTENT(IN) :: A(0:MAX_COS_ORDER) 
REAL(DP), INTENT(IN) :: y1, y2 
COMPLEX(DP):: U(1: G_6_COEFFS_LEN ) 
REAL(DP):: l, g 

l = y2-y1
g = (1-y2)/l

U(1) = -(A(3)*3.75E+0_DP)/l**3
U(2) = (1.875E+0_DP*A(3))/l**3
U(3) = ((g*(A(3)*3.E+1_DP*g+A(3)*3.E+1_DP-A(2)*6.E&
	&+0_DP)+A(3)*6.E+0_DP-A(2)*3.E+0_DP+A(1)*1.E+0_DP)*l**2&
	&+A(3)*5.625E+0_DP)/l**3
U(4) = (A(3)*5.625E+0_DP)/l**3
U(5) = (4.6875E-1_DP*A(3))/l**3
U(6) = -(1.40625E+0_DP*A(3))/l**3
U(7) = ((g*((-A(3)*3.E+1_DP*g)-A(3)*3.E+1_DP&
	&+A(2)*6.E+0_DP)-A(3)*6.E+0_DP+A(2)*3.E+0_DP-A(1)*1.E+0_DP)*l*&
	&*2-A(3)*6.09375E+0_DP)/l**3
U(8) = -(A(3)*4.21875E+0_DP)/l**3
U(9) = ((-A(3)*2.25E+1_DP*g)-A(3)*1.125E+1_DP&
	&+A(2)*2.25E+0_DP)/l**2
U(10) = (A(3)*7.5E+0_DP*g&
	&+A(3)*3.75E+0_DP-7.499999999999999E-1_DP*A(2))/l**2
U(11) = (A(3)*1.5E+1_DP*g+A(3)&
	&*7.5E+0_DP-1.4999999999999997E+0_DP*A(2))/l**2
U(12) = ((-A(3)*1.5E+1_DP*g)-A(3)&
	&*7.5E+0_DP+1.4999999999999997E+0_DP*A(2))/l**2
U(13) = (A(3)*3.75E+0_DP)/l**3
U(14) = -(1.875E+0_DP*A(3))/l**3
U(15) = -(1.875E+0_DP*A(3))/l**3
U(16) = -(1.875E+0_DP*A(3))/l**3
U(17) = (2.34375E+0_DP*A(3))/l**3
U(18) = (4.6875E-1_DP*A(3))/l**3
U(19) = -(4.6875E-1_DP*A(3))/l**3
U(20) = (1.40625E+0_DP*A(3))/l**3
U(21) = (A(3)*2.25E+1_DP*g+A(3)*1.125E&
	&+1_DP-A(2)*2.25E+0_DP)/l**2
U(22) = ((-A(3)*7.5E+0_DP*g)-A(3)&
	&*3.75E+0_DP+7.499999999999999E-1_DP*A(2))/l**2
ENDFUNCTION 


PURE FUNCTION  CALCULATE_ANALYTICALLY_G_6 (S0_X, S0_Y, S0_Z, SC_X, SC_Y, SC_Z, U)& 
			& RESULT(RES) 
REAL(DP), INTENT(IN)::S0_X, S0_Y, S0_Z 
REAL(DP), INTENT(IN)::SC_X(PMAX,2), SC_Y(PMAX,2), SC_Z(PMAX,2) 
COMPLEX(DP),INTENT(IN)::U(1: G_6_COEFFS_LEN ) 
COMPLEX(DP)::RES 
RES = (SC_X(3,2)*SC_Z(3,2)*U(20)+SC_Z(1,2)&
	&*SC_X(3,2)*U(19)+SC_X(1,2)*SC_Z(3,2)*U(8)+SC_X(1,2)*SC_Z(1,2)*U(7))&
	&*S0_y+(SC_Z(2,2)*SC_Y(3,2)*U(22)+SC_Y(1,2)*SC_Z(2,2)*U(21))*S0_x&
	&+SC_X(3,2)*SC_Z(3,2)*SC_Y(4,1)*U(18)+SC_Z(1,2)*SC_X(3,2)*SC_Y(4,1)&
	&*U(17)+SC_Y(2,1)*SC_X(3,2)*SC_Z(3,2)*U(16)+SC_Z(1,2)*SC_Y(2,1)&
	&*SC_X(3,2)*U(15)+SC_Z(2,2)*SC_X(3,1)*SC_Y(4,2)*U(14)+SC_Y(2,2)*SC_Z(2,2)&
	&*SC_X(3,1)*U(13)+SC_Z(1,2)*SC_X(2,2)*SC_Y(3,1)*U(12)&
	&+SC_Y(1,1)*SC_Z(1,2)*SC_X(2,2)*U(11)+SC_X(2,1)*SC_Z(2,2)*SC_Y(3,2)*U(10)&
	&+SC_Y(1,2)*SC_X(2,1)*SC_Z(2,2)*U(9)+SC_X(1,2)*SC_Z(3,2)*SC_Y(4,1)&
	&*U(6)+SC_X(1,2)*SC_Z(1,2)*SC_Y(4,1)*U(5)+SC_X(1,1)*U(2)*SC_Z(2,2)&
	&*SC_Y(4,2)+SC_X(1,2)*SC_Y(2,1)*SC_Z(3,2)*U(4)+SC_X(1,2)*SC_Z(1,2)*SC_Y(2,1)&
	&*U(3)+U(1)*SC_X(1,1)*SC_Y(2,2)*SC_Z(2,2)
ENDFUNCTION 
