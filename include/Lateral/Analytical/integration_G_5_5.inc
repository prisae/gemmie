!INTEGER, PARAMETER:: G_5_COEFFS_LEN = 117 

PURE FUNCTION  CONVERT_COEFFS_FOR_G_5 (A, y1, y2) RESULT(U) 
COMPLEX(DP),INTENT(IN) :: A(0:MAX_COS_ORDER) 
REAL(DP), INTENT(IN) :: y1, y2 
COMPLEX(DP):: U(1: G_5_COEFFS_LEN ) 
REAL(DP):: l, g 

l = y2-y1
g = (1-y2)/l

U(1) = ((g*(g*((-A(5)*4.725E+2_DP*g)-A(5)*7.0875E&
	&+2_DP+A(4)*7.875E+1_DP)-A(5)*3.15E+2_DP+A(4)*7.875E&
	&+1_DP-A(3)*1.125E+1_DP)-A(5)*3.9375E+1_DP+A(4)*1.6875E+1_DP-A(3)*5.625E&
	&+0_DP+1.125E+0_DP*A(2))*l**2-A(5)*1.96875E+2_DP*g-A(5)*9.84375E&
	&+1_DP+A(4)*1.09375E+1_DP)/l**4
U(2) = ((-A(5)*4.921875E+1_DP*g)-A(5)&
	&*2.4609375E+1_DP+2.734375E+0_DP*A(4))/l**4
U(3) = ((g*(g*((-A(5)*1.575E+2_DP*g)-A(5)*2.3625E&
	&+2_DP+A(4)*2.625E+1_DP)-A(5)*1.0500000000000001E+2_DP+A(4)&
	&*2.625E+1_DP-A(3)*3.75E+0_DP)-A(5)*1.3125E+1_DP+A(4)*5.625E&
	&+0_DP-1.875E+0_DP*A(3)+3.7499999999999994E-1_DP*A(2))*l**2-A(5)&
	&*6.890625E+1_DP*g-A(5)*3.4453125E+1_DP+3.828125E+0_DP*A(4))/l**4
U(4) = ((g*(g*(A(5)*1.575E+2_DP*g+A(5)&
	&*2.3625E+2_DP-A(4)*2.625E+1_DP)+A(5)*1.0500000000000001E+2_DP-A(4)*2.625E&
	&+1_DP+A(3)*3.75E+0_DP)+A(5)*1.3125E+1_DP-A(4)*5.625E+0_DP&
	&+1.875E+0_DP*A(3)-3.7499999999999994E-1_DP*A(2))*l**2+A(5)*9.84375E+1_DP&
	&*g+A(5)*4.921875E+1_DP-A(4)*5.46875E+0_DP)/l**4
U(5) = (A(5)*2.4609375E+1_DP*g&
	&+A(5)*1.23046875E+1_DP-1.3671875E+0_DP*A(4))/l**4
U(6) = ((g*(g*(A(5)*4.725E+2_DP*g+A(5)&
	&*7.0875E+2_DP-A(4)*7.875E+1_DP)+A(5)*3.15E+2_DP-A(4)*7.875E+1_DP+A(3)&
	&*1.125E+1_DP)+A(5)*3.9375E+1_DP-A(4)*1.6875E+1_DP+A(3)&
	&*5.625E+0_DP-1.125E+0_DP*A(2))*l**2+A(5)*1.91953125E+2_DP*g+A(5)&
	&*9.59765625E+1_DP-A(4)*1.06640625E+1_DP)/l**4
U(7) = ((-A(5)*1.96875E+1_DP*g)-A(5)*9.84375E&
	&+0_DP+1.09375E+0_DP*A(4))/l**4
U(8) = ((-4.921875E+0_DP*A(5)*g)-2.4609375E&
	&+0_DP*A(5)+2.734375E-1_DP*A(4))/l**4
U(9) = (A(5)*2.4609375E+1_DP*g&
	&+A(5)*1.23046875E+1_DP-1.3671875E+0_DP*A(4))/l**4
U(10) = ((g*(g*(A(5)*6.3E+2_DP*g+A(5)&
	&*9.45E+2_DP-A(4)*1.0500000000000001E+2_DP)+A(5)*4.2E+2_DP-A(4)&
	&*1.0500000000000001E+2_DP+A(3)*1.5E+1_DP)+A(5)*5.25E+1_DP-A(4)*2.25E+1_DP&
	&+A(3)*7.5E+0_DP-1.4999999999999997E+0_DP*A(2))*l**2+A(5)*2.75625E&
	&+2_DP*g+A(5)*1.378125E+2_DP-A(4)*1.53125E+1_DP)/l**4
U(11) = (A(5)*3.9375E+1_DP*g+A(5)*1.96875E&
	&+1_DP-2.1875E+0_DP*A(4))/l**4
U(12) = ((g*(g*((-A(5)*6.3E+2_DP*g)-A(5)*9.45E&
	&+2_DP+A(4)*1.0500000000000001E+2_DP)-A(5)*4.2E+2_DP&
	&+A(4)*1.0500000000000001E+2_DP-A(3)*1.5E+1_DP)-A(5)*5.25E+1_DP+A(4)*2.25E&
	&+1_DP-A(3)*7.5E+0_DP+1.4999999999999997E+0_DP*A(2))*l&
	&**2-A(5)*2.5593750000000004E+2_DP*g-A(5)*1.2796875000000002E+2_DP+A(4)&
	&*1.421875E+1_DP)/l**4
U(13) = ((-A(5)*5.90625E+1_DP*g)-A(5)*2.953125E&
	&+1_DP+3.28125E+0_DP*A(4))/l**4
U(14) = ((-A(5)*1.96875E+1_DP*g)-A(5)*9.84375E&
	&+0_DP+1.09375E+0_DP*A(4))/l**4
U(15) = (A(5)*1.96875E+1_DP*g+A(5)&
	&*9.84375E+0_DP-1.09375E+0_DP*A(4))/l**4
U(16) = ((g*(A(5)*4.725E+2_DP*g+A(5)*4.725E&
	&+2_DP-A(4)*5.25E+1_DP)+A(5)*1.0500000000000001E+2_DP-A(4)*2.625E+1_DP&
	&+A(3)*3.75E+0_DP)*l**2+A(5)*5.2294921875E+1_DP)/l**5
U(17) = (A(5)*8.45947265625E+0_DP)/l**5
U(18) = (l**2*((g*(g*(g*(A(5)*3.15E+2_DP*g+A(5)&
	&*6.3E+2_DP-A(4)*7.E+1_DP)+A(5)*4.2E+2_DP-A(4)*1.0500000000000001E&
	&+2_DP+A(3)*1.5E+1_DP)+A(5)*1.0500000000000001E+2_DP-A(4)*4.5E&
	&+1_DP+A(3)*1.5E+1_DP-A(2)*3.E+0_DP)+A(5)*7.5E+0_DP-A(4)*5.E+0_DP&
	&+A(3)*3.E+0_DP-1.4999999999999997E+0_DP*A(2)+5.E-1_DP*A(1))*l**2&
	&+g*(A(5)*4.725E+2_DP*g+A(5)*4.725E+2_DP-A(4)*5.25E&
	&+1_DP)+A(5)*1.0500000000000001E+2_DP-A(4)*2.625E+1_DP+A(3)*3.75E&
	&+0_DP)+A(5)*3.768310546875E+1_DP)/l**5
U(19) = ((g*((-A(5)*2.3625E+2_DP*g)-A(5)*2.3625E&
	&+2_DP+A(4)*2.625E+1_DP)-A(5)*5.25E+1_DP+A(4)*1.3125E+1_DP-1.875E&
	&+0_DP*A(3))*l**2-A(5)*2.70703125E+1_DP)/l**5
U(20) = -(A(5)*6.767578125E+0_DP)/l**5
U(21) = ((g*((-A(5)*2.3625E+2_DP*g)-A(5)*2.3625E&
	&+2_DP+A(4)*2.625E+1_DP)-A(5)*5.25E+1_DP+A(4)*1.3125E+1_DP-1.875E&
	&+0_DP*A(3))*l**2-A(5)*2.5224609375E+1_DP)/l**5
U(22) = (6.15234375E-1_DP*A(5))/l**5
U(23) = (1.69189453125E+0_DP*A(5))/l**5
U(24) = -(2.30712890625E+0_DP*A(5))/l**5
U(25) = (l**2*((g*(g*(g*((-A(5)*3.15E+2_DP*g)-A(5)&
	&*6.3E+2_DP+A(4)*7.E+1_DP)-A(5)*4.2E+2_DP&
	&+A(4)*1.0500000000000001E+2_DP-A(3)*1.5E+1_DP)-A(5)*1.0500000000000001E+2_DP+A(4)&
	&*4.5E+1_DP-A(3)*1.5E+1_DP+A(2)*3.E+0_DP)-A(5)*7.5E+0_DP+A(4)&
	&*5.E+0_DP-A(3)*3.E+0_DP+1.4999999999999997E+0_DP*A(2)-5.E-1_DP*A(1))*l*&
	&*2+g*((-A(5)*7.0875E+2_DP*g)-A(5)*7.0875E+2_DP+A(4)&
	&*7.875E+1_DP)-A(5)*1.575E+2_DP+A(4)*3.9375E+1_DP-A(3)&
	&*5.625E+0_DP)-A(5)*6.444580078125E+1_DP)/l**5
U(26) = ((g*((-A(5)*2.3625E+2_DP*g)-A(5)*2.3625E&
	&+2_DP+A(4)*2.625E+1_DP)-A(5)*5.25E+1_DP+A(4)*1.3125E+1_DP-1.875E&
	&+0_DP*A(3))*l**2-A(5)*2.8223876953125E+1_DP)/l**5
U(27) = -(A(5)*5.767822265625E+0_DP)/l**5
U(28) = ((g*(A(5)*4.134375E+2_DP*g+A(5)&
	&*4.134375E+2_DP-A(4)*4.59375E+1_DP)+A(5)*9.1875E+1_DP-A(4)*2.296875E&
	&+1_DP+A(3)*3.28125E+0_DP)*l**2+A(5)*4.39892578125E+1_DP)/l**5
U(29) = ((g*(A(5)*5.90625E+1_DP*g+A(5)&
	&*5.90625E+1_DP-A(4)*6.5625E+0_DP)+A(5)*1.3125E+1_DP-3.28125E+0_DP&
	&*A(4)+4.6875E-1_DP*A(3))*l**2+A(5)*1.276611328125E+1_DP)/l**5
U(30) = (2.30712890625E+0_DP*A(5))/l**5
U(31) = (2.92236328125E+0_DP*A(5))/l**5
U(32) = -(2.537841796875E+0_DP*A(5))/l**5
U(33) = -(3.84521484375E-1_DP*A(5))/l**5
U(34) = (l**2*((g*(g*(g*(A(5)*3.15E+2_DP*g+A(5)&
	&*6.3E+2_DP-A(4)*7.E+1_DP)+A(5)*4.2E+2_DP-A(4)*1.0500000000000001E&
	&+2_DP+A(3)*1.5E+1_DP)+A(5)*1.0500000000000001E+2_DP-A(4)*4.5E&
	&+1_DP+A(3)*1.5E+1_DP-A(2)*3.E+0_DP)+A(5)*7.5E+0_DP-A(4)*5.E+0_DP&
	&+A(3)*3.E+0_DP-1.4999999999999997E+0_DP*A(2)+5.E-1_DP*A(1))*l**2&
	&+g*(A(5)*2.953125E+2_DP*g+A(5)*2.953125E+2_DP-A(4)*3.28125E&
	&+1_DP)+A(5)*6.5625E+1_DP-A(4)*1.640625E+1_DP+2.34375E+0_DP&
	&*A(3))+A(5)*1.75341796875E+1_DP)/l**5
U(35) = ((g*(A(5)*1.771875E+2_DP*g+A(5)&
	&*1.771875E+2_DP-A(4)*1.96875E+1_DP)+A(5)*3.9375E+1_DP-A(4)*9.84375E&
	&+0_DP+1.40625E+0_DP*A(3))*l**2+A(5)*1.799560546875E+1_DP)/l**5
U(36) = (3.84521484375E+0_DP*A(5))/l**5
U(37) = ((g*(g*(A(5)*4.725E+2_DP*g+A(5)&
	&*7.0875E+2_DP-A(4)*7.875E+1_DP)+A(5)*3.15E+2_DP-A(4)*7.875E+1_DP+A(3)&
	&*1.125E+1_DP)+A(5)*3.9375E+1_DP-A(4)*1.6875E+1_DP+A(3)&
	&*5.625E+0_DP-1.125E+0_DP*A(2))*l**2+A(5)*1.771875E+2_DP*g+A(5)&
	&*8.859375E+1_DP-A(4)*9.84375E+0_DP)/l**4
U(38) = (A(5)*7.3828125E+1_DP*g+A(5)&
	&*3.69140625E+1_DP-A(4)*4.1015625E+0_DP)/l**4
U(39) = ((g*(g*(A(5)*1.575E+2_DP*g+A(5)&
	&*2.3625E+2_DP-A(4)*2.625E+1_DP)+A(5)*1.0500000000000001E+2_DP-A(4)*2.625E&
	&+1_DP+A(3)*3.75E+0_DP)+A(5)*1.3125E+1_DP-A(4)*5.625E+0_DP&
	&+1.875E+0_DP*A(3)-3.7499999999999994E-1_DP*A(2))*l**2+A(5)*6.3984375E&
	&+1_DP*g+A(5)*3.19921875E+1_DP-3.5546875E+0_DP*A(4))/l**4
U(40) = ((g*(g*((-A(5)*1.575E+2_DP*g)-A(5)*2.3625E&
	&+2_DP+A(4)*2.625E+1_DP)-A(5)*1.0500000000000001E+2_DP&
	&+A(4)*2.625E+1_DP-A(3)*3.75E+0_DP)-A(5)*1.3125E+1_DP+A(4)*5.625E&
	&+0_DP-1.875E+0_DP*A(3)+3.7499999999999994E-1_DP*A(2))*l**2-A(5)&
	&*2.0671875E+2_DP*g-A(5)*1.03359375E+2_DP+A(4)*1.1484375E+1_DP)/l**4
U(41) = ((-A(5)*3.69140625E+1_DP*g)-A(5)&
	&*1.845703125E+1_DP+2.05078125E+0_DP*A(4))/l**4
U(42) = ((g*(g*((-A(5)*4.725E+2_DP*g)-A(5)*7.0875E&
	&+2_DP+A(4)*7.875E+1_DP)-A(5)*3.15E+2_DP+A(4)*7.875E&
	&+1_DP-A(3)*1.125E+1_DP)-A(5)*3.9375E+1_DP+A(4)*1.6875E+1_DP-A(3)*5.625E&
	&+0_DP+1.125E+0_DP*A(2))*l**2-A(5)*2.2886718749999999E&
	&+2_DP*g-A(5)*1.1443359374999999E+2_DP+A(4)*1.271484375E+1_DP)/l**4
U(43) = (A(5)*8.859375E+1_DP*g+A(5)&
	&*4.4296875E+1_DP-A(4)*4.921875E+0_DP)/l**4
U(44) = (A(5)*7.3828125E+0_DP*g&
	&+3.69140625E+0_DP*A(5)-4.1015625E-1_DP*A(4))/l**4
U(45) = (A(5)*6.15234375E+1_DP*g&
	&+A(5)*3.076171875E+1_DP-3.41796875E+0_DP*A(4))/l**4
U(46) = ((g*(g*((-A(5)*6.3E+2_DP*g)-A(5)*9.45E&
	&+2_DP+A(4)*1.0500000000000001E+2_DP)-A(5)*4.2E+2_DP&
	&+A(4)*1.0500000000000001E+2_DP-A(3)*1.5E+1_DP)-A(5)*5.25E+1_DP+A(4)*2.25E&
	&+1_DP-A(3)*7.5E+0_DP+1.4999999999999997E+0_DP*A(2))*l**2-A(5)&
	&*2.3625E+2_DP*g-A(5)*1.18125E+2_DP+A(4)*1.3125E+1_DP)/l**4
U(47) = ((-A(5)*7.875E+1_DP*g)-A(5)*3.9375E+1_DP&
	&+A(4)*4.375E+0_DP)/l**4
U(48) = ((g*(g*(A(5)*6.3E+2_DP*g+A(5)&
	&*9.45E+2_DP-A(4)*1.0500000000000001E+2_DP)+A(5)*4.2E+2_DP-A(4)&
	&*1.0500000000000001E+2_DP+A(3)*1.5E+1_DP)+A(5)*5.25E+1_DP-A(4)*2.25E+1_DP&
	&+A(3)*7.5E+0_DP-1.4999999999999997E+0_DP*A(2))*l**2+A(5)*3.54375E&
	&+2_DP*g+A(5)*1.771875E+2_DP-A(4)*1.96875E+1_DP)/l**4
U(49) = (A(5)*1.18125E+2_DP*g+A(5)*5.90625E&
	&+1_DP-A(4)*6.5625E+0_DP)/l**4
U(50) = ((-A(5)*1.18125E+2_DP*g)-A(5)*5.90625E&
	&+1_DP+A(4)*6.5625E+0_DP)/l**4
U(51) = ((-A(5)*3.9375E+1_DP*g)-A(5)*1.96875E&
	&+1_DP+2.1875E+0_DP*A(4))/l**4
U(52) = ((g*((-A(5)*3.54375E+2_DP*g)-A(5)*3.54375E&
	&+2_DP+A(4)*3.9375E+1_DP)-A(5)*7.875E+1_DP+A(4)*1.96875E&
	&+1_DP-2.8125E+0_DP*A(3))*l**2-A(5)*3.568359375E+1_DP)/l**5
U(53) = -(A(5)*1.07666015625E+1_DP)/l**5
U(54) = ((g*((-A(5)*1.18125E+2_DP*g)-A(5)*1.18125E&
	&+2_DP+A(4)*1.3125E+1_DP)-A(5)*2.625E+1_DP+A(4)&
	&*6.5625E+0_DP-9.375E-1_DP*A(3))*l**2-A(5)*1.26123046875E+1_DP)/l**5
U(55) = ((g*(A(5)*1.771875E+2_DP*g+A(5)&
	&*1.771875E+2_DP-A(4)*1.96875E+1_DP)+A(5)*3.9375E+1_DP-A(4)*9.84375E&
	&+0_DP+1.40625E+0_DP*A(3))*l**2+A(5)*3.4453125E+1_DP)/l**5
U(56) = (A(5)*8.61328125E+0_DP)/l**5
U(57) = ((g*(A(5)*2.953125E+2_DP*g+A(5)&
	&*2.953125E+2_DP-A(4)*3.28125E+1_DP)+A(5)*6.5625E+1_DP-A(4)*1.640625E&
	&+1_DP+2.34375E+0_DP*A(3))*l**2+A(5)*3.568359375E+1_DP)/l**5
U(58) = -(A(5)*1.107421875E+1_DP)/l**5
U(59) = -(2.1533203125E+0_DP*A(5))/l**5
U(60) = -(A(5)*6.4599609375E+0_DP)/l**5
U(61) = ((g*(A(5)*3.54375E+2_DP*g+A(5)&
	&*3.54375E+2_DP-A(4)*3.9375E+1_DP)+A(5)*7.875E+1_DP-A(4)*1.96875E+1_DP&
	&+2.8125E+0_DP*A(3))*l**2+A(5)*3.6298828125E+1_DP)/l**5
U(62) = ((g*(A(5)*1.18125E+2_DP*g+A(5)&
	&*1.18125E+2_DP-A(4)*1.3125E+1_DP)+A(5)*2.625E+1_DP-A(4)*6.5625E&
	&+0_DP+9.375E-1_DP*A(3))*l**2+A(5)*1.81494140625E+1_DP)/l**5
U(63) = (4.6142578125E+0_DP*A(5))/l**5
U(64) = ((g*((-A(5)*4.4296875E+2_DP*g)-A(5)&
	&*4.4296875E+2_DP+A(4)*4.921875E+1_DP)-A(5)*9.84375E+1_DP&
	&+A(4)*2.4609375E+1_DP-A(3)*3.515625E+0_DP)*l**2-A(5)*5.783203125E+1_DP)/l**5
U(65) = ((g*((-A(5)*2.953125E+1_DP*g)-A(5)&
	&*2.953125E+1_DP+3.28125E+0_DP*A(4))-A(5)*6.5625E+0_DP&
	&+1.640625E+0_DP*A(4)-2.34375E-1_DP*A(3))*l**2-A(5)*1.9072265625E+1_DP)/l**5
U(66) = -(1.845703125E+0_DP*A(5))/l**5
U(67) = (A(5)*1.2919921875E+1_DP)/l**5
U(68) = (A(5)*6.4599609375E+0_DP)/l**5
U(69) = (3.076171875E-1_DP*A(5))/l**5
U(70) = ((g*(A(5)*8.859375E+1_DP*g+A(5)&
	&*8.859375E+1_DP-A(4)*9.84375E+0_DP)+A(5)*1.96875E+1_DP-A(4)&
	&*4.921875E+0_DP+7.03125E-1_DP*A(3))*l**2+A(5)*8.61328125E+0_DP)/l**5
U(71) = ((g*((-A(5)*8.859375E+1_DP*g)-A(5)*8.859375E&
	&+1_DP+A(4)*9.84375E+0_DP)-A(5)*1.96875E+1_DP&
	&+A(4)*4.921875E+0_DP-7.03125E-1_DP*A(3))*l**2-A(5)*5.537109375E+0_DP)/l**5
U(72) = -(3.076171875E+0_DP*A(5))/l**5
U(73) = (A(5)*1.96875E+1_DP*g+A(5)&
	&*9.84375E+0_DP-1.09375E+0_DP*A(4))/l**4
U(74) = ((-A(5)*2.4609375E+1_DP*g)-A(5)&
	&*1.23046875E+1_DP+1.3671875E+0_DP*A(4))/l**4
U(75) = (4.921875E+0_DP*A(5)*g&
	&+2.4609375E+0_DP*A(5)-2.734375E-1_DP*A(4))/l**4
U(76) = (A(5)*1.0828125E+2_DP*g+A(5)&
	&*5.4140625E+1_DP-A(4)*6.015625E+0_DP)/l**4
U(77) = (A(5)*1.23046875E+1_DP*g&
	&+A(5)*6.15234375E+0_DP-6.8359375E-1_DP*A(4))/l**4
U(78) = (A(5)*3.69140625E+1_DP*g&
	&+A(5)*1.845703125E+1_DP-2.05078125E+0_DP*A(4))/l**4
U(79) = ((-A(5)*6.890625E+1_DP*g)-A(5)&
	&*3.4453125E+1_DP+3.828125E+0_DP*A(4))/l**4
U(80) = ((-2.4609375E+0_DP*A(5)*g)-1.23046875E&
	&+0_DP*A(5)+1.3671875E-1_DP*A(4))/l**4
U(81) = ((-A(5)*8.61328125E+1_DP*g)-A(5)&
	&*4.306640625E+1_DP+A(4)*4.78515625E+0_DP)/l**4
U(82) = ((-A(5)*3.9375E+1_DP*g)-A(5)*1.96875E&
	&+1_DP+2.1875E+0_DP*A(4))/l**4
U(83) = (A(5)*3.9375E+1_DP*g+A(5)*1.96875E&
	&+1_DP-2.1875E+0_DP*A(4))/l**4
U(84) = ((-A(5)*9.84375E+1_DP*g)-A(5)*4.921875E&
	&+1_DP+A(4)*5.46875E+0_DP)/l**4
U(85) = ((-A(5)*5.90625E+1_DP*g)-A(5)*2.953125E&
	&+1_DP+3.28125E+0_DP*A(4))/l**4
U(86) = (A(5)*1.378125E+2_DP*g+A(5)&
	&*6.890625E+1_DP-A(4)*7.65625E+0_DP)/l**4
U(87) = (A(5)*1.96875E+1_DP*g+A(5)&
	&*9.84375E+0_DP-1.09375E+0_DP*A(4))/l**4
U(88) = -(3.076171875E+0_DP*A(5))/l**5
U(89) = (3.84521484375E+0_DP*A(5))/l**5
U(90) = -(7.6904296875E-1_DP*A(5))/l**5
U(91) = -(A(5)*1.23046875E+1_DP)/l**5
U(92) = -(3.076171875E+0_DP*A(5))/l**5
U(93) = -(4.306640625E+0_DP*A(5))/l**5
U(94) = (A(5)*9.228515625E+0_DP)/l**5
U(95) = (7.6904296875E-1_DP*A(5))/l**5
U(96) = (A(5)*9.68994140625E+0_DP)/l**5
U(97) = (3.84521484375E+0_DP*A(5))/l**5
U(98) = -(2.691650390625E+0_DP*A(5))/l**5
U(99) = -(1.153564453125E+0_DP*A(5))/l**5
U(100) = (A(5)*1.07666015625E+1_DP)/l**5
U(101) = (A(5)*8.45947265625E+0_DP)/l**5
U(102) = (4.6142578125E-1_DP*A(5))/l**5
U(103) = -(A(5)*1.614990234375E+1_DP)/l**5
U(104) = -(3.460693359375E+0_DP*A(5))/l**5
U(105) = -(7.6904296875E-2_DP*A(5))/l**5
U(106) = (1.5380859375E+0_DP*A(5))/l**5
U(107) = -(2.30712890625E+0_DP*A(5))/l**5
U(108) = (7.6904296875E-1_DP*A(5))/l**5
U(109) = ((g*((-A(5)*1.18125E+2_DP*g)-A(5)*1.18125E&
	&+2_DP+A(4)*1.3125E+1_DP)-A(5)*2.625E+1_DP+A(4)&
	&*6.5625E+0_DP-9.375E-1_DP*A(3))*l**2-A(5)*1.353515625E+1_DP)/l**5
U(110) = -(1.5380859375E+0_DP*A(5))/l**5
U(111) = (l**2*((g*(g*(g*((-A(5)*3.15E+2_DP*g)-A(5)&
	&*6.3E+2_DP+A(4)*7.E+1_DP)-A(5)*4.2E+2_DP&
	&+A(4)*1.0500000000000001E+2_DP-A(3)*1.5E+1_DP)-A(5)*1.0500000000000001E+2_DP&
	&+A(4)*4.5E+1_DP-A(3)*1.5E+1_DP+A(2)*3.E+0_DP)-A(5)*7.5E+0_DP+A(4)&
	&*5.E+0_DP-A(3)*3.E+0_DP+1.4999999999999997E+0_DP*A(2)-5.E-1_DP*A(1))*l*&
	&*2+g*((-A(5)*3.54375E+2_DP*g)-A(5)*3.54375E+2_DP&
	&+A(4)*3.9375E+1_DP)-A(5)*7.875E+1_DP+A(4)*1.96875E+1_DP-2.8125E&
	&+0_DP*A(3))-A(5)*2.43017578125E+1_DP)/l**5
U(112) = ((g*(A(5)*5.90625E+1_DP*g+A(5)&
	&*5.90625E+1_DP-A(4)*6.5625E+0_DP)+A(5)*1.3125E+1_DP-3.28125E+0_DP&
	&*A(4)+4.6875E-1_DP*A(3))*l**2+4.921875E+0_DP*A(5))/l**5
U(113) = (1.23046875E+0_DP*A(5))/l**5
U(114) = ((g*((-A(5)*5.90625E+1_DP*g)-A(5)*5.90625E&
	&+1_DP+A(4)*6.5625E+0_DP)-A(5)*1.3125E+1_DP&
	&+3.28125E+0_DP*A(4)-4.6875E-1_DP*A(3))*l**2-A(5)*6.15234375E+0_DP)/l**5
U(115) = (1.23046875E+0_DP*A(5))/l**5
U(116) = -(3.076171875E-1_DP*A(5))/l**5
U(117) = -(9.228515625E-1_DP*A(5))/l**5
ENDFUNCTION 


PURE FUNCTION  CALCULATE_ANALYTICALLY_G_5 (S0_X, S0_Y, S0_Z, SC_X, SC_Y, SC_Z, U)& 
			& RESULT(RES) 
REAL(DP), INTENT(IN)::S0_X, S0_Y, S0_Z 
REAL(DP), INTENT(IN)::SC_X(PMAX,2), SC_Y(PMAX,2), SC_Z(PMAX,2) 
COMPLEX(DP),INTENT(IN)::U(1: G_5_COEFFS_LEN ) 
COMPLEX(DP)::RES 
RES = ((SC_Y(6,2)*U(117)+SC_Y(4,2)*U(114)+SC_Y(2,2)&
	&*U(111))*S0_x+SC_X(6,1)*SC_Y(6,2)*U(96)+SC_Y(4,2)*SC_X(6,1)&
	&*U(93)+SC_Y(2,2)*SC_X(6,1)*U(90)+SC_X(5,1)*SC_Y(5,2)*U(81)+SC_Y(3,2)&
	&*SC_X(5,1)*U(78)+SC_Y(1,2)*SC_X(5,1)*U(75)+SC_X(4,1)*SC_Y(6,2)&
	&*U(60)+SC_X(4,1)*SC_Y(4,2)*U(57)+SC_Y(2,2)*SC_X(4,1)*U(54)+SC_X(3,1)&
	&*SC_Y(5,2)*U(45)+SC_X(3,1)*SC_Y(3,2)*U(42)+SC_Y(1,2)*SC_X(3,1)&
	&*U(39)+SC_X(2,1)*SC_Y(6,2)*U(24)+SC_X(2,1)*SC_Y(4,2)*U(21)+SC_X(2,1)&
	&*SC_Y(2,2)*U(18)+SC_X(1,1)*SC_Y(5,2)*U(9)+SC_X(1,1)*SC_Y(3,2)&
	&*U(6)+SC_X(1,1)*SC_Y(1,2)*U(3))*S0_z+(SC_Z(5,1)*SC_X(6,2)*U(108)&
	&+SC_Z(3,1)*SC_X(6,2)*U(107)+SC_Z(1,1)*SC_X(6,2)*U(106)+SC_X(4,2)&
	&*SC_Z(5,1)*U(72)+SC_Z(3,1)*SC_X(4,2)*U(71)+SC_Z(1,1)*SC_X(4,2)&
	&*U(70)+SC_X(2,2)*SC_Z(5,1)*U(36)+SC_X(2,2)*SC_Z(3,1)*U(35)+SC_Z(1,1)*SC_X(2,2)&
	&*U(34))*S0_y+(SC_Z(4,1)*SC_Y(6,2)*U(116)+SC_Z(2,1)*SC_Y(6,2)&
	&*U(115)+SC_Z(4,1)*SC_Y(4,2)*U(113)+SC_Z(2,1)*SC_Y(4,2)*U(112)&
	&+SC_Y(2,2)*SC_Z(4,1)*U(110)+SC_Z(2,1)*SC_Y(2,2)*U(109))*S0_x&
	&+SC_Z(5,1)*SC_Y(6,1)*SC_X(6,2)*U(105)+SC_Z(3,1)*SC_Y(6,1)*SC_X(6,2)&
	&*U(104)+SC_Z(1,1)*SC_Y(6,1)*SC_X(6,2)*U(103)+SC_Y(4,1)*SC_Z(5,1)&
	&*SC_X(6,2)*U(102)+SC_Z(3,1)*SC_Y(4,1)*SC_X(6,2)*U(101)+SC_Z(1,1)&
	&*SC_Y(4,1)*SC_X(6,2)*U(100)+SC_Y(2,1)*SC_Z(5,1)*SC_X(6,2)*U(99)&
	&+SC_Y(2,1)*SC_Z(3,1)*SC_X(6,2)*U(98)+SC_Z(1,1)*SC_Y(2,1)*SC_X(6,2)*U(97)&
	&+SC_Z(4,1)*SC_X(6,1)*SC_Y(6,2)*U(95)+SC_Z(2,1)*SC_X(6,1)*SC_Y(6,2)&
	&*U(94)+SC_Z(4,1)*SC_Y(4,2)*SC_X(6,1)*U(92)+SC_Z(2,1)*SC_Y(4,2)&
	&*SC_X(6,1)*U(91)+SC_Y(2,2)*SC_Z(4,1)*SC_X(6,1)*U(89)+SC_Z(2,1)*SC_Y(2,2)&
	&*SC_X(6,1)*U(88)+SC_Z(3,1)*SC_Y(5,1)*SC_X(5,2)*U(87)&
	&+SC_Z(1,1)*SC_Y(5,1)*SC_X(5,2)*U(86)+SC_Y(3,1)*SC_Z(3,1)*SC_X(5,2)*U(85)&
	&+SC_Z(1,1)*SC_Y(3,1)*SC_X(5,2)*U(84)+SC_Y(1,1)*SC_Z(3,1)*SC_X(5,2)&
	&*U(83)+SC_Y(1,1)*SC_Z(1,1)*SC_X(5,2)*U(82)+SC_Z(4,1)*SC_X(5,1)&
	&*SC_Y(5,2)*U(80)+SC_Z(2,1)*SC_X(5,1)*SC_Y(5,2)*U(79)+SC_Y(3,2)*SC_Z(4,1)&
	&*SC_X(5,1)*U(77)+SC_Z(2,1)*SC_Y(3,2)*SC_X(5,1)*U(76)+SC_Y(1,2)&
	&*SC_Z(4,1)*SC_X(5,1)*U(74)+SC_Y(1,2)*SC_Z(2,1)*SC_X(5,1)*U(73)&
	&+SC_X(4,2)*SC_Z(5,1)*SC_Y(6,1)*U(69)+SC_Z(3,1)*SC_X(4,2)*SC_Y(6,1)&
	&*U(68)+SC_Z(1,1)*SC_X(4,2)*SC_Y(6,1)*U(67)+SC_Y(4,1)*SC_X(4,2)&
	&*SC_Z(5,1)*U(66)+SC_Z(3,1)*SC_Y(4,1)*SC_X(4,2)*U(65)+SC_Z(1,1)*SC_Y(4,1)&
	&*SC_X(4,2)*U(64)+SC_Y(2,1)*SC_X(4,2)*SC_Z(5,1)*U(63)+SC_Y(2,1)&
	&*SC_Z(3,1)*SC_X(4,2)*U(62)+SC_Z(1,1)*SC_Y(2,1)*SC_X(4,2)*U(61)&
	&+SC_X(4,1)*SC_Z(4,1)*SC_Y(6,2)*U(59)+SC_Z(2,1)*SC_X(4,1)*SC_Y(6,2)&
	&*U(58)+SC_X(4,1)*SC_Z(4,1)*SC_Y(4,2)*U(56)+SC_Z(2,1)*SC_X(4,1)*SC_Y(4,2)&
	&*U(55)+SC_Y(2,2)*SC_X(4,1)*SC_Z(4,1)*U(53)+SC_Z(2,1)*SC_Y(2,2)&
	&*SC_X(4,1)*U(52)+SC_Z(3,1)*SC_X(3,2)*SC_Y(5,1)*U(51)+SC_Z(1,1)&
	&*SC_X(3,2)*SC_Y(5,1)*U(50)+SC_Y(3,1)*SC_Z(3,1)*SC_X(3,2)*U(49)&
	&+SC_Z(1,1)*SC_Y(3,1)*SC_X(3,2)*U(48)+SC_Y(1,1)*SC_Z(3,1)*SC_X(3,2)&
	&*U(47)+SC_Y(1,1)*SC_Z(1,1)*SC_X(3,2)*U(46)+SC_X(3,1)*SC_Z(4,1)*SC_Y(5,2)&
	&*U(44)+SC_Z(2,1)*SC_X(3,1)*SC_Y(5,2)*U(43)+SC_X(3,1)*SC_Y(3,2)&
	&*SC_Z(4,1)*U(41)+SC_Z(2,1)*SC_X(3,1)*SC_Y(3,2)*U(40)+SC_Y(1,2)&
	&*SC_X(3,1)*SC_Z(4,1)*U(38)+SC_Y(1,2)*SC_Z(2,1)*SC_X(3,1)*U(37)&
	&+SC_X(2,2)*SC_Z(5,1)*SC_Y(6,1)*U(33)+SC_X(2,2)*SC_Z(3,1)*SC_Y(6,1)&
	&*U(32)+SC_Z(1,1)*SC_X(2,2)*SC_Y(6,1)*U(31)+SC_X(2,2)*SC_Y(4,1)*SC_Z(5,1)&
	&*U(30)+SC_X(2,2)*SC_Z(3,1)*SC_Y(4,1)*U(29)+SC_Z(1,1)*SC_X(2,2)&
	&*SC_Y(4,1)*U(28)+SC_Y(2,1)*SC_X(2,2)*SC_Z(5,1)*U(27)+SC_Y(2,1)&
	&*SC_X(2,2)*SC_Z(3,1)*U(26)+SC_Z(1,1)*SC_Y(2,1)*SC_X(2,2)*U(25)&
	&+SC_X(2,1)*SC_Z(4,1)*SC_Y(6,2)*U(23)+SC_X(2,1)*SC_Z(2,1)*SC_Y(6,2)*U(22)&
	&+SC_X(2,1)*SC_Z(4,1)*SC_Y(4,2)*U(20)+SC_X(2,1)*SC_Z(2,1)*SC_Y(4,2)&
	&*U(19)+SC_X(2,1)*SC_Y(2,2)*SC_Z(4,1)*U(17)+SC_X(2,1)*SC_Z(2,1)&
	&*SC_Y(2,2)*U(16)+SC_X(1,2)*SC_Z(3,1)*SC_Y(5,1)*U(15)+SC_Z(1,1)&
	&*SC_X(1,2)*SC_Y(5,1)*U(14)+SC_X(1,2)*SC_Y(3,1)*SC_Z(3,1)*U(13)&
	&+SC_Z(1,1)*SC_X(1,2)*SC_Y(3,1)*U(12)+SC_Y(1,1)*SC_X(1,2)*SC_Z(3,1)*U(11)&
	&+SC_Y(1,1)*SC_Z(1,1)*SC_X(1,2)*U(10)+SC_X(1,1)*SC_Z(4,1)*SC_Y(5,2)&
	&*U(8)+SC_X(1,1)*SC_Z(2,1)*SC_Y(5,2)*U(7)+SC_X(1,1)*SC_Y(3,2)*SC_Z(4,1)&
	&*U(5)+SC_X(1,1)*SC_Y(1,2)*U(2)*SC_Z(4,1)+SC_X(1,1)*SC_Z(2,1)*SC_Y(3,2)&
	&*U(4)+U(1)*SC_X(1,1)*SC_Y(1,2)*SC_Z(2,1)
ENDFUNCTION 
