PURE FUNCTION  FINALIZE_G_E_THETA_THETA_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER12_F_E) RESULT( G_E_TT ) 
         
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER12_F_E(:, :, :, RC_E_LP:) 
    COMPLEX(DP) :: G_E_TT(SIZE(DER12_F_E,1), SIZE(DER12_F_E,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER12_F_E,2) 
        DO I=1,  SIZE(DER12_F_E,1) 
            G_E_TT(I,Ir) = cos(theta_s(I))&
                &*(DER12_F_E(I,Ir,1,RC_E_LP)&
                &*cos(phi_r-phi_s(I))*cos(theta_r)-DER12_F_E(I,Ir,2,RC_E_LP)*cos(theta_s(I))*cos(phi_r-phi_s(I))*sin(theta_r)*&
                &*2)+sin(theta_s(I))*(cos(theta_s(I))*(DER12_F_E(I,Ir,2,RC_E_LP)*cos(phi_r-phi_s(I))&
                &**2+DER12_F_E(I,Ir,2,RC_E_LP))&
                &*cos(theta_r)*sin(theta_r)+DER12_F_E(I,Ir,1,RC_E_LP)&
                &*sin(theta_r)-DER12_F_E(I,Ir,2,RC_E_LP)*sin(theta_s(I))*cos(phi_r-phi_s(I))*cos(theta_r)**2)
        ENDDO 
    ENDDO 
ENDFUNCTION 

PURE FUNCTION  FINALIZE_G_E_THETA_R_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER_E_LR) RESULT( G_E_TR ) 
         
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER_E_LR(:, :) 
    COMPLEX(DP) :: G_E_TR(SIZE(DER_E_LR,1), SIZE(DER_E_LR,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER_E_LR,2) 
        DO I=1,  SIZE(DER_E_LR,1) 
            G_E_TR(I,Ir) =  DER_E_LR(I,Ir)*(cos(theta_s(I))*sin(theta_r)&
                -sin(theta_s(I))*cos(phi_r-phi_s(I))*cos(theta_r))
        ENDDO 
    ENDDO 
ENDFUNCTION 

PURE FUNCTION  FINALIZE_G_E_PHI_PHI_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER12_F_E) RESULT( G_E_PP ) 
         
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER12_F_E(:, :, :, RC_E_LP:) 
    COMPLEX(DP) :: G_E_PP(SIZE(DER12_F_E,1), SIZE(DER12_F_E,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER12_F_E,2) 
        DO I=1,  SIZE(DER12_F_E,1) 
            G_E_PP(I,Ir) = cos(theta_s(I))&
                &*(DER12_F_E(I,Ir,1,RC_E_LT)&
                &*cos(phi_r-phi_s(I))*cos(theta_r)-DER12_F_E(I,Ir,2,RC_E_LT)*cos(theta_s(I))*cos(phi_r-phi_s(I))*sin(theta_r)*&
                &*2)+sin(theta_s(I))*(cos(theta_s(I))*(DER12_F_E(I,Ir,2,RC_E_LT)*cos(phi_r-phi_s(I))&
                &**2+DER12_F_E(I,Ir,2,RC_E_LT))&
                &*cos(theta_r)*sin(theta_r)+DER12_F_E(I,Ir,1,RC_E_LT)&
                &*sin(theta_r)-DER12_F_E(I,Ir,2,RC_E_LT)*sin(theta_s(I))*cos(phi_r-phi_s(I))*cos(theta_r)**2)
        ENDDO 
    ENDDO 
ENDFUNCTION 

PURE FUNCTION  FINALIZE_G_E_R_THETA_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, DER_E_RL) RESULT( G_E_RT ) 
         
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: DER_E_RL(:, :) 
    COMPLEX(DP) :: G_E_RT(SIZE(DER_E_RL,1), SIZE(DER_E_RL,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(DER_E_RL,2) 
        DO I=1,  SIZE(DER_E_RL,1) 
            G_E_RT(I,Ir) =  DER_E_RL(I,Ir)*(sin(theta_s(I))*cos(theta_r)&
                -cos(theta_s(I))*cos(phi_r-phi_s(I))*sin(theta_r))
        ENDDO 
    ENDDO 
ENDFUNCTION 


PURE FUNCTION  FINALIZE_G_E_R_R_MANY_SRC_ALONG_PHI_NOT_CACHED & 
        (phi_r, theta_r, phi_s,  theta_s, F_E_RR) RESULT( G_E_RR ) 
         
    REAL(DP),INTENT(IN) :: phi_r, theta_r 
    REAL(DP),INTENT(IN) :: phi_s(:), theta_s(:) 
    COMPLEX(DP), INTENT(IN) :: F_E_RR(:, :) 
    COMPLEX(DP) :: G_E_RR(SIZE(F_E_RR,1), SIZE(F_E_RR,2)) 
    INTEGER :: I, Ir 
    DO Ir=1, SIZE(F_E_RR,2) 
        DO I=1,  SIZE(F_E_RR,1) 
            G_E_RR(I,Ir) =  F_E_RR(I,Ir)
        ENDDO 
    ENDDO 
ENDFUNCTION 

