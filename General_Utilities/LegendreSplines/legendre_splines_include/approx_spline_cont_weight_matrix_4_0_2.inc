DO I=0, N-1 
AB(11,6*I+1) = 2.E+0_DP*y(I+1)-2.E+0_DP*y(I)
AB(11,6*I+2) = 3.333333333333333E-1_DP*(2.E+0_DP&
	&*y(I+1)-2.E+0_DP*y(I))
AB(11,6*I+3) = 2.E-1_DP*(2.E+0_DP*y(I+1)-2.E+0_DP*y(I))
AB(11,6*I+4) = 1.4285714285714285E-1_DP*(2.E+0_DP&
	&*y(I+1)-2.E+0_DP*y(I))
AB(11,6*I+5) = 1.1111111111111112E-1_DP*(2.E+0_DP&
	&*y(I+1)-2.E+0_DP*y(I))
ENDDO 
DO I=1, N 

IF (6*I-11>0 ) AB(12,6*I-6) = sg(I-1)
IF ( N2>=6*I) AB(6,6*I) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(16,6*I-5) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(10,6*I+1) = sg(I)

IF (6*I-11>0 ) AB(13,6*I-6) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(7,6*I) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(15,6*I-4) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(9,6*I+2) = -1.E+0_DP*sg(I)

IF (6*I-11>0 ) AB(14,6*I-6) = sg(I-1)
IF ( N2>=6*I) AB(8,6*I) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(14,6*I-3) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(8,6*I+3) = sg(I)

IF (6*I-11>0 ) AB(15,6*I-6) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(9,6*I) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(13,6*I-2) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(7,6*I+4) = -1.E+0_DP*sg(I)

IF (6*I-11>0 ) AB(16,6*I-6) = sg(I-1)
IF ( N2>=6*I) AB(10,6*I) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(12,6*I-1) = -1.E+0_DP*sg(I-1)
IF ( N2>=6*I) AB(6,6*I+5) = sg(I)
ENDDO 
