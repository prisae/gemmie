DO I=0, N-1 
AB(9,5*I+1) = 2.E+0_DP*y(I+1)-2.E+0_DP*y(I)
AB(9,5*I+2) = 3.333333333333333E-1_DP*(2.E+0_DP&
	&*y(I+1)-2.E+0_DP*y(I))
AB(9,5*I+3) = 2.E-1_DP*(2.E+0_DP*y(I+1)-2.E+0_DP*y(I))
AB(9,5*I+4) = 1.4285714285714285E-1_DP*(2.E+0_DP&
	&*y(I+1)-2.E+0_DP*y(I))
ENDDO 
DO I=1, N 

IF (5*I-9>0 ) AB(10,5*I-5) = 1.E+0_DP
IF ( N2>=5*I) AB(5,5*I) = -1.E+0_DP
IF ( N2>=5*I) AB(13,5*I-4) = -1.E+0_DP
IF ( N2>=5*I) AB(8,5*I+1) = 1.E+0_DP

IF (5*I-9>0 ) AB(11,5*I-5) = -1.E+0_DP
IF ( N2>=5*I) AB(6,5*I) = -1.E+0_DP
IF ( N2>=5*I) AB(12,5*I-3) = -1.E+0_DP
IF ( N2>=5*I) AB(7,5*I+2) = -1.E+0_DP

IF (5*I-9>0 ) AB(12,5*I-5) = 1.E+0_DP
IF ( N2>=5*I) AB(7,5*I) = -1.E+0_DP
IF ( N2>=5*I) AB(11,5*I-2) = -1.E+0_DP
IF ( N2>=5*I) AB(6,5*I+3) = 1.E+0_DP

IF (5*I-9>0 ) AB(13,5*I-5) = -1.E+0_DP
IF ( N2>=5*I) AB(8,5*I) = -1.E+0_DP
IF ( N2>=5*I) AB(10,5*I-1) = -1.E+0_DP
IF ( N2>=5*I) AB(5,5*I+4) = -1.E+0_DP
ENDDO 
